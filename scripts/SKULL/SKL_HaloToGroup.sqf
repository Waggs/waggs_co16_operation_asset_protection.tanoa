// HALO Drop To Group: use in object to add action to allow JIP'ers to catch up with their group
//
// usage - in init field: _nil = this execVM "scripts\SKULL\SKL_HaloToGroup.sqf";
//
// Version 1.1  10-04-14 changed add action color
// Version 1.0  5-17-14
// 1.0: initial release

_this addAction ["<t color='#00aeff'>Parachute to team</t>", 
    { 
        private ["_done","_player"];
        _done = false;
        _player = _this select 1;
        _pos = getPos player;
        {
            if (_player distance _x > 400) exitWith {
                _pos = getPos _x;
                _done = true;
            };
        } forEach units group _player;
        if (!_done) then {
            {
                if ((side _x == side _player) && ((name _player != "HC") || (name _player != "HC2")) && (_player distance _x > 400)) exitWith {
                    _pos = getPos _x;
                    _done = true;
                };
            } forEach playableUnits;
        };
        if (_done) then {
            _pos set [2,1000];
            _player setPos _pos; 
            _player setCaptive true;
            waitUntil {((getPosATL _player) select 2) < 100}; 
            [_player] call BIS_fnc_halo;
            waitUntil { isTouchingGround _player }; 
            sleep 3;
            _player setCaptive false;
        };
    } 
];