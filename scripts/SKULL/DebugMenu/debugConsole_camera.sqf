if (!isNil "SKL_feed_target") then
{
  if ((!isNull SKL_feed_target) && (typeOf SKL_feed_target == "Land_HelipadEmpty_F")) then
    {
        deleteVehicle SKL_feed_target;
        SKL_feed_target = objNull;
    };
};

[] call BIS_fnc_liveFeedTerminate;
waitUntil {(isNil "BIS_liveFeed")};

if (_this == 1) then 
{
    SKL_feed_target = cursorTarget;
    if (isNull SKL_feed_target) then 
    { 
        SKL_feed_target = "Land_HelipadEmpty_F" createVehicle screenToWorld [0.5,0.5];
    };
    _SKL_feed_source = "Land_HelipadEmpty_F" createVehicle (getPos player);
    _SKL_feed_source enableSimulation false;
    _SKL_feed_source setPosATL [getPos player select 0, getPos player select 1, (getPos player select 2) + 5];
    [_SKL_feed_source, SKL_feed_target, player] call BIS_fnc_liveFeed;
	waitUntil {!(isNil "BIS_liveFeed")};
    deleteVehicle _SKL_feed_source;
};
