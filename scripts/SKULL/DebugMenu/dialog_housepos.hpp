
///////////////////////////////////////////////////////////////////////////
/// HousePos dialog
///////////////////////////////////////////////////////////////////////////

class FHQ_DebugDialog_HousePos
{
	idd = 140871;
	movingEnable = true;
	enableSimulation = true;
	objects[] = { };

	controlsBackground[] = { FHQ_DebugConsole_HousePos_Backdrop };

	controls[] = {FHQ_DebugConsole_HousePos_Header, FHQ_DebugConsole_HousePos_Close, FHQ_DebugConsole_HousePos_Pause,
		FHQ_DebugConsole_HousePos_Select, FHQ_DebugConsole_HousePos_ModelText, FHQ_DebugConsole_HousePos_Model,
		FHQ_DebugConsole_HousePos_IDText, FHQ_DebugConsole_HousePos_SelectText, FHQ_DebugConsole_HousePos_PositionText,
		FHQ_DebugConsole_HousePos_InitFieldText, FHQ_DebugConsole_HousePos_InitField,
		FHQ_DebugConsole_HousePos_Position, FHQ_DebugConsole_HousePos_ID, FHQ_DebugConsole_HousePos_Jump};

	class FHQ_DebugConsole_HousePos_Header: FHQRscText
	{
		moving = false;

		idc = 1000;
		text = "House Positions";
		x = 0.29374 * safezoneW + safezoneX;
		y = 0.224918 * safezoneH + safezoneY;
		w = 0.41252 * safezoneW;
		h = 0.0275082 * safezoneH;
		colorBackground[] = {0,0,0,1};
	};
	class FHQ_DebugConsole_HousePos_Close: FHQRscButton
	{
		action = "closeDialog 0;";

		idc = 1600;
		text = "Close";
		x = 0.29374 * safezoneW + safezoneX;
		y = 0.747574 * safezoneH + safezoneY;
		w = 0.0902388 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_Pause: FHQRscButton
	{
		action = "FHQ_DebugConsole_CurrentHouse = FHQ_DebugConsole_SelectedHouse; closeDialog 0; hint 'Press ENTER to restore dialog'; SKL_KeyHandler = FHQ_DebugConsole_HouseposSelect; call SKL_DebugWatchKeys;";
		idc = 1601;
		text = "Hide dialog";
		x = 0.390424 * safezoneW + safezoneX;
		y = 0.747574 * safezoneH + safezoneY;
		w = 0.0902388 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_Select: FHQRscCombo
	{
		onLBSelChanged = "_this call FHQ_DebugConsole_HousePos_SelectPos;";

		idc = 2100;
		text = "Select";
		x = 0.39687 * safezoneW + safezoneX;
		y = 0.279935 * safezoneH + safezoneY;
		w = 0.116021 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_ModelText: FHQRscText
	{
		idc = 1001;
		text = "House: ";
		x = 0.306631 * safezoneW + safezoneX;
		y = 0.334951 * safezoneH + safezoneY;
		w = 0.0837931 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_Model: FHQRscText
	{
		idc = 1002;
		x = 0.39687 * safezoneW + safezoneX;
		y = 0.334951 * safezoneH + safezoneY;
		w = 0.270716 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_IDText: FHQRscText
	{
		idc = 1003;
		text = "House ID:";
		x = 0.306631 * safezoneW + safezoneX;
		y = 0.389967 * safezoneH + safezoneY;
		w = 0.0837931 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_SelectText: FHQRscText
	{
		idc = 1005;
		text = "Select Position";
		x = 0.306631 * safezoneW + safezoneX;
		y = 0.279935 * safezoneH + safezoneY;
		w = 0.0837931 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_PositionText: FHQRscText
	{
		idc = 1006;
		text = "Position:";
		x = 0.306631 * safezoneW + safezoneX;
		y = 0.444984 * safezoneH + safezoneY;
		w = 0.0837931 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_InitFieldText: FHQRscText
	{
		idc = 1008;
		text = "Unit Init Field:";
		x = 0.306631 * safezoneW + safezoneX;
		y = 0.5 * safezoneH + safezoneY;
		w = 0.0837931 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_InitField: FHQRscEdit
	{
		idc = 1400;
		x = 0.39687 * safezoneW + safezoneX;
		y = 0.5 * safezoneH + safezoneY;
		w = 0.270716 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_Position: FHQRscEdit
	{
		idc = 1401;
		x = 0.39687 * safezoneW + safezoneX;
		y = 0.444984 * safezoneH + safezoneY;
		w = 0.270716 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_ID: FHQRscEdit
	{
		idc = 1402;
		x = 0.39687 * safezoneW + safezoneX;
		y = 0.389967 * safezoneH + safezoneY;
		w = 0.270716 * safezoneW;
		h = 0.0275082 * safezoneH;
	};
	class FHQ_DebugConsole_HousePos_Backdrop: FHQRscText
	{
		moving = false;

		idc = 1004;
		x = 0.29374 * safezoneW + safezoneX;
		y = 0.224918 * safezoneH + safezoneY;
		w = 0.41252 * safezoneW;
		h = 0.550164 * safezoneH;
		colorBackground[] = {0,0,0,0.4};
	};
	class FHQ_DebugConsole_HousePos_Jump: FHQRscButton
	{
		action = "[] call FHQ_DebugConsole_HousePos_Jump;";

		idc = 1602;
		text = "Jump to Position";
		x = 0.532228 * safezoneW + safezoneX;
		y = 0.279935 * safezoneH + safezoneY;
		w = 0.135358 * safezoneW;
		h = 0.0275082 * safezoneH;
	};

};

