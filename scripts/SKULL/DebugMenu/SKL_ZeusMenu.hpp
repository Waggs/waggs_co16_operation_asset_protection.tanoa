// Debug Dialog
// To use, set a radio trigger to activate: _handle = CreateDialog "SKL_DEBUG_DIALOG";
// In description.ext add:  
//         #include "scripts\SKULL\DebugMenu.hpp"

#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class SKLZD_BOX
{
	type = 0;
	idc = -1;
	style = ST_CENTER;
	shadow = 3;
	colorBackground[] = {
		0.2,
		0.2,
		0.3,
		0.7
	};
    colorText[] = {1,1,1,1};
	font = "PuristaMedium";
	sizeEx = 0.02;
	text = "";
};
class SKL_ZEUS_DIALOG
{
    idd = 2805;
    movingenabled = true;
    onLoad = "((_this select 0) displayCtrl  2644) ctrlSetText (format ['%1', SKL_ZEUS_THIS]); if (SKL_ZEUS_ALLOW_POSSESSION) then {((_this select 0) displayCtrl 2645) ctrlSetText 'Stop Possess'} else {((_this select 0) displayCtrl 2645) ctrlSetText 'Possess'};";
    
    class Controls
    {
        class SKLZD_Box: SKLZD_BOX
        {
            idc = -1;
            text = ""; 
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 24 * GUI_GRID_H;
        };
        ////////////////////////////////////////////////////////
        // GUI EDITOR OUTPUT START (by SkullTT, v1.063, #Vememo)
        ////////////////////////////////////////////////////////

        class SKLZD_Frame: FHQRscFrame
        {
            idc = 2800;
            text = "Skull's Zeus Menu - v2.1"; 
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 24 * GUI_GRID_H;
            moving = false;
            sizeEx = 0.04;
        };
        class SKLZD_Button1a: FHQRscButton
        {
            idc = -1;
            text = "FhMz"; 
            action = "closeDialog 0; SKL_ZEUS_THIS_ONE call z_fhmz;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button1b: FHQRscButton
        {
            idc = -1;
            text = "Fill Ammobox"; 
            action = "closeDialog 0; SKL_ZEUS_THIS call z_ammoBox;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button2a: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Fortify 20"; 
            action = "[SKL_ZEUS_THIS_ONE,20,'F'] call z_upsmon;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button2b: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Fortify 50"; 
            action = "[SKL_ZEUS_THIS_ONE,50,'F'] call z_upsmon;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button3a: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Road 150"; 
            action = "[SKL_ZEUS_THIS_ONE,150,'R'] call z_upsmon;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button3b: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Road 250"; 
            action = "[SKL_ZEUS_THIS_ONE,250,'R'] call z_upsmon;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button4a: FHQRscButton
        {
            idc = -1;
            text = "UPSMON 75"; 
            action = "[SKL_ZEUS_THIS_ONE,75,''] call z_upsmon;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button4b: FHQRscButton
        {
            idc = -1;
            text = "UPSMON 150"; 
            action = "[SKL_ZEUS_THIS_ONE,150,''] call z_upsmon;";
            x = 12  * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button5a: FHQRscButton
        {
            idc = -1;
            text = "Teleport"; 
            action = "closeDialog 0;SKL_ZEUS_THIS call z_teleport;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 10  * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button5b: FHQRscButton
        {
            idc = -1;
            text = "Teleport Grp"; 
            action = "closeDialog 0;SKL_ZEUS_THIS_ONE call z_teleportGroup;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button6a: FHQRscButton
        {
            idc = -1;
            text = "Suicide Bomber"; 
            action = "{[_x,[],'grenadeHand'] execVM 'scripts\SKULL\suicideBomber.sqf'} forEach SKL_ZEUS_THIS;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button6b: FHQRscButton
        {
            idc = -1;
            text = "Walk"; 
            action = "{_x setSpeedMode 'LIMITED'} forEach SKL_ZEUS_THIS;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button7a: FHQRscButton
        {
            idc = -1;
            text = "Safer Vehicle"; 
            action = "{SKL_SAFE_VEH = _x; publicVariable 'SKL_ZEUS_THIS_ONE'; [SKL_ZEUS_THIS_ONE,'SKL_SAFER_VEHICLES',true,true] spawn BIS_fnc_MP;} forEach SKL_ZEUS_THIS; closeDialog 0;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button7b: FHQRscButton
        {
            idc = -1;
            text = "Smart Static"; 
            action = "SKL_ZEUS_THIS_ONE execVM 'scripts\SKULL\SKL_SmartStaticDefence.sqf';";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button8a: FHQRscButton
        {
            idc = -1;
            text = "Chase"; 
            action = "SKL_ZEUS_THIS_ONE call z_chase;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button8b: FHQRscButton
        {
            idc = -1;
            text = "End Chases"; 
            action = "{_x call z_endChase} forEach SKL_ZEUS_THIS;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button9a: FHQRscButton
        {
            idc = -1;
            text = "Save Scene";
            action = "SKL_ZEUS_THIS call z_saveScene; closeDialog 0;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button9b: FHQRscButton
        {
            idc = -1;
            text = "Appearance";
            action = "closeDialog 0; player call z_changeAppearance;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button10a: FHQRscButton
        {
            idc = -1;
            text = "High Skill";
            action = "{_x setSkill 0.75} foreach (SKL_ZEUS_THIS);";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button10b: FHQRscButton
        {
            idc = -1;
            text = "Low Skill";
            action = "{_x setSkill 0.25} foreach (SKL_ZEUS_THIS);";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button11a: FHQRscButton
        {
            idc = -1;
            text = "--";
            action = "";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 19 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button11b: FHQRscButton
        {
            idc = -1;
            text = "--";
            action = "";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 19 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button12a: FHQRscButton
        {
            idc = -1;
            text = "STOP Grp";
            action = "{doStop _x} forEach units (group SKL_ZEUS_THIS_ONE);";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button12b: FHQRscButton
        {
            idc = 2645;
            text = "?????"; 
            action = "closeDialog 0; SKL_ZEUS_ALLOW_POSSESSION = !SKL_ZEUS_ALLOW_POSSESSION; if (SKL_ZEUS_ALLOW_POSSESSION) then {[] spawn z_possess};";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button13a: FHQRscButton
        {
            idc = -1;
            text = "Hold Fire";
            action = "[SKL_ZEUS_THIS,true] call z_holdFire; closeDialog 0;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button13b: FHQRscButton
        {
            idc = -1;
            text = "Open Fire";
            action = "[SKL_ZEUS_THIS,false] call z_holdFire; closeDialog 0;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 22 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        //////////////////////////////////////////////////////////////////

        class SKLZD_Button1c: FHQRscButton
        {
            idc = -1;
            text = "Invisible Zeus"; 
            action = "[[player,true],'z_hideObject',true,false] spawn BIS_fnc_MP; closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button1d: FHQRscButton
        {
            idc = -1;
            text = "Visible Zeus"; 
            action = "[[player,false],'z_hideObject',true,false] spawn BIS_fnc_MP; closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button2c: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Fortify 100"; 
            action = "[SKL_ZEUS_THIS_ONE,100,'F'] call z_upsmon;closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button2d: FHQRscButton
        {
            idc = -1;
            text = "Remove UPSMON"; 
            action = "SKL_ZEUS_THIS_ONE call z_unupsmon;closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button3c: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Road 350"; 
            action = "[SKL_ZEUS_THIS_ONE,350,'R'] call z_upsmon;closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button3d: FHQRscButton
        {
            idc = -1;
            text = "UPSMON Road 500"; 
            action = "[SKL_ZEUS_THIS_ONE,500,'R'] call z_upsmon;closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button4c: FHQRscButton
        {
            idc = -1;
            text = "UPSMON 250"; 
            action = "[SKL_ZEUS_THIS_ONE,250,''] call z_upsmon;closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button4d: FHQRscButton
        {
            idc = -1;
            text = "UPSMON 350"; 
            action = "[SKL_ZEUS_THIS_ONE,350,''] call z_upsmon;closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button5c: FHQRscButton
        {
            idc = -1;
            text = "Tele Players"; 
            action = "closeDialog 0;SKL_ZEUS_THIS_ONE call z_teleportPlayers;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button5d: FHQRscButton
        {
            idc = -1;
            text = "Tele Zeus"; 
            action = "closeDialog 0;SKL_ZEUS_THIS_ONE call z_teleportZeus;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button6c: FHQRscButton
        {
            idc = -1;
            text = "Land"; 
            action = "SKL_ZEUS_THIS_ONE land 'LAND';";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button6d: FHQRscButton
        {
            idc = -1;
            text = "Low Hover"; 
            action = "SKL_ZEUS_THIS_ONE land 'GET IN';";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button7c: FHQRscButton
        {
            idc = -1;
            text = "Surrender"; 
            action = "{_x call z_surrender} forEach SKL_ZEUS_THIS;closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button7d: FHQRscButton
        {
            idc = -1;
            text = "Done Surrender"; 
            action = "{_x call z_UnSurrender} forEach SKL_ZEUS_THIS;closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button8c: FHQRscButton
        {
            idc = -1;
            text = "Reset Rating"; 
            action = "closeDialog 0;publicVariable 'SKL_ZEUS_THIS_ONE'; ['SKL_ZEUS_THIS_ONE addRating -(rating SKL_ZEUS_THIS_ONE)', 'SKL_DebugCmd', true] call BIS_fnc_MP;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button8d: FHQRscButton
        {
            idc = -1;
            text = "Reset Animation";
            action = "closeDialog 0;publicVariable 'SKL_ZEUS_THIS_ONE'; ['SKL_ZEUS_THIS_ONE switchMove """"', 'SKL_DebugCmd', true] call BIS_fnc_MP;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button9c: FHQRscButton
        {
            idc = -1;
            text = "Earthquake";
            action = "closeDialog 0;[getPos SKL_ZEUS_THIS_ONE,500,42] call bis_fnc_destroyCity;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button9d: FHQRscButton
        {
            idc = -1;
            text = "Smart Static"; 
            action = "SKL_ZEUS_THIS_ONE execVM 'scripts\SKULL\SKL_SmartStaticDefence.sqf'; closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button10c: FHQRscButton
        {
            idc = -1;
            text = "Arty Ambiance"; 
            action = "[120,[SKL_ZEUS_THIS_ONE],1,false,20] execVM 'scripts\SKULL\SKL_ArtyAmbiance.sqf'; closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button10d: FHQRscButton
        {
            idc = -1;
            text = "Arty Assault"; 
            action = "[120,[SKL_ZEUS_THIS_ONE],1,true,20] execVM 'scripts\SKULL\SKL_ArtyAmbiance.sqf'; closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };

        class SKLZD_Button11c: FHQRscButton
        {
            idc = -1;
            text = "Protected Zeus";
            action = "skloz = player; publicVariable 'skloz'; ['skloz setCaptive true; skloz allowDamage false;','SKL_DebugCmd', true] spawn BIS_fnc_MP;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 19 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button11d: FHQRscButton
        {
            idc = -1;
            text = "Unprotected Zeus";
            action = "skloz = player; publicVariable 'skloz'; ['skloz setCaptive false; skloz allowDamage true;','SKL_DebugCmd', true] spawn BIS_fnc_MP;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 19 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button12c: FHQRscButton
        {
            idc = -1;
            text = "--";
            action = "";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button12d: FHQRscButton
        {
            idc = -1;
            text = "Make Editable 1K";
            action = "(getPos SKL_ZEUS_THIS_ONE nearObjects 1000) call z_addObjects; closeDialog 0;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button13c: FHQRscButton
        {
            idc = -1;
            text = "Add DebugMenu"; 
            action = "{_x call SKL_DebugRespawn} forEach SKL_ZEUS_THIS; closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLZD_Button13d: FHQRscButton
        {
            idc = -1;
            text = "DebugMenu";
            action = "closeDialog 0; _handle = CreateDialog 'SKL_DEBUG_DIALOG';";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 22 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        ///////////////////////////////////////////////////////////////
        /////////////////////////  O  K  //////////////////////////////
       
        class SKLZD_PosText: FHQRscActiveText
        {
            type = CT_ACTIVETEXT;
            idc = 2644;
            text = "---"; //--- ToDo: Localize;
            action = "_s = (format ['%1',SKL_ZEUS_THIS]); if (!isServer) then {uiNamespace setVariable ['SKL_ClipBoard',_s]; _handle = CreateDialog 'SKL_CLIPBOARD_DIALOG';} else {copyToClipboard _s; hint 'Unit name copied to clipboard';};";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 20.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
         
        class SKLZD_ButtonOK: FHQRscButton
        {
            idc = 2632;
            text = "OK"; 
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 23.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "closeDialog 0;";
        };
        ////////////////////////////////////////////////////////
        // GUI EDITOR OUTPUT END
        ////////////////////////////////////////////////////////

    };
};    


////////////////////////////////////////////////////////
// Begin FhMz Menu
////////////////////////////////////////////////////////

class SKL_ZEUS_FHMZ_DIALOG
{
    idd = 2900;
    movingenabled = true;
    //onLoad = "";
    
    class Controls
    {
        class SKLZD_FHMZ_Box: SKLZD_BOX
        {
            idc = -1;
            text = ""; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 7 * GUI_GRID_H;
        };
            
        class SKLZD_FHMZ_FRAME: FHQRscFrame
        {
            idc = -1;
            text = "FhMz"; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 7 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TCTR: FHQRscText
        {
            idc = -1;
            text = "Center:"; 
            x = 11.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_X: FHQRscEdit
        {
            idc = 2901;
            text = "0"; 
            x = 15 * GUI_GRID_W + GUI_GRID_X;
            y = 5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_Y: FHQRscEdit
        {
            idc = 2902;
            text = "0"; 
            x = 18.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_Z: FHQRscEdit
        {
            idc = 2903;
            text = "0"; 
            x = 22 * GUI_GRID_W + GUI_GRID_X;
            y = 5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        // class SKLZD_FHMZ_MAPCLICK: FHQRscButton
        // {
            // idc = -1;
            // text = "Map Click"; 
            // action = "closeDialog 0; onMapSingleClick {openCuratorInterface; CreateDialog 'SKL_ZEUS_FHMZ_DIALOG';_display = (findDisplay 2900);(_display displayCtrl 2901) ctrlSetText (_pos select 0);(_display displayCtrl 2902) ctrlSetText (_pos select 1);(_display displayCtrl 2903) ctrlSetText (_pos select 2);  hintSilent ''; openMap false; onMapSingleClick { }; }; [] spawn { sleep 0.5; findDisplay 312 closeDisplay 2; sleep 0.5; showMap true; openMap true;";
            // x = 25.5 * GUI_GRID_W + GUI_GRID_X;
            // y = 5 * GUI_GRID_H + GUI_GRID_Y;
            // w = 4.5 * GUI_GRID_W;
            // h = 1 * GUI_GRID_H;
        // };
        class SKLZD_FHMZ_TSZ: FHQRscText
        {
            idc = -1;
            text = "Size:";
            x = 12.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 2.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_SIZE: FHQRscEdit
        {
            idc = 2904;
            text = "500"; 
            x = 15 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TSD: FHQRscText
        {
            idc = -1;
            text = "Side:"; 
            x = 12.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8 * GUI_GRID_H + GUI_GRID_Y;
            w = 2.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_SIDE: FHQRscCombo
        {
            idc = 2905;
            text = "";
            x = 15 * GUI_GRID_W + GUI_GRID_X;
            y = 8 * GUI_GRID_H + GUI_GRID_Y;
            w = 7.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TT: FHQRscText
        {
            idc = -1;
            text = "Type:";
            x = 20.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 2.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TYPE1: FHQRscEdit
        {
            idc = 2906;
            text = "FHG"; 
            x = 23.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TYPE2: FHQRscEdit
        {
            idc = 2907;
            text = "U";
            x = 27 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TCNT: FHQRscText
        {
            idc = -1;
            text = "Scale:"; //--- ToDo: Localize;
            x = 23.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_COUNT: FHQRscEdit
        {
            idc = 2908;
            text = "10"; //--- ToDo: Localize;
            x = 27 * GUI_GRID_W + GUI_GRID_X;
            y = 8 * GUI_GRID_H + GUI_GRID_Y;
            w = 3 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_TSK: FHQRscText
        {
            idc = -1;
            text = "Skill:"; 
            x = 12.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 2.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_SKILL: FHQRscEdit
        {
            idc = 2909;
            text = "0.5";
            x = 15 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 7.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_OK: FHQRscButton
        {
            idc = -1;
            text =  "OK";
            action = "_side = [east,west,resistance,civilian];[[[ (ctrlText 2906), (ctrlText 2907)],parseNumber (ctrlText 2904), parseNumber (ctrlText 2908),  [[parseNumber (ctrlText 2901),parseNumber (ctrlText 2902),parseNumber (ctrlText 2903)],parseNumber (ctrlText 2904)+600],_side select (lbCurSel 2905),parseNumber (ctrlText 2909)],'SKL_FH_MZ'] call BIS_fnc_MP; closeDialog 0;";
            x = 26 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_FHMZ_CANCEL: FHQRscButton
        {
            idc = -1;
            text = "CANCEL";
            action = "closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
    };
};
////////////////////////////////////////////////////////
// End FhMz GUI
////////////////////////////////////////////////////////
    

////////////////////////////////////////////////////////
// Begin Appearance Menu
////////////////////////////////////////////////////////

class SKL_ZEUS_APPEARANCE_DIALOG
{
    idd = 2950;
    movingenabled = true;
    //onLoad = "";
    
    class Controls
    {
        class SKLZD_APPEAR_Box: SKLZD_BOX
        {
            idc = -1;
            text = ""; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 7 * GUI_GRID_H;
        };
            
        class SKLZD_APPEAR_FRAME: FHQRscFrame
        {
            idc = -1;
            text = "Change Appearance"; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 7 * GUI_GRID_H;
        };

        class SKLZD_APPEAR_TSD: FHQRscText
        {
            idc = -1;
            text = "Appearance:"; 
            x = 12.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_APPEAR_SIDE: FHQRscCombo
        {
            idc = 2951;
            text = "";
            x = 18 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 12 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_APPEAR_OK: FHQRscButton
        {
            idc = -1;
            text =  "OK";
            action = "(lbCurSel 2951) call z_setAppearance; closeDialog 0;";
            x = 26 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_APPEAR_CANCEL: FHQRscButton
        {
            idc = -1;
            text = "CANCEL";
            action = "closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
    };
};
////////////////////////////////////////////////////////
// End Appearance GUI
////////////////////////////////////////////////////////
	
    
////////////////////////////////////////////////////////
// Begin Side Menu
////////////////////////////////////////////////////////

class SKL_ZEUS_SIDE_DIALOG
{
    idd = 2960;
    movingenabled = true;
    //onLoad = "";
    
    class Controls
    {
        class SKLZD_SIDEGUI_Box: SKLZD_BOX
        {
            idc = -1;
            text = ""; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 5 * GUI_GRID_H;
        };
            
        class SKLZD_SIDEGUI_FRAME: FHQRscFrame
        {
            idc = -1;
            text = "Choose Side"; 
            x = 11.05 * GUI_GRID_W + GUI_GRID_X;
            y = 4.08 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 5 * GUI_GRID_H;
        };
        class SKLZD_SIDEGUI_TSD: FHQRscText
        {
            idc = -1;
            text = "Side:"; 
            x = 12.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 2.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_SIDEGUI_SIDE: FHQRscCombo
        {
            idc = 2910;
            text = "";
            x = 15 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 7.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };

        class SKLZD_SIDEGUI_OK: FHQRscButton
        {
            idc = -1;
            text =  "OK";
            action = "_side = [east,west,resistance,civilian];SKL_ZEUS_SIDE = _side select (lbCurSel 2910); closeDialog 0;";
            x = 26 * GUI_GRID_W + GUI_GRID_X;
            y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
        class SKLZD_SIDEGUI_CANCEL: FHQRscButton
        {
            idc = -1;
            text = "CANCEL";
            action = "SKL_ZEUS_SIDE = sideLogic; closeDialog 0;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
        };
    };
};
////////////////////////////////////////////////////////
// End Side GUI
////////////////////////////////////////////////////////
