// SKL_RatingMinimum.sqf
//
// Will keep players from becoming enemy due to rating
//
// place in init.sqf:    execVM "scripts\SKL_RatingMinimum.sqf";
//
// Version 1.0  2-13-14
// 1.0: initial release

SKL_MinRatingReset = compileFinal "_this addRating -(rating _this)";
if (!isServer) exitWith{};

while {true} do
{
    {
        _rating = rating _x;
        if (_rating < -1500) then
        {
            [["ScoreAdded",[format ["%1 has negative rating.  Adjusting it up to zero",name _x],_rating]],"BIS_fnc_ShowNotification",nil] spawn BIS_fnc_MP;
            [_x,"SKL_MinRatingReset"] call BIS_fnc_MP ;
        };
        sleep 0.1;
    } forEach playableUnits;
    sleep 15;
};
