// SKL_SmartStaticDefence
//
// Script will cause a static defence gunner to exit his static vehicle and defend himself if enemy get close
//  and the closest enemy is on foot.
//
// Usage: in manned static defence init:  _nil = this execVM "scripts\SKL_SmartStaticDefence.sqf"

sleep 3;

_veh = _this;
if !(local _veh) exitWith {};

sleep 2;

waitUntil {!isNull gunner _veh};
_gunner = gunner _veh;

while {(canMove _veh) && (alive _gunner)} do
{ 
    _enemy = _gunner findNearestEnemy _gunner;
    if (!(_enemy isKindOf "Man")) then {_enemy = objNull;};
    if ((alive _gunner) && (_gunner distance _enemy < 50)) then
    {  
        while {(alive _gunner) && (_gunner distance (_gunner findNearestEnemy _gunner) < 110)} do
        {
            if (vehicle _gunner != _gunner) then 
            {        
                unassignVehicle _gunner; 
                _gunner action ["GetOut",_veh];
            };
            sleep 10;
        };
        _gunner assignAsGunner _veh;
    };
    if (_gunner == vehicle _gunner) then { _gunner action ["GetInGunner", _veh]; };
    sleep 10;
};

if ((!canMove _veh) && (alive _gunner)) then
{
    _gunner action ["GetOut",_veh];
};
