// SKL_PlayerMenu.sqf
//
// Settings are client side, so can be set for each player
//
// Enemy Markers can be forced off with: SKL_PM_ALLOW_ENEMY = false;
//
// Type of map markers can be forece with: SKL_PM_FORCE_MARKER = "NONE"];
// use one of: "NONE" "SELF" "TEAM" "TEAM_N" "SQUAD" "SQUAD_N" "SIDE" "FRIEND"
// or SKL_PM_FORCE_MARKER = ""; to return to allowing user selection
//
// The entire player menu can be blocked with: SKL_PM_ALLOW_PLAYERMENU = false; 
//

/* choose one of: "NONE" "SELF" "TEAM" "TEAM_N" "SQUAD" "SQUAD_N" "SIDE" "FRIEND" */
skl_pm_default_map_markers = "SQUAD"; 
    
/* missions may force this disabled, but if allowed, what is your preference? (1=show, 0=hide)*/
skl_pm_default_show_enemy = 0; 

/* personalMenuKeys */
skl_key = 219; 
skl_shift = 1; 
skl_ctrl = 0; 
skl_alt = 0; 

//////////// DO NOT EDIT BELOW /////////////////
if (isNil "SKL_PM_ALLOW_ENEMY") then {SKL_PM_ALLOW_ENEMY = true}; 
if (isNil "SKL_PM_FORCE_MARKER") then {SKL_PM_FORCE_MARKER = ''}; 
if (isNil "SKL_PM_ALLOW_PLAYERMENU") then {SKL_PM_ALLOW_PLAYERMENU = true}; 

////////////////////////////////////////////////

skl_pm_default_map_markers call SKL_SetMarkerType; 
skl_pm_default_show_enemy call SKL_ShowEnemyMarkers; 

SKLMenu_MAIN = [ 
    ["Player Menu", true],
    ["Group Controls",         [2], "#USER:SKLMenu_GROUP_CONTROL", -5, [["expression", ""]], "1", "1"],
    ["Map Markers",            [3], "#USER:SKLMenu_TEAM_MARKERS",  -5, [["expression", ""]], if (difficultyEnabled "Map") then {"0"} else {"1"}, "1"],
    ["Cancel",                [11], "", -3, [["expression", ""]], "1", "1"]
]; 

SKLMenu_PlayerMenu_Main = { 
    if (SKL_PM_ALLOW_PLAYERMENU) then { 
        call SKL_PlayerMenu_AllowEnemy; 
        call SKL_PlayerMenu_JoinGroup; 
        showCommandingMenu "#USER:SKLMenu_MAIN"; 
    } 
}; 

////////////////////////////////////////////////
SKLMenu_GROUP_CONTROL = [
    ["Group Control", true],
    ["Set Your Team Color",    [2], "#USER:SKLMenu_TEAM_COLOR",  -5, [["expression", ""]], "1", "1"],
    ["Join Nearby Team",       [3], "#USER:SKL_PlayerMenu_JOIN", -5, [["expression", ""]], "1", "1"],
    ["Leave Your Team",        [4], "", -5, [["expression", "[] spawn SKLMenu_LeaveTeam"]], "1", "1"],
    ["Take Command of Team",   [5], "", -5, [["expression", "[] spawn SKLMenu_TakeCommand"]], "1", "1"],
    ["Back",                  [11], "", -4, [["expression", ""]], "1", "1"]
]; 

SKLMenu_LeaveTeam = {[player] joinSilent (createGroup side player)}; 
SKLMenu_TakeCommand = {[ [[group player, player], {(_this select 0) selectLeader (_this select 1)}], "BIS_fnc_spawn", true] spawn BIS_fnc_MP}; 

////////////////////////////////////////////////
SKL_PlayerMenu_JoinGroup = { 
    private ["_menu","_id","_list"]; 
    _id = 2; 
    _list = if (vehicle player == player) then { 
        (nearestObjects [player, ["Man"], 8] - [player]) 
    } else { 
        (crew (vehicle player)) - [player] 
    }; 
    SKL_PlayerMenu_TargetGroup = []; 
    SKL_PlayerMenu_JOIN = [["Join Group", true]]; 
    { 
        if (/*(group player != group _x) &&*/ (name _x !=  "Error: No unit")) then 
        { 
            SKL_PlayerMenu_JOIN = SKL_PlayerMenu_JOIN + 
                 [[name _x, [_id], "", -5, [["expression", format ["[player] joinSilent (SKL_PlayerMenu_TargetGroup select %1);",_id-2]]], "1", "1"]]; 
            _id = _id + 1; 
            SKL_PlayerMenu_TargetGroup = SKL_PlayerMenu_TargetGroup + [group _x]; 
        } 
    } forEach _list; 
    SKL_PlayerMenu_JOIN = SKL_PlayerMenu_JOIN + [["Back",   [11], "", -4, [["expression", ""]], "1", "1"]]; 
}; 

////////////////////////////////////////////////
SKLMenu_TEAM_COLOR = [ 
    ["Set Team Color", true],
    ["Red",    [2], "", -5, [["expression", "[player,'RED']    spawn SKL_PlayerMenu_DoTeamColor"]], "1", "1"],
    ["Green",  [3], "", -5, [["expression", "[player,'GREEN']  spawn SKL_PlayerMenu_DoTeamColor"]], "1", "1"],
    ["Blue",   [4], "", -5, [["expression", "[player,'BLUE']   spawn SKL_PlayerMenu_DoTeamColor"]], "1", "1"],
    ["Yellow", [5], "", -5, [["expression", "[player,'YELLOW'] spawn SKL_PlayerMenu_DoTeamColor"]], "1", "1"],
    ["White",  [6], "", -5, [["expression", "[player,'MAIN']   spawn SKL_PlayerMenu_DoTeamColor"]], "1", "1"],
    ["Back",  [11], "", -4, [["expression", ""]], "1", "1"]
]; 

SKL_PlayerMenu_DoTeamColor = {[[ _this,{(_this select 0) assignTeam (_this select 1)}],"BIS_fnc_spawn", true] spawn BIS_fnc_MP}; 

////////////////////////////////////////////////
SKL_PlayerMenu_AllowEnemy = { 
    _i = if (SKL_PM_FORCE_MARKER == "") then {"1"} else {"0"}; 
    SKLMenu_TEAM_MARKERS = 
    [ 
        ["Set Map Markers", true],
        ["None",                  [2], "", -5, [["expression", "'NONE'    call SKL_SetMarkerType"]], "1", _i],
        ["Self",                  [3], "", -5, [["expression", "'SELF'    call SKL_SetMarkerType"]], "1", _i],
        ["Team",                  [4], "", -5, [["expression", "'TEAM'    call SKL_SetMarkerType"]], "1", _i],
        ["Team Named",            [5], "", -5, [["expression", "'TEAM_N'  call SKL_SetMarkerType"]], "1", _i],
        ["Squad",                 [6], "", -5, [["expression", "'SQUAD'   call SKL_SetMarkerType"]], "1", _i],
        ["Squad Named",           [7], "", -5, [["expression", "'SQUAD_T' call SKL_SetMarkerType"]], "1", _i],
        ["Squad (Leaders Named)", [8], "", -5, [["expression", "'SQUAD_N' call SKL_SetMarkerType"]], "1", _i],
        ["Nearby Side",           [9], "", -5, [["expression", "'SIDE'    call SKL_SetMarkerType"]], "1", _i],
        ["Nearby Friendly",      [10], "", -5, [["expression", "'FRIEND'  call SKL_SetMarkerType"]], "1", _i],
        ["--",                    [0], "", -1, [["expression", ""]], "1", "1"],
        ["Enemy Markers",        [11], "#USER:SKLMenu_ENEMY_MARKERS", -5, [["expression", ""]], if (SKL_PM_ALLOW_ENEMY) then {"1"} else {"0"}, _i],
        ["Back",                 [12], "", -4, [["expression", ""]], "1", "1"]
    ]; 
}; 
    
////////////////////////////////////////////////
SKLMenu_ENEMY_MARKERS = 
[ 
    ["Set Enemy Map Markers", true],
    ["Hide",        [2], "", -5, [["expression", "0 call SKL_ShowEnemyMarkers"]], "1", "1"],
    ["Show",        [3], "", -5, [["expression", "1 call SKL_ShowEnemyMarkers"]], "1", "1"],
    ["Back",       [11], "", -4, [["expression", ""]], "1", "1"]
]; 

////////////////////////////////////////////////

_key = skl_key; 
if (_key == 0) then {_key = 220}; 
_shift = skl_shift == 1; 
_ctrl  = skl_ctrl == 1; 
_alt   = skl_alt == 1; 

SKLMenu_Keys = [_key,[_shift,_ctrl,_alt]]; 
SKLMenu_KeyHandler = { 
    _handled = false; 
    if ((SKLMenu_Keys select 0) == (_this select 1)) then { 
        if ((SKLMenu_Keys select 1) isEqualTo [(_this select 2),(_this select 3),(_this select 4)]) then { 
            [] spawn SKLMenu_PlayerMenu_Main; 
            _handled = true; 
        }; 
    }; 
    _handled 
}; 

waitUntil {!isNull(findDisplay 46)}; 
(findDisplay 46) displayAddEventHandler ["KeyDown","_this call SKLMenu_KeyHandler;"]; 