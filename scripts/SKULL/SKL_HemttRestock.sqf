// script keeps hemtt vehicles stocked
//
// In vehicle init:  _nil = this execVM "scripts\SKULL\SKL_HemttRestock.sqf";
//
// Version 1.0  2-13-14
// 1.0: initial release

private ["_type","_items","_loc","_firstAidCount","_error","_ammoTrucks","_fuelTrucks","_repairTrucks","_medicalTrucks","_truckType","_delay","_counter","_increment","_addMedKits","_sleep"];

if (isDedicated) exitWith {};

_type = toUpper (typeOf _this); // switch to upper case so we can be case insensitive
_error = false;
_truckType = 0;
_counter = 1;
_delay = 1;
_increment = false;
_sleep = 20;

// USE UPPERCASE
_ammoTrucks =   ["B_TRUCK_01_AMMO_F"    , "O_TRUCK_02_AMMO_F"   , "I_TRUCK_02_AMMO_F"   ];  
_fuelTrucks =   ["B_TRUCK_01_FUEL_F"    , "O_TRUCK_02_FUEL_F"   , "I_TRUCK_02_FUEL_F"   , "C_VAN_01_FUEL_F", "B_G_VAN_01_FUEL_F"];    
_repairTrucks = ["B_TRUCK_01_REPAIR_F"  , "O_TRUCK_02_BOX_F"    , "I_TRUCK_02_BOX_F"    ];    
_medicalTrucks= ["B_TRUCK_01_MEDICAL_F" , "O_TRUCK_02_MEDICAL_F", "I_TRUCK_02_MEDICAL_F"];  

switch true do
{
        case (_type in _ammoTrucks)   : { _truckType = 1; _delay = 20}; // delay in seconds
        case (_type in _fuelTrucks)   : { _truckType = 2; _delay = 240}; 
        case (_type in _repairTrucks) : { _truckType = 3; _delay = 360}; 
        case (_type in _medicalTrucks): { _truckType = 4; _delay = 120}; 
        default {hint format ["Error in HemttRestock.sqf for vehicle %1:%2",_this,_type]; _error = true;};
};
_delay = _delay / _sleep; // convert to loop counts
while {(alive _this) && !_error} do
{
    sleep _sleep;
    waitUntil {local _this};
    _increment = false;
    switch (_truckType) do
    {
        case (1): {if (getAmmoCargo _this < 1) then {if (_counter >= _delay) then {_this setAmmoCargo 1;} else {_increment = true;}; }; };
        case (2): {if (getFuelCargo _this < 1) then {if (_counter >= _delay) then {_this setFuelCargo 1;} else {_increment = true;}; }; };
        case (3): {if (getRepairCargo _this < 1) then {if (_counter >= _delay) then {_this setRepairCargo 1;} else {_increment = true;}; }; };
        case (4): 
        {                                                                                         
            // stock up on first aid kits
            _firstAidCount = 10;
            _addMedKits = 0;
            _items = getItemCargo _this;
            _loc = (_items select 0) find "FirstAidKit";
            if (_loc > -1) then
            {
                _count = ((_items select 1) select _loc);
                if ( _count < _firstAidCount) then
                {
                    _addMedKits = _firstAidCount - _count;               
                };
            }
            else
            {
                _addMedKits = _firstAidCount;
            };
            if (_addMedKits > 0) then
            {
                if (_counter >= _delay) then {
                    _this addItemCargo ["FirstAidKit",_addMedKits];
                }
                else
                {
                    _increment = true;
                };
            };
        };
        default {_error = true;};
    };
    if (_increment) then { _counter = _counter + 1; } else {_counter = 1;};
};