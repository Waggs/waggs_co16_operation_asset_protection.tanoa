/*
find_medBox_land.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/
private["_dropbox","_c"]; 

_dropbox = _this select 0; 
_c = 0; 

while {({_x distance _dropbox > 50} count playableUnits >0 ) && (_c == 0)} do { 
    if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
        if !(isNil "_lightLand") then {deleteVehicle _lightLand}; 
        _lightLand	=  "Chemlight_Red" createVehicle position _dropbox; 
        _smokeLand	=  "SmokeShellRed" createVehicle position _dropbox; 
        _smokeLand attachTo [_dropbox,[0,0,0]]; 
        sleep 50; 
        if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
            sleep 50; 
            _smokeLand	=  "SmokeShellRed" createVehicle position _dropbox; 
            _smokeLand attachTo [_dropbox,[0,0,0]]; 
            _flareLand	= "F_40mm_red" createVehicle position _dropbox; 
            _flareLand attachTo [_dropbox,[0,0,0]]; 
            sleep 50; 
            if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
                sleep 50; 
                deleteVehicle _lightLand; 
            }; 
        }; 
    }; 
    if ({_x distance _dropbox < 50} count playableUnits >0 ) then { 
        _c = 1; 
    }; 
}; 