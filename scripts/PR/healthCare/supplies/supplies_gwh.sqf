/*
  supplies_gwh.sqf
  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/
0 = [] spawn { 
    waitUntil {!isNull findDisplay 46}; 
    findDisplay 46 displayAddEventHandler ["KeyDown", { 
        if (_this select 1 in actionKeys "Gear") then [{ 
            _tents = nearestObjects [player modelToWorld [0,2,0], ["Land_TentDome_F"], 2]; 
            if (count _tents > 0) then 
            [{ 
                _tent = _tents select 0; 
                medHolder = mash_man_tent getVariable ["medHolder", objNull]; 
                if (isNull medHolder) then { 
                    medHolder = createVehicle ["GroundWeaponHolder", [0,0,0], [], 0, "NONE"]; 
                    medHolder attachTo [mash_man_tent, [0,0,0]]; 
                    medHolder setVectorUp vectorUp mash_man_tent; 
                    mash_man_tent setVariable ["medHolder", medHolder, true]; 
                    detach medHolder; 
                    medHolder addItemCargoGlobal [aceBandage, 18]; 
                    medHolder addItemCargoGlobal [aceMorphine, 10]; 
                    medHolder addItemCargoGlobal [aceEpi, 8]; 
                    medHolder addItemCargoGlobal [aceBlood1000, 6]; 
                    medHolder addMagazineCargoGlobal [smoke, 5]; 
                }; 
                player action ["Gear", medHolder]; 
                true 
            },{false}]; 
        },{false}]; 
    }]; 
}; 
