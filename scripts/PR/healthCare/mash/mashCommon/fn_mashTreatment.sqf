/* 
*  Author: PapaReap 
*  Function names: pr_fnc_mashTreatment 
*  Called upon from fn_fmBuild.sqf & fn_mashTentCreate.sqf 
*  Restores blood to player at given distance 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*  ver 1.1 - 2016-01-02 system rework in progress 
*  ver 1.2 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <OBJECT>  --- Object the script shall be attached to
*  1: <RADIUS>  --- Radius around the object in which units blood is restored 
*  2: <BLOOD>   --- Amount of blood restored per "tick" 
*  3: <LENGHT>  --- Length of one "tick" in seconds 
*
*  Can also be used through objects init: 0 = [this, 5, 1, 1.5] execVM "scripts\PR\healthCare\mash\mashCommon\fn_mashTreatment.sqf" 
*  If spawned use: 
*  [[_fieldMash, 5, 1, 1.5], "pr_fnc_mashTreatment"] call BIS_fnc_MP; 
*  The example above heals every 1.5 seconds 1% health of each unit that is within 5 meters to the Field Mash. 
*/ 

if !(aceOn) exitWith {}; 
//fnc_mashTreatment = { 
    if ((isDedicated) || !(hasInterface)) exitWith {}; 
    if (player == player) then { 
        private ["_obj","_radius","_healPerSleep","_damage","_blood","_sleepTime"]; 
        _meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 
        _obj           = _this select 0; 
        _radius        = _this select 1; 
        _healPerSleep  = _this select 2; 
        _sleepTime     = _this select 3; 
        _container     = _this select 4; 
        if (_container == mash_man_tent) then { mash_man_tent setVariable ["bloodHolder", 0, true] }; 
        _unit = player; 
        _count = 0; 
        scopename "healScope"; 
        while { true } do { 
            if (_unit distance _obj <= _radius) then { 
                if (_meds == 0) then { 
                    if ((_unit getVariable "bloodHolder" == 1) || (_container getVariable "bloodHolder" == 1)) then { 
                        _unit setVariable ["aceBloodBag", 1, true]; 
                        _blood = [_unit] call fnc_blood; 
                        _blood2 = ((_blood + _healPerSleep) Min 100); 
                        _unit setVariable ["ace_medical_bloodVolume", _blood2, true]; 
                        if (_unit getVariable ["ACE_isUnconscious", true]) then { 
                            if (([_unit] call fnc_blood == 100) && ((_unit getVariable "ace_medical_pain") == 0)) then { [_unit] call fnc_wakeUp }; 
                        }; 
                        if ([_unit] call fnc_blood < 100) then { _bloodGiven = _blood2 - _blood; _count = _count + _bloodGiven }; 
                    } else { 
                        _unit setVariable ["aceBloodBag", 0, true]; 
                    }; 
                } else { 
                    _blood = [_unit] call fnc_blood; 
                    _blood2 = ((_blood + _healPerSleep) Min 100); 
                    _unit setVariable ["ace_medical_bloodVolume", _blood2, true]; 
                    if (_unit getVariable ["ACE_isUnconscious", true]) then { 
                        if (([_unit] call fnc_blood == 100) && ((_unit getVariable "ace_medical_pain") == 0)) then { [_unit] call fnc_wakeUp }; 
                    }; 
                }; 
            }; 
            if (_meds == 0) then { 
                [[_count, _container], "pr_fnc_bloodCounter"] call BIS_fnc_MP; 
                _count = 0; 
                if (isNull _container) then { breakto "healScope" }; 
            }; 
            sleep _sleepTime; 
			pr_obj2 = _obj; pr_obj3 = _container; // delete after testing
        }; 
    }; 
//}; 
/*
if (isNil "bloodCounter") then { bloodCounter = true; publicVariable "bloodCounter"; server setVariable ["bloodCount", 0, true] }; 
fnc_bloodCounter = { 
    if !(isServer) exitWith {}; 
    scopename "healScope"; 
    _cnt = _this select 0; 
    _container = _this select 1; 
    if !(_container == mash_man_tent) then { 
        if (isNull _container) then { breakto "healScope" }; 
        _count = _container getVariable "bloodCount"; 
        _count = _count + _cnt; 
        _container setVariable ["bloodCount", _count, true]; 
        if (_container getVariable "bloodCount" > 99) then { 
            _count = 0; // delete after testing 
            _container setVariable ["bloodCount", 0, true]; 
            _container setVariable ["removeBlood", 1, true]; 
        }; pr_count = _count; publicVariable "pr_count"; // delete after testing 
    } else { 
        if (isNull mash_man_tent) then { breakto "healScope" }; 
        _count = server getVariable "bloodCount"; 
        _count = _count + _cnt; 
        server setVariable ["bloodCount", _count, false]; 
        if (server getVariable "bloodCount" > 99) then { 
            _count = 0; 
            server setVariable ["bloodCount", 0, false]; removeBloodBag = true; publicVariable "removeBloodBag"; 
        }; pr_count = _count; publicVariable "pr_count"; // delete after testing 
    }; 
}; 



fnc_crateBloodInv = { 
    _container = _this select 0; 
    while { !(isNull _container) } do { 
        if ((_container getVariable "removeBlood" == 1) && !(isNull _container)) then { 
            _bloodbags = 0; 
            _items = []; 
            _bloodbags = (({"ACE_bloodIV" == _x} count (itemCargo _container)) - 1) max 0; 
            _items = (itemCargo _container) - ["ACE_bloodIV"]; 
            clearItemCargoGlobal _container; 
            for "_i" from 0 to (count _items) do { _container addItemCargoGlobal [(_items select _i), 1]; }; 
            _container addItemCargoGlobal ["ACE_bloodIV", _bloodbags]; 
            sleep 0.05; 
            _container setVariable ["removeBlood", 0, true]; 
        }; 
        sleep 2; 
    }; 
}; 

fnc_medHolder = { 
    _unit = _this select 0; 
    _unit setVariable ["bloodHolder", 0, true]; 
    _meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 
    if (_meds == 0) then { 
        while { true } do { 
            if !(isNull medHolder) then { 
                if ("ACE_bloodIV" in ItemCargo medHolder) then { _unit setVariable ["bloodHolder", 1, true]; } else { _unit setVariable ["bloodHolder", 0, true]; }; 
            } else { 
                _unit setVariable ["bloodHolder", 0, true]; 
            }; 
            sleep 2; 
        }; 
    }; 
}; 

fnc_bloodCargo = { 
    _container = _this select 0; 
    _meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 
    if (_meds == 0) then { 
        scopename "bloodScope"; 
        while { true } do { 
            if !(isNull _container) then { 
                if ("ACE_bloodIV" in ItemCargo _container) then { _container setVariable ["bloodHolder", 1, true] } else { _container setVariable ["bloodHolder", 0, true] }; 
            } else { 
                _container setVariable ["bloodHolder", 0, true]; 
                breakto "bloodScope"; 
            }; 
            sleep 2; 
        }; 
    }; 
}; 

fnc_restoreSpeech = { 
    if (isServer) then { 
        while { true } do { 
            { if (alive _x) then { if (_x getVariable ["ACE_isUnconscious", true]) then { _x setVariable ["tf_voiceVolume", 0.6, true] }; }; } forEach playableUnits; sleep 4; 
        }; 
    }; 
}; 
*/
//waitUntil { time > 0 }; 
//[player] spawn fnc_medHolder; 
