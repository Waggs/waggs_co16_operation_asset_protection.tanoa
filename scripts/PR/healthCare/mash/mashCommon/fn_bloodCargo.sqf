/* 
*  Author: PapaReap 
*  Function names: pr_fnc_bloodCargo 
*  Called from pr_fn_fmBuild.sqf" 
*  Sets variable if container has blood in it 
*  ver 1.0 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <CONTAINER>  --- Container to check for blood bags  
*/ 
if !(aceOn) exitWith {}; 
_container = _this select 0; 
_meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 

if (_meds == 0) then { 
    scopename "bloodScope"; 
    while { true } do { 
        if !(isNull _container) then { 
            if ("ACE_bloodIV" in ItemCargo _container) then { _container setVariable ["bloodHolder", 1, true] } else { _container setVariable ["bloodHolder", 0, true] }; 
        } else { 
            _container setVariable ["bloodHolder", 0, true]; 
            breakto "bloodScope"; 
        }; 
        sleep 2; 
    }; 
}; 
