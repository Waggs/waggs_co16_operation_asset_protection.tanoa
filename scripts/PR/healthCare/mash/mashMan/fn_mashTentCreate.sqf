/* deploy_mash_man.sqf
*  Author: PapaReap 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*/ 
if !(aceOn) exitWith {}; 
private ["_deployer","_pos_deployer","_dir_deployer","_tent"]; 
_deployer  = _this select 0; 
_time  = mash_Array select 11; 
_deployer setVariable ["tentAbort", 0, false]; 
[_deployer] spawn fnc_deployTent; 
sleep (_time + 1); 

if (_deployer getVariable "tentAbort" == 1) then { 
    _pos_deployer   = getPosATL _deployer; 
    _dir_deployer   = getDir _deployer; 
    mash_man_tent   = objNull; 
    _tent           = "Land_TentDome_F"; 
    mash_man_tent   = _tent createVehicle [(_pos_deployer select 0) + (sqrt (4) * cos (100 - _dir_deployer)), (_pos_deployer select 1) + (sqrt (4) * sin (100 - _dir_deployer)) , 0]; 
    mash_man_tent enableSimulation false; 
    mash_man_tent allowDamage false; 
    mash_man_tent setVariable ["ace_isMedicalFacility", true, true];    //mash_man_tent getVariable "ace_isMedicalFacility"; //for check
    _deployer setVariable ["mash", 2, false]; 
    sleep 0.5; 
    publicVariable "mash_man_tent"; 

/*    if (getMarkerColor "medDrop" == "") then { _marker = createMarker ["medDrop", position _deployer] } else { "medDrop" setMarkerPos getPos _deployer }; 

    if (getMarkerColor "Mash" == "") then { 
        _markerName = format ["Mash"]; 
        createMarker [_markerName, position _deployer]; 
        _markerName setMarkerColor "ColorGreen"; 
        _markerName setMarkerType "loc_Hospital"; 
        _markerName setMarkerText "Mash"; 
        _markerName setMarkerSize [1, 1]; 
        _markerName setMarkerDir -0; 
    } else { 
        "Mash" setMarkerPos getPos _deployer; 
        "Mash" setMarkerAlpha 1; 
    }; */

    ["scripts\PR\HealthCare\supplies\supplies_gwh_empty.sqf","BIS_fnc_execVM",true,true ] call BIS_fnc_MP; 
    [[mash_man_tent, 5, 1, 1.5, mash_man_tent], "pr_fnc_mashTreatment"] call BIS_fnc_MP; 
    [[mash_man_tent, 5, 1], "pr_fnc_mashTreatmentClient"] call BIS_fnc_MP; 
    [[[],"scripts\PR\HealthCare\supplies\take_supplies.sqf"],"BIS_fnc_execVM",true,true] call BIS_fnc_MP; 

    mash_man_deployed = true; publicVariable "mash_man_deployed"; 
    _deployer removeAction mash_man_deployAction; 
    _deployer setVariable ["tentAbort", 0, false]; 
} else { 
    if (_deployer getVariable "tentAbort" == 2) then { mash_man_deployed = false; publicVariable "mash_man_deployed"; _deployer setVariable ["tentAbort", 0, false]; }; 
}; 
