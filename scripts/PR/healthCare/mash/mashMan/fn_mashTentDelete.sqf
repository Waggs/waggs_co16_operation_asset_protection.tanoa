/*
fn_mashTentDelete.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/ 
if !(aceOn) exitWith {}; 
private ["_deployer"]; 
_deployer  = _this select 0; 
_time  = mash_Array select 11; 

_deployer setVariable ["stowAbort", 0, false]; //need to figure this one out
[_deployer] spawn fnc_stowTent; 
sleep (_time + 1); 

if (_deployer getVariable "stowAbort" == 1) then { 
    deleteVehicle mash_man_tent; 
    mash_man_tent = objNull; 
    mash_man_deployed = false; publicVariable "mash_man_deployed"; 
    mash_reset = true; publicVariable "mash_reset"; 
    if (!isNil "getDropAction") then { 
        mash_man_tent removeAction getDropAction; 
    }; 
    if (!isNil "placeMedsAction") then { 
        mash_man_tent removeAction placeMedsAction; 
    }; 
    _deployer removeAction mash_man_stowAction; 
    _deployer setVariable ["stowAbort", 0, false]; 
    "Mash" setMarkerAlpha 0; 
    if !(isNil "medHolder") then { 
        deleteVehicle medHolder; 
    }; 
} else { 
    if (_deployer getVariable "stowAbort" == 2) then { 
        _deployer setVariable ["stowAbort", 0, false]; 
    }; 
}; 