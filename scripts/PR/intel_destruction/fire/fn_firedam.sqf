// by ALIAS
// damage inflicted by fire

_damage = 0.01; 
_new_damage = 0; 

while { fireexpunere } do { 
    _new_damage = ((damage player) + _damage); 
    enableCamShake true; 
    addCamShake [5, 1, 17]; 
    _damage = _damage + 0.02; 
    player setdammage _new_damage; 
    playsound "burned"; 
    sleep 2.5 + random 1; 
    enableCamShake false; 
}; 
