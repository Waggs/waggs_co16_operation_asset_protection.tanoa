/*
=======================================================================================================================

	UploadData - script to upload data from a laptop and because of this complete a task (as example)

	File:		uploadData.sqf
	Author:		T-800a

=======================================================================================================================
*/

T9_varFileSize = 32875;  								// Filesize ... smaller files will take shorter time to Upload!

T9_varTLine01 = "Upload aborted!";								
T9_varTLine02 = "Upload already in progress by someone else!";
T9_varTLine03 = "Upload started!";
T9_varTLine04 = "Upload finished!";
T9_varTLine05 = "##  Upload Data  ##";				// line for the addaction

T9_varDiagAbort = false;
T9_varDownSucce = false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

T9_fnc_abortActionLaptop =
{
	if ( T9_varDownSucce ) then 
	{
		// hint "DEBUG - DONE";
		T9_varDiagAbort = false;
		T9_varDownSucce = false;		
	
	} else { 
		player sideChat T9_varTLine01; 
		T9_varDiagAbort = true; 
		T9_varDownSucce = false; 
	};
};


T9_fnc_addActionLaptop =
{
	{
		private [ "_UT", "_cUT" ];
		_UT = format [ "%1_%2", "T9_pubVarDataTask", _x ];
		_cUT = missionNamespace getVariable [ _qV, false ];
		if ( _cUT ) exitWith {};
		_x addAction ["<t color=""#82C7FF"">" + T9_varTLine05, { call T9_fnc_ActionLaptop; }, [], 10, false, false ];
	} forEach _this;	
};


T9_fnc_removeActionLaptop =
{
	_radio = _this select 0;
	_is = _this select 1;
	_radio removeAction _is;
};


T9_fnc_ActionLaptop =
{
	private [ "_radio", "_sender", "_is", "_eIU", "_UT", "_IZ" ];
	_radio = _this select 0;
	_sender = _this select 1;
	_is = _this select 2;

	_UT = format [ "%1_%2", "T9_pubVarDataTask", _radio ];
	_IZ = format [ "%1_%2", "T9_pubVarInUse", _radio ];
	
	_eIU = missionNamespace getVariable [ _IZ, false ];
	if ( _eIU ) exitWith { player sideChat T9_varTLine02; };
	
	player sideChat T9_varTLine03;
	
	missionNamespace setVariable [ _IZ, true ];
	publicVariable _IZ;
		
	[ _radio, _is, _IZ, _UT ] spawn 
	{
		private [ "_radio", "_is", "_newupload", "_ulRate" ];
		
		_radio		= _this select 0;
		_is			= _this select 1;
		_IZ			= _this select 2;
		_UT			= _this select 3;
		
		_newupload = 0;
		
		createDialog "T9_DataDownloadDialog";
		
		sleep 0.5;
		ctrlSetText [ 8011, "Connecting ..." ];
		sleep 0.5;
		ctrlSetText [ 8011, "Connected:" ];		
		ctrlSetText [ 8013, format [ "%1 kb", T9_varFileSize ] ];		
		ctrlSetText [ 8014, format [ "%1 kb", _newupload ] ];		
		
		while { !T9_varDiagAbort } do
		{
			_ulRate = 200 + random 80;
			_newupload = _newupload + _ulRate;

			if ( _newupload > T9_varFileSize ) then 
			{
				_ulRate = 0;		
				_newupload = T9_varFileSize;
				ctrlSetText [ 8011, "Upload finished!" ];	
				T9_varDiagAbort = true;
				player sideChat T9_varTLine04;
				T9_varDownSucce = true;
				
				missionNamespace setVariable [ _UT, true ];
				publicVariable _UT;				

				[ [ _radio, _is ], "T9_fnc_removeActionLaptop", true, true ] spawn BIS_fnc_MP;
				UploadDestroy = true; publicVariable "UploadDestroy";
			};
			
			ctrlSetText [ 8012, format [ "%1 kb/t", _ulRate ] ];		
			ctrlSetText [ 8014, format [ "%1 kb", _newupload ] ];				
			
			sleep 0.2;
		};
		
		T9_varDiagAbort = false;

		missionNamespace setVariable [ _IZ, false ];
		publicVariable _IZ;		
	};
};

UploadDataDONE = true;

