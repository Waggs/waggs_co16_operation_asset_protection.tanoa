/*
=======================================================================================================================

	downloadData - script to download data from a laptop and because of this complete a task (as example)

	File:		downloadData.sqf
	Author:		T-800a

=======================================================================================================================
*/

T8_varFileSize = 32875;  								// Filesize ... smaller files will take shorter time to download!

T8_varTLine01 = "Download aborted!";								
T8_varTLine02 = "Download already in progress by someone else!";
T8_varTLine03 = "Download started!";
T8_varTLine04 = "Download finished!";
T8_varTLine05 = "##  Download Data  ##";				// line for the addaction

T8_varDiagAbort = false;
T8_varDownSucce = false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

T8_fnc_abortActionLaptop =
{
	if ( T8_varDownSucce ) then 
	{
		// hint "DEBUG - DONE";
		T8_varDiagAbort = false;
		T8_varDownSucce = false;		
	
	} else { 
		player sideChat T8_varTLine01; 
		T8_varDiagAbort = true; 
		T8_varDownSucce = false; 
	};
};


T8_fnc_addActionLaptop =
{
	{
		private [ "_DT", "_cDT" ];
		_DT = format [ "%1_%2", "T8_pubVarDataTask", _x ];
		_cDT = missionNamespace getVariable [ _pV, false ];
		if ( _cDT ) exitWith {};
		_x addAction ["<t color=""#82C7FF"">" + T8_varTLine05, { call T8_fnc_ActionLaptop; }, [], 10, false, false ];
	} forEach _this;	
};


T8_fnc_removeActionLaptop =
{
	_laptop = _this select 0;
	_id = _this select 1;
	_laptop removeAction _id;
};


T8_fnc_ActionLaptop =
{
	private [ "_laptop", "_caller", "_id", "_cIU", "_DT", "_IU" ];
	_laptop = _this select 0;
	_caller = _this select 1;
	_id = _this select 2;

	_DT = format [ "%1_%2", "T8_pubVarDataTask", _laptop ];
	_IU = format [ "%1_%2", "T8_pubVarInUse", _laptop ];
	
	_cIU = missionNamespace getVariable [ _IU, false ];
	if ( _cIU ) exitWith { player sideChat T8_varTLine02; };
	
	player sideChat T8_varTLine03;
	
	missionNamespace setVariable [ _IU, true ];
	publicVariable _IU;
		
	[ _laptop, _id, _IU, _DT ] spawn 
	{
		private [ "_laptop", "_id", "_newFile", "_dlRate" ];
		
		_laptop		= _this select 0;
		_id			= _this select 1;
		_IU			= _this select 2;
		_DT			= _this select 3;
		
		_newFile = 0;
		
		createDialog "T8_DataDownloadDialog";
		
		sleep 0.5;
		ctrlSetText [ 8001, "Connecting ..." ];
		sleep 0.5;
		ctrlSetText [ 8001, "Connected:" ];		
		ctrlSetText [ 8003, format [ "%1 kb", T8_varFileSize ] ];		
		ctrlSetText [ 8004, format [ "%1 kb", _newFile ] ];		
		
		while { !T8_varDiagAbort } do
		{
			_dlRate = 200 + random 80;
			_newFile = _newFile + _dlRate;

			if ( _newFile > T8_varFileSize ) then 
			{
				_dlRate = 0;		
				_newFile = T8_varFileSize;
				ctrlSetText [ 8001, "Download finished!" ];	
				T8_varDiagAbort = true;
				player sideChat T8_varTLine04;
				T8_varDownSucce = true;
				
				missionNamespace setVariable [ _DT, true ];
				publicVariable _DT;				

				[ [ _laptop, _id ], "T8_fnc_removeActionLaptop", true, true ] spawn BIS_fnc_MP;
				downloadDestroy = true; publicVariable "downloadDestroy";
			};
			
			ctrlSetText [ 8002, format [ "%1 kb/t", _dlRate ] ];		
			ctrlSetText [ 8004, format [ "%1 kb", _newFile ] ];				
			
			sleep 0.2;
		};
		
		T8_varDiagAbort = false;

		missionNamespace setVariable [ _IU, false ];
		publicVariable _IU;		
	};
};

downloadDataDONE = true;

