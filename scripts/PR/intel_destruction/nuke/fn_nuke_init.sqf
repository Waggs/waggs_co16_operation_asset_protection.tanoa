if (isNil "noNukeDestroy") then { noNukeDestroy = []; publicVariable "noNukeDestroy"; };

private ["_bomb","_wave_radius"];

_bomb      = _this select 0;   //  name of bomb
_condition = _this select 1;   //  condition name to create for detonation, use quotes. In a trigger or by other means, to activate the bomb use for example,    setbomb1 = true;
_time      = _this select 2;   //  time before bomb detonation
_geigerDst = _this select 3;   //  distance from bomb to hear the radiation meter
_reqMineDt = _this select 4;   //  require a mine detector to hear geiger
_radius    = _this select 5;   //  affected radius
_doDamage  = _this select 6;   //  do damage from explosion harm_to = true;
_fire      = _this select 7;   //  set random objects on fire, could have frame impact. *** needs implamation and testing
_fireScale = _this select 8;   //  fire scale - buildings to burn. Higher number means more fires. Value of 0 will only do small amount of fires at immediate blast area, roughly 1/2 of the affected radius
_irradTime = _this select 9;   //  irradiation_time
_reqGPS    = _this select 10;  //  require gps to hear radiation
_blowSpeed = _this select 11;  //  nuclear blow speed
_radDamage = _this select 12;  //  nuclear radiation damage coefficient
_carArmor  = _this select 13;  //  nuclear car armour
_blacklist = _this select 14;  //  nuclear blacklist
_radFree   = _this select 15;  //  radiation free zones, can be house, vehicle

_yield = (_radius / 40) * 1000;
_fireScale = _fireScale * .1;

_bomb setVariable ["nukeArray", [_yield, _radius,_doDamage,_fire,_fireScale,_irradTime,_reqGPS,_blowSpeed,_radDamage,_carArmor,_blacklist,_radFree], true];
[[_bomb, _geigerDst, _reqMineDt], "pr_fnc_nuke_preGeiger", true, false] call BIS_fnc_MP;
missionNamespace setVariable [_condition, false];

_nukeNameComplied = format [ "%1_%2", "Nuke", _bomb ];
_bomb setVariable ["nukeComplied", "false", true];
_bomb setVariable ["nukeUsed", "false", true];
_bomb setVariable ["pvpfw_cleanUp_keep", true];

waitUntil { (missionNamespace getVariable _condition) };

[[_bomb, _time], "pr_fnc_bombTimer", true, false] call BIS_fnc_MP;

_bomb setVariable ["nukeComplied", "true", true];

buildNukes = true;
