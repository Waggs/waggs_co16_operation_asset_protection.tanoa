1. Place the PR_Commander folder in your mission's folder \scripts. If one doesn't exist, create one.

2. Add the following to your description.ext:
#include "scripts\PR_Commander\PR_commander.hpp" 

3. Add the following to your init.sqf:
execVM "scripts\PR_Commander\leadersChoice.sqf"; 

4. If you place the script in a directory other than scripts\PR_Commander, then set the PATH in Pr_ids.h
