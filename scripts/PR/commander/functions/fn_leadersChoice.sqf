/* fn_leadersChoice.sqf
   usage
   init.sqf:         execVM "scripts\PR_Commander\leadersChoice.sqf";
   description.ext:  #include "scripts\PR_Commander\PR_commander.hpp"
   ver 1.0  - 2015-03-12
*/

if (["AllowCD", 1] call BIS_fnc_getParamValue == 0) exitWith {};
if (!isDedicated) then { waitUntil { local player; player == player }; };

_excludedPlayers = [];
if (!isNil "civ1") then { _excludedPlayers = _excludedPlayers + [civ1]; };
if (!isNil "PG_ai") then { _excludedPlayers = _excludedPlayers + [PG_ai]; };
if (!isNil "PG1_ai") then { _excludedPlayers = _excludedPlayers + [PG1_ai]; };
if (!isNil "PG2_ai") then { _excludedPlayers = _excludedPlayers + [PG2_ai]; };
if (!isNil "PG3_ai") then { _excludedPlayers = _excludedPlayers + [PG3_ai]; };
if (!isNil "PG4_ai") then { _excludedPlayers = _excludedPlayers + [PG4_ai]; };
excludedPlayers = _excludedPlayers; publicVariable "excludedPlayers";

//grpAll = (playableUnits) - excludedPlayers; publicVariable "grpAll";
if (isNil "PG") then { PG = []; };
if (isNil "PG1") then { PG1 = []; };
if (isNil "PG2") then { PG2 = []; };
if (isNil "PG3") then { PG3 = []; };
if (isNil "PG4") then { PG4 = []; };

grpAll = (units PG1) + (units PG2) + (units PG3) + (units PG4) - excludedPlayers; publicVariable "grpAll";

// transport vars to reset on jip
if !(isNil "startHeliTimer") then { startHeliTimer = false; publicVariable "startHeliTimer"; };
if !(isNil "heli_transport") then { heli_transport = false; publicVariable "heli_transport"; };
if !(isNil "heliTransportDestroyed") then { heliTransportDestroyed = false; publicVariable "heliTransportDestroyed"; };
if !(isNil "heliTransportCleanup") then { heliTransportCleanup = false; publicVariable "heliTransportCleanup"; };
if !(isNil "heliTransportEnd") then { heliTransportEnd = false; publicVariable "heliTransportEnd"; };
if !(isNil "new_Heli_transport") then { new_Heli_transport = true; publicVariable "new_Heli_transport"; };
if !(isNil "pr_rtb_order") then { pr_rtb_order = false; publicVariable "pr_rtb_order"; };
if !(isNil "pr_liftoff_order") then { pr_liftoff_order = false; publicVariable "pr_liftoff_order"; };

// supply heli vars to reset on jip
if !(isNil "heliSupplyDrop") then { heliSupplyDrop = false; publicVariable "heliSupplyDrop"; };

fnc_commandDialogue = {
    if !((p1) == (player)) exitWith {};
    #include "ids.h"

    pr_commRespawn = compileFinal "PR_Commander = p1 addAction [""<t color='#84c7ff'>Commander's Dialogue</t>"", '_handle = CreateDialog ""COMMAND_DIALOG"";', 0, -97, false];";
    
    pr_keyHandler = { hint 'empty pr_keyHandler'; };
    pr_keyEventId = -1;

    pr_keyEventHandler = {
        private "_res";
        _res = false;
        if ((_this select 1) == 28) then { // ENTER key
            [] call pr_keyHandler;
            _res = true;
            call pr_debugUnWatchKeys; // disable trapping the ENTER key
        };
        _res;
    };
    pr_debugWatchKeys = {
        [] spawn {
            if (!isDedicated) then {
                waitUntil { !isNull (findDisplay 46) };
                pr_keyEventId = (findDisplay 46) displayAddEventHandler ["KeyDown", "_this call pr_keyEventHandler;"];
            };
        };
    };
    pr_debugUnWatchKeys = {
        if (pr_keyEventId != -1) then { (findDisplay 46) displayRemoveEventHandler ["KeyDown", pr_keyEventId]; };
        pr_keyEventId = -1;
        pr_keyHandler = {};
    };
    _name = name player;
    //player addMPEventHandler ["respawn",{ _unit = _this select 0; _unit call pr_commRespawn }];
    player call pr_commRespawn;
    hintSilent "";
};

//if (!isDedicated) then { waitUntil { local player; player == player }; };
_nul = [[p1] call fnc_commandDialogue],'BIS_fnc_execVM', nil, false call BIS_fnc_MP;
