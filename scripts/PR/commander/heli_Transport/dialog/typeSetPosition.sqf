
waitUntil { !isNull (findDisplay 1801) && { dialog } };

_display = 1821;
_string = ["Map Click Only","Nearest Player","Nearest Leader","Nearest Rooftop","Nearest Smoke"];
_arr = [0,1,2,3];
_choice = -1;
{
    _index = lbAdd [_display, _x];
} forEach _string;

while { dialog } do {

    waitUntil { lbCurSel _display != -1 };
    _count = lbCurSel _display;

    if (_count == 0) then {
        _choice = _arr select 0;
    } else {
        if (_count == 1) then {
            _choice = _arr select 1;
        } else {
            if (_count == 2) then {
                _choice = _arr select 2;
            } else {
                if (_count == 3) then {
                    _choice = _arr select 3;
                } else {
                    if (_count == 4) then {
                        _choice = _arr select 4;
                    };
                };
            };
        };
    };

    missionNamespace setVariable ["setPositionType", _choice];
    missionNamespace setVariable ["positionTypeSelected", 1];
};

missionNamespace setVariable ["positionTypeSelected", 0];