
// Script which updates the text value below "SELECTED DELAY" in the right hand box of the GUI.
// Adds values first, then updates it frequently, as well as assigning missionNamespace variable "setAction".

waitUntil { !isNull (findDisplay 1801) && { dialog } };

_display = 1812;
_string = ["Land","Fast Rope","Paradrop","Rooftop"];
_arr = [0,1,2,3];
_choice = -1;
{
    _index = lbAdd [_display, _x];
} forEach _string;

while { dialog } do {

    waitUntil { lbCurSel _display != -1 };
    _count = lbCurSel _display;

    if (_count == 0) then {
        _choice = _arr select 0;
    };
    if (_count == 1) then {
        _choice = _arr select 1;
    };
    if (_count == 2) then {
        _choice = _arr select 2;
    };
    if (_count == 3) then {
        _choice = _arr select 3;
    };

    missionNamespace setVariable ["setAction", _choice];
    missionNamespace setVariable ["actionSelected", 1];
};

missionNamespace setVariable ["actionSelected", 0];