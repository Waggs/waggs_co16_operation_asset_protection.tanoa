/* fn_extraction.sqf
*  Author: PapaReap
*  function name: pr_fnc_staticSpawn
*  credit to cobra4v320 for inspiration from his code.
*  ver 1.0
*  ver 1.1 2016-06-23 added heli will hover over water to pickup and dropoff, changed waypointing

*  Description:
*  Spawns a helicopter at a designated location. 
*  The helicopter moves to another designated location to pick up soldiers.
*  Prior to the helicopter touching down it will open both of its doors. When it lifts off it will close its doors.
*  The helicopter will then move to another designated location and drop off the soldiers. Prior to landing the doors will open. 
*  When the group gets out the doors will close and the helicopter will return to its spawn position to delete the crew, helicopter, and group.

*  Parameter(s):
*  0: GROUP    - group the helicopter will pick up (group)
*  1: STARTPOS - spawn position - can be string or position (array)
*  2: ENDPOS   - drop off position - can be string or position (array)
*  3: HEIGHT   - (optional) the altitude where the HALO will start from (number)
*  4: CAPTIVE  - (optional) true to set the helicopter and crew captive (boolean)
*  5: DAMAGE   - (optional) true to allow the helicopter and crew to be damaged (boolean)
*  6: HELITYPE - (optional) default "B_Heli_Light_01_F", enter classname to change (string)
*  7: ACTION   - (optional) default "Land", availble options are "Land", "Fast Rope", "Paradrop", "Rooftop"
*  8: GROUPALL - (optional) use - grpAll to transport all units
*  Example(s):
*  //  [[group player, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, "B_Heli_Light_01_F", "Land", grpAll], "pr_fnc_heliTransport"] call BIS_fnc_MP;
*  [group player, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, "B_Heli_Light_01_F", "Land", grpAll] remoteExec ["pr_fnc_heliTransport", 2];
*  //  [[PG1, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, "B_Heli_Light_01_F", "Land"], "pr_fnc_heliTransport"] call BIS_fnc_MP;
*  [PG1, markerPos "extractSpawn", markerPos "dropOff", 50, FALSE, TRUE, "B_Heli_Light_01_F", "Land"] remoteExec ["pr_fnc_heliTransport", 2];
*/
if !(isServer) exitWith {};

sleep 5;
//Parameters
private ["_group","_startPos","_endPos","_height","_captive","_damage","_heliType","_actionType","_grpAll"];
transportarray =_this; publicVariable "transportarray";
_group    = [_this, 0, grpNull, [grpNull, objNull]] call BIS_fnc_param;
_startPos = [_this, 1, [], [[]]] call BIS_fnc_param;
_endPos   = [_this, 2, [], [[]]] call BIS_fnc_param;
_height   = [_this, 3, 50, [0]] call BIS_fnc_param;
_captive  = [_this, 4, false, [false]] call BIS_fnc_param;
_damage   = [_this, 5, true, [true]] call BIS_fnc_param;
_heliType = [_this, 6, "B_Heli_Light_01_F", []] call BIS_fnc_param;
_actionType = [_this, 7, "Land", []] call BIS_fnc_param;
_grpAll = [];
if ((count _this) > 8) then { _grpAll = _this select 8; };

// Validate parameter count
if ((count _this) < 3) exitWith { "[Extraction] function requires at least (3) parameters!" call BIS_fnc_error; false };

// Validate Parameters
if (isNull _group) exitWith {"[Extraction] Group (0) parameter must not be null. Accepted: GROUP or OBJECT" call BIS_fnc_error; false};
if ((typeName _startPos) != (typeName [])) exitWith {"[Extraction] Position (1) should be an Array!" call BIS_fnc_error; false};
if ((count _startPos) < 2) exitWith {"[Extraction] Position (1) should contain at least 2 elements!" call BIS_fnc_error; false};
if ((typeName _endPos) != (typeName [])) exitWith {"[Extraction] Position (2) should be an Array!" call BIS_fnc_error; false};
if ((count _endPos) < 2) exitWith {"[Extraction] Position (2) should contain at least 2 elements!" call BIS_fnc_error; false};
if (_height < 30) exitWith {"[Extraction] height (3) should be at least 30!" call BIS_fnc_error; false};

// Object given instead of group
if (typeName _group == "OBJECT") then {
    if (_group isKindOf "MAN") then {
        _group = group _group;
    } else {
        if (count crew _group < 1) exitWith {
            "Vehicle given as GROUP has no crew" call BIS_fnc_error;
        };
        if (!isNull group driver _group) then {
            _group = group driver _group;
        } else {
            if (!isNull group gunner _group) then {
                _group = group gunner _group;
            } else {
                _group = group ((crew _group) select 0);
            };
        };
    };
};

// Atlas LHD Mod check
if (!isNil "atlas") then {
    atlasPresent = true; publicVariable "atlasPresent";
    atlasPickup = false; publicVariable "atlasPickup";
    atlasDropoff = false; publicVariable "atlasDropoff";
} else {
    atlasPresent = false; publicVariable "atlasPresent";
};

// check for smoke, strobe, default is player position
// TODO: check for flare
_signalArray = [];
_smoke = position player nearObjects ["SmokeShell", 50];
_strobe = position player nearObjects ["B_IRstrobe", 50];
_signalArray = _signalArray + _smoke;
_signalArray = _signalArray + _strobe;
_pickupGridPos = [0,0,0];

private ["_signal","_signalPos"];
if (getMarkerColor "pickUp" == "ColorBlue") then {
    _signalPos = getMarkerPos "pickUp"; 
    _pickupGridPos = mapGridPosition _signalPos;
}; /*
} else {
    if (count _signalArray > 0) then {
        _signal = _signalArray select 0;
        _signalPos = position _signal;
    } else {
        _signalPos = position player;
    };
}; */

// find safe landing position
private "_pickUpPos";
_safePos = [];

if !(surfaceIsWater _signalPos) then {
    _range = 35;
    _maxGrad = 0.1;

    while { ((count _safePos) == 0) } do {
        _safePos = [
            ["position", _signalPos],
            ["number", 1],
            ["objDistance", 9],
            ["range", [0, _range]],
            ["maxGradient", _maxGrad],
            ["waterMode",1],
            ["onShore",0]
        ] call pr_fnc_randomCirclePositions;
        _range = _range * 1.25;
        _maxGrad = _maxGrad + 0.01;
    };
    _pickUpPos = createVehicle ["Land_HelipadEmpty_F", (_safePos select 0), [], 0, "NONE"]; pr_pickUpPos = _pickUpPos;
} else {
    _safePos = _signalPos;
    _pickUpPos = createVehicle ["Land_HelipadEmpty_F", _safePos, [], 0, "NONE"];
    _pickUpPos setPosASL [getPosASL _pickUpPos select 0, getPosASL _pickUpPos select 1, 2];
};
pr_pickUpPos = _pickUpPos;

//if !(surfaceIsWater _signalPos) then {
    //if (_actionType == "Land") then {
        //_pickUpPos = createVehicle ["Land_HelipadEmpty_F", (_safePos select 0), [], 0, "NONE"]; pr_pickUpPos = _pickUpPos;
    //} else {
	    //if (_actionType == "Land") then {
		//};
//} else {
    //_pickUpPos = createVehicle ["Land_HelipadEmpty_F", _safePos, [], 0, "NONE"];
//};

//if (surfaceIsWater getPos _pickUpPos) then {
//    _pickUpPos setPosASL [getPosASL _pickUpPos select 0, getPosASL _pickUpPos select 1, 2];
//};

if ((!isNil "atlas") && (surfaceIsWater getMarkerPos "pickUp")) then {
    if (([position atlas select 0, position atlas select 1,0] distance getMarkerPos "pickUp") < 200) then {
        _pickUpPos attachTo [atlas, [0.0, 100.0, -4.5]];
    };
};
//if (isNil "noHCSwap") then { noHCSwap = []; publicVariable "noHCSwap" };

//Create the helicopter based on groups side
private ["_side","_heliContainer"];
_side = blufor;
switch (side _group) do {
    case blufor:      { _side = blufor; };
    case opfor:       { _heliType = "O_Heli_Light_02_F"; _side = opfor; };
    case independent: { _heliType = "I_Heli_light_03_F"; _side = independent; };
};
_heliContainer = [[_startPos select 0, _startPos select 1, _height], [_startPos, _pickUpPos] call BIS_fnc_dirTo , _heliType, _side] call BIS_fnc_spawnVehicle; Z_heliContainer=_heliContainer;

private ["_heli","_heliGroup"];
_heli = _heliContainer select 0; pr_heliTrans = _heli; publicVariable "pr_heliTrans";
_heliGroup = _heliContainer select 2; pr_heliTransGroup = _heliGroup; publicVariable "pr_heliTransGroup";
[_heli, _heliGroup] spawn pr_fnc_transportMonitor;

//{ noHCSwap = noHCSwap + [_x]; } forEach units _heliGroup; publicVariable "noHCSwap";
sleep 0.5;

_transportRequestHint = format ["Angel Wing, Transport Requested at Grid %1", _pickupGridPos];
logic_firefly sideChat _transportRequestHint;
logic_firefly_SideChat = _transportRequestHint; publicVariable "logic_firefly_SideChat";

//height & speed
private "_dir";
_dir = direction _heli;
_heli setVelocity [sin (_dir) * 50, cos (_dir) * 50, 0];
_heli flyInHeight _height;

_heliGroup allowFleeing 0;
_heli allowDamage _damage;
_heli setCaptive _captive;

//turn off collision lights
[_heli] spawn { private "_heli"; _heli = _this select 0; while { alive _heli } do { _heli action ["collisionlightOff", _heli]; sleep 0.01 }; };
sleep 4;

_copyBaseFireflyHint = format ["Copy Base Firefly, Transport Inbound to Grid %1", _pickupGridPos];
logic_pigeon sideChat _copyBaseFireflyHint;
logic_pigeon_SideChat = _copyBaseFireflyHint; publicVariable "logic_pigeon_SideChat";

if (isNil "heli_liftOff") then { heli_liftOff = false; publicVariable "heli_liftOff" };

// waypoint pickup
_behaviour = ["CARELESS","YELLOW","FULL","FILE"];
_wpStatement = "_heli = vehicle this; { _heli animateDoor [_x, 1] } forEach ['door_back_L','door_back_R','door_L','door_R']; _heli lock false; pr_liftoff_order = true; publicVariable 'pr_liftoff_order';";
_wpLand = "_heli land 'GET IN';";

[_heliGroup, position _pickUpPos, ["LOAD"], _behaviour, _wpStatement + _wpLand] call pr_fnc_addWaypoint;

atlasToFar = false;
scopeName "preAtlas";
if ((!isNil "atlas") && !(atlasToFar)) then {
   if ((surfaceIsWater getMarkerPos "pickUp") && (([position atlas select 0, position atlas select 1,0] distance (getMarkerPos "pickUp")) < 100)) then {
        waitUntil { (((_heli distance atlas < 200) && (getPosASL _heli select 2 < 22)) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive))) };
        atlasPickup = true; publicVariable "atlasPickup";
        _atlasLanding = [_heli, _pickUpPos, _grpAll, _group, 0.05] spawn pr_fnc_atlasLanding;
        waitUntil { (scriptDone _atlasLanding || ((pr_rtb_order) || !(pr_transportAlive)))};
    } else {
        atlasToFar = true;
        breakTo "preAtlas";
    };
} else {
    waitUntil {
        if (count _grpAll > 0) then {
            ((heli_liftOff) || ({ _x in _heli } count _grpAll == { alive _x } count _grpAll) || ((!heli_transport) && ({ _x in _heli } count _grpAll < 1)) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)))
        } else {
            ((heli_liftOff) || ({ _x in _heli } count (units _group) == { alive _x } count (units _group)) || ((!heli_transport) && ({ _x in _heli } count (units _group) < 1)) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)))
        };
    };
};
if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};

if ((alive _heli) && (canMove _heli)) then { { _heli animateDoor [_x, 0] } forEach ["door_back_L","door_back_R","door_L","door_R"] };

waitUntil { ((!heli_transport) || (!heli_liftOff) || ((pr_rtb_order) || !(pr_transportAlive))) }; //for heli_liftOff to reset to false
if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};

sleep 2;
_nil = [] spawn { sleep 3; pr_liftoff_order = false; publicVariable "pr_liftoff_order"; };
_heli allowDamage _damage;

_dropOffPos = "Land_HelipadEmpty_F" createVehicle getMarkerPos "dropOff";
_wpType2 = "";
_wpStatement2 = "";

if (_actionType == "Land") then {
    _wpStatement2 = "_heli land 'GET OUT';";
    if (surfaceIsWater getPos _dropOffPos) then {
        _dropOffPos setPosASL [getPosASL _dropOffPos select 0, getPosASL _dropOffPos select 1, 2];
    };
} else {
    if (_actionType == "FastRope") then {
        _wpStatement2 = "_heli land 'GET OUT';";
        if (surfaceIsWater getPos _dropOffPos) then {
            _dropOffPos setPosASL [getPosASL _dropOffPos select 0, getPosASL _dropOffPos select 1, 20];
        } else {
            _dropOffPos setPosATL [getPosATL _dropOffPos select 0, getPosATL _dropOffPos select 1, 20]; 
        };
    } else {
        if (_actionType == "Paradrop") then {
            _wpStatement2 = "";
            if (surfaceIsWater getPos _dropOffPos) then {
                _dropOffPos setPosASL [getPosASL _dropOffPos select 0, getPosASL _dropOffPos select 1, 1300];
            } else {
                _dropOffPos setPosATL [getPosATL _dropOffPos select 0, getPosATL _dropOffPos select 1, 1300]; 
            };
        } else {
            if (_actionType == "Rooftop") then {
            };
        };
    };
};

// waypoint dropoff
if ((heli_transport) ||  (({ _x in _heli } count _grpAll > 0) || ({ _x in _heli } count (units _group) > 0)) ) then {
    if ((alive _heli) && (canMove _heli)) then {
        atlasToFar2 = false;
        scopeName "preAtlas2";
        if ((!isNil "atlas") && !(atlasToFar2)) then {
            if ((surfaceIsWater getMarkerPos "dropOff") && (([position atlas select 0, position atlas select 1,0] distance getMarkerPos "dropOff") < 100)) then {
                _dropOffPos attachTo [atlas, [0.0, 100.0, -4.5]];
                "dropOff" setMarkerPos getPos _dropOffPos;
                [_heliGroup, position _dropOffPos, ["UNLOAD"], _behaviour, _wpStatement + _wpStatement2] call pr_fnc_addWaypoint;
                waitUntil { (((_heli distance atlas < 200) && (getPosASL _heli select 2 < 22)) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive))) };
                if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};
                atlasDropoff = true; publicVariable "atlasDropoff";
                _atlasLanding = [_heli, _dropOffPos, _grpAll, _group, 0.05] spawn pr_fnc_atlasLanding;
                waitUntil { (scriptDone _atlasLanding || ((pr_rtb_order) || !(pr_transportAlive))) };
            } else {
                atlasToFar2 = true;
                breakTo "preAtlas2";
            };
        } else {
            [_heliGroup, position _dropOffPos, ["UNLOAD"], _behaviour, _wpStatement + _wpStatement2] call pr_fnc_addWaypoint;
            if (_actionType == "FastRope") then { waitUntil { (heli_liftOff) }; };
            if (_actionType == "Paradrop") then { _heli flyInHeight 1300; };
            waitUntil {
                if (count _grpAll > 0) then {
                    ((heli_liftOff) || ({ _x in _heli } count _grpAll < 1) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)));
                } else {
                    ((heli_liftOff) || ({ _x in _heli } count (units _group) < 1) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)));
                };
            };
        };
    };
};
if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};

if ((alive _heli) && (canMove _heli)) then {
    {_heli animateDoor [_x,0]} forEach ["door_back_L","door_back_R","door_L","door_R"];

    waitUntil {
        if (count _grpAll > 0) then {
            ((heli_liftOff) || ({ _x distance _heli > 10 } count _grpAll == { alive _x } count _grpAll) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)));
        } else {
            ((heli_liftOff) || ({ _x distance _heli > 10 } count (units _group) == { alive _x } count (units _group)) || (!(alive _heli) || (!canMove _heli)) || ((pr_rtb_order) || !(pr_transportAlive)));
        };
    };
};
if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};

waitUntil { ((!heli_transport) || (!heli_liftOff) || ((pr_rtb_order) || !(pr_transportAlive))) }; //for heli_liftOff to reset to false
if ((pr_rtb_order) || !(pr_transportAlive)) exitWith {};

sleep 2;
_nil = [] spawn { sleep 3; pr_liftoff_order = false; publicVariable "pr_liftoff_order"; };
_heli allowDamage _damage;

if (isNil "heliTransportEnd") then { heliTransportEnd = false } else { heliTransportEnd = false };

if ((alive _heli) && (canMove _heli)) then {
    [_heli, _heliGroup, _startPos] spawn pr_fnc_rtb;
};

deleteVehicle _pickUpPos;
deleteVehicle _dropOffPos;
