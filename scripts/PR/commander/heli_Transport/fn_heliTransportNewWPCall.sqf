/* fn_heliTransportNewWPCall.sqf
*  Author: PapaReap
*
*  ver 1.0 - 2016-07-27
*/

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_heliTransportNewWPCall start"]; };

_quickMove = false;
if (missionNamespace getVariable "quickMove") then { _quickMove = true; };

_marker = "";
_posType = "";
_transType = "";
_flyHeight = 50;
_transSpeed = "";

if !(_quickMove) then {
    _transportPosition = missionNamespace getVariable "setPositionType";
    if (_transportPosition == 0) then {
        _posType = "MapClickOnly";
    } else {
        if (_transportPosition == 1) then {
            _posType = "NearestPlayer";
        } else {
            if (_transportPosition == 2) then {
                _posType = "NearestLeader";
            } else {
                if (_transportPosition == 3) then {
                    _posType = "NearestRooftop";
                } else {
                    if (_transportPosition == 4) then {
                        _posType = "NearestSmoke";
                    };
                };
            };
        };
    };

    _transportType = missionNamespace getVariable "setChangeRouteType";
    if (_transportType == 0) then {
        _transType = "PickupUnload";
    } else {
        if (_transportType == 1) then {
            _transType = "Fastrope";
        } else {
            if (_transportType == 2) then {
                _transType = "MoveHover";
            } else {
                if (_transportType == 3) then {
                    _transType = "LandEngineOn";
                } else {
                    if (_transportType == 4) then {
                        _transType = "LandEngineOff";
                    };
                };
            };
        };
    };

    _flyHeightType = missionNamespace getVariable "setFlyHeightType";
    if (_flyHeightType == 0) then {
        _flyHeight = 10;
    } else {
        if (_flyHeightType == 1) then {
            _flyHeight = 20;
        } else {
            if (_flyHeightType == 2) then {
                _flyHeight = 50;
            } else {
                if (_flyHeightType == 3) then {
                    _flyHeight = 100;
                } else {
                    if (_flyHeightType == 4) then {
                        _flyHeight = 200;
                    } else {
                        if (_flyHeightType == 5) then {
                            _flyHeight = 500;
                        };
                    };
                };
            };
        };
    };

    _transSpeedType = missionNamespace getVariable "setTransSpeed";
    if (_transSpeedType == 0) then {
        _transSpeed = "UNCHANGED";
    } else {
        if (_transSpeedType == 1) then {
            _transSpeed = "LIMITED";
        } else {
            if (_transSpeedType == 2) then {
                _transSpeed = "NORMAL";
            } else {
                if (_transSpeedType == 3) then {
                    _transSpeed = "FULL";
                };
            };
        };
    };

    //private _fastRopeType = missionNamespace getVariable "setFastRopeType";
    //if (isNil "_fastRopeType") then {
    //    missionNamespace setVariable ["setFastRopeType", 1];
    //    _fastRopeType = 1;
    //};
    //_fastRope = false;
    //if (_fastRopeType == 0) then {
    //    _fastRope = true;
    //} else {
    //    if (_fastRopeType == 1) then {
    //        _fastRope = false;
    //    };
    //};
};

if !(isNull pr_heliTrans) then {
    missionNameSpace setVariable ["wpMarker", "Transport"];
    newTransClick = false; publicVariable "newTransClick";

    openMap [true, false];
    ["<t size='0.7' color='#00E0FD'>" + "Commander<br/>Place transport location on map<br/>(click location)<br/><br/>" + "</t>" + "<t size='0.7' color='#ff9900'>exit map to cancel" + "</t>",0,0.8,8,1] spawn BIS_fnc_dynamicText;
    if (isNil "NwpID") then { NwpID = 1 };
    if (isNil "NwpID_new") then { NwpID_new = 0 };
    NwpID = NwpID + NwpID_new;

    ["NwpID","onMapSingleClick", {
        {
            deleteMarker _x;
            transportMarkers = transportMarkers - [_x];
        } forEach transportMarkers; publicVariable "transportMarkers";

        _wpMarker = missionNameSpace getVariable "wpMarker";
        click = _pos;
        newTransClick = true; publicVariable "newTransClick";

        //if (getMarkerColor _wpMarker == "ColorBlue") then {
        //    deleteMarker _wpMarker;
        //};
        _marker = "";
        if (getMarkerColor (missionNameSpace getVariable "wpMarker") == "") then {
            _marker = createMarker [_wpMarker, click];
            _marker setMarkerType "hd_pickup";
            _marker setMarkerColor "ColorBlue";
            _marker setMarkerText _wpMarker;
            missionNameSpace setVariable ["wpMarker", _marker];
            transportMarkers = transportMarkers + [_marker]; publicVariable "transportMarkers";
        };
    }] call BIS_fnc_addStackedEventHandler;

    sleep .01;
    waitUntil { ((!visibleMap) || (newTransClick)) };

    if (newTransClick) then {
        ["<t size='0.7' color='#00E0FD'>" + "Commander has called for transport" + "</t>",0,0.8,4,1] spawn BIS_fnc_dynamicText;
        transMark = "Land_PenBlack_F" createVehicle getMarkerPos "Transport";
        transMark setPos getMarkerPos "Transport"; publicVariable "transMark";
        waitUntil { (!isNull transMark) };
        _gridPos = mapGridPosition transMark;
        if !(_quickMove) then {
            [(missionNameSpace getVariable "wpMarker"), _posType, _transType, _flyHeight, _transSpeed] remoteExec ["pr_fnc_newWaypoint", 2];
        } else {
            [(missionNameSpace getVariable "wpMarker")] remoteExec ["pr_fnc_newWaypoint", 2];
        };

        _requestTransportHint = format ["Base Firefly, Requesting Heli Transport for All Units on Grid Location %1", _gridPos];
        logic_Leader sideChat _requestTransportHint;
        logic_Leader_SideChat = _requestTransportHint; publicVariable "logic_Leader_SideChat";
        startHeliTimer = false; publicVariable "startHeliTimer";
    } else {
        if (!newTransClick) exitWith {
            titleText ["","PLAIN",0.5];
            click = nil;
            onMapSingleClick "";
            ["<t size='0.7' color='#ff9900'>" + "Pick up request cancelled" + "</t>",0,0.8,4,1] spawn BIS_fnc_dynamicText;
            if !(newTransClick) then {
                deleteMarker "Transport";
            };
        };
    };

    if (newTransClick) then {
        NwpID_new = (NwpID + NwpID_new) - 1;
        ["NwpID", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler;
        deleteVehicle transMark;
    };
};

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_heliTransportNewWPCall complete"]; };
