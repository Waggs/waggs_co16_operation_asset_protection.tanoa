/* 
*  Author: PapaReap 
*  function name: pr_fnc_findBoxAir 
*  ver 1.3 - 2016-01-07 code cleanup and made into function 
*  ver 1.3 - 2015-07-18 compacted all scripts into 1 
*  ver 1.2 - 2015-01-01 more refinement 
*  ver 1.1 - 2014-12-26 complete medical system rework in progress 
*  ver 1.0 - 2014-12-15 
*/ 

private["_dropbox", "_smoke", "_flare"]; 
_dropbox  = _this select 0; 
_typeDrop = _this select 1; 
_smoke = ""; 
_flare = ""; 
scopename "findAirScope"; 
if (_typeDrop == "Ammo") then { 
    _smoke = "SmokeShellBlue"; 
    _flare = "F_40mm_white"; 
} else { 
    if (_typeDrop == "Med") then { 
        _smoke = "SmokeShellRed"; 
        _flare = "F_40mm_red"; 
    } else { 
        if (_typeDrop == "Mash") then { 
            _smoke = "SmokeShellGreen"; 
            _flare = "F_40mm_white"; 
        } else { 
            if (_typeDrop == "MH9") then { 
                _smoke = "SmokeShellBlue"; 
                _flare = ""; 
            }; 
        }; 
    }; 
}; 

while { ((((getPos _dropbox) select 2) > 3) && ({ _x distance _dropbox > 60 } count playableUnits >0 )) } do { 
    if ((((getPos _dropbox) select 2) < 3) || ({ _x distance _dropbox < 60 } count playableUnits >0 )) then { breakto "findAirScope" }; 
    if !(isNil "_smokeAir") then { deleteVehicle _smokeAir }; 
    _smokeAir = _smoke createVehicle position _dropbox; 
    _smokeAir attachTo [_dropbox, [0,0,0]]; 
    _flareAir = _flare createVehicle position _dropbox; 
    _flareAir attachTo [_dropbox, [0,0,0]]; 
    if ((((getPos _dropbox) select 2) < 3) || ({ _x distance _dropbox < 60 } count playableUnits >0 )) then { if !(isNil "_smokeAir") then { deleteVehicle _smokeAir }; breakto "findAirScope" }; 
    sleep 10.9; 
    if ((((getPos _dropbox) select 2) < 3) || ({ _x distance _dropbox < 60 } count playableUnits >0 )) then { if !(isNil "_smokeAir") then { deleteVehicle _smokeAir }; breakto "findAirScope" }; 
}; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_findBoxAir complete"]; }; 
