// Script which updates the text value below "SELECTED DELAY" in the right hand box of the GUI.
// Adds values first, then updates it frequently, as well as assigning missionNamespace variable "SetDelay".

waitUntil {!isNull (findDisplay 1111) && {dialog}}; 

_display = 2101; 
_delayString = ["0","2","5","10","20"]; 
_delay = [0,2,5,10,20]; 
_setDelay = 0; 
{ 
    _index = lbAdd [_display, _x]; 
} forEach _delayString; 

while {dialog} do { 
    waitUntil {lbCurSel _display != -1}; 
    _count = lbCurSel _display; 

    if (_count == 0) then { 
        _setDelay = _delay select 0; 
    }; 
    if (_count == 1) then { 
        _setDelay = _delay select 1; 
    }; 
    if (_count == 2) then { 
        _setDelay = _delay select 2; 
    }; 
    if (_count == 3) then { 
        _setDelay = _delay select 3; 
    }; 
    if (_count == 4) then { 
        _setDelay = _delay select 4; 
    }; 

    missionNamespace setVariable ["SetDelay", _setDelay]; 
    ctrlSetText [1010, "Seconds: " + str (_setDelay)]; 
    missionNamespace setVariable ["DelaySelected", 1]; 
}; 

missionNamespace setVariable ["DelaySelected", 0]; 
