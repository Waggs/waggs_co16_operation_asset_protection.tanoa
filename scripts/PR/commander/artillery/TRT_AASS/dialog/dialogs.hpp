////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by tryteyker, v1.063, #Kavuza)
////////////////////////////////////////////////////////
#define PR_GRID_X  (0)
#define PR_GRID_Y  (0)
#define PR_GRID_W  (0.025)
#define PR_GRID_H  (0.04)
#define PR_GRID_WAbs  (1)
#define PR_GRID_HAbs  (1)

class PR_ArtyBOX {
    type = 0;
    idc = -1;
    style = ST_CENTER;
    shadow = 2;
                       //Red Green Blue Alpha
    colorBackground[] = {0.0,0.2,0.3,0.2};
    colorText[] = {1,1,1,1};
    font = "PuristaMedium";
    sizeEx = 0.02;
    text = "";
};

class TRT_ArtyGUI {
    idd = 1111;
    movingenable = true;
    class Controls {
        class PR_ArtyBOX: PR_ArtyBOX {
            idc = -1;
            text = "";
            x = -20 * PR_GRID_W + PR_GRID_X;
            y = 6 * PR_GRID_H + PR_GRID_Y;
            w = 24 * PR_GRID_W;
            h = 24 * PR_GRID_H;
        };
        class PR_ArtilleryFRAME: RscFrame {
            idc = 1800;
            text = "PR Artillery System - v1.0 ";
            x = -20 * PR_GRID_W + PR_GRID_X;
            y = 6 * PR_GRID_H + PR_GRID_Y;
            w = 24 * PR_GRID_W;
            h = 24 * PR_GRID_H;
            moving = true;
            sizeEx = 0.025;
        };

        class PR_ChooseArtilleryFRAME: RscFrame {
            idc = 1801;
            text = "CHOOSE ARTILLERY";
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 7.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 8.5 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_ChooseArtilleryLIST: RscListbox {
            idc = 1500;
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 8.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 7.5 * PR_GRID_H;
            sizeEx = 0.035;
        };

        class PR_RoundsCountFRAME: RscFrame {
            idc = 2001;
            text = " #";
            x = -7.5 * PR_GRID_W + PR_GRID_X;
            y = 7.0 * PR_GRID_H + PR_GRID_Y;
            w = 2.0 * PR_GRID_W;
            h = 8.5 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_RoundsCountLIST: RscListbox {
            idc = 3001;
            x = -7.45 * PR_GRID_W + PR_GRID_X;
            y = 8.0 * PR_GRID_H + PR_GRID_Y;
            w = 1.8 * PR_GRID_W;
            h = 7.35 * PR_GRID_H;
            sizeEx = 0.035;
        };

        class PR_ChooseRoundTypeFRAME: RscFrame {
            idc = 2002;
            text = "CHOOSE ROUND TYPE"; //--- ToDo: Localize;
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 7.0 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 8.5 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_ChooseRoundTypeLIST: RscListbox {
            idc = 3002;
            x = -5.25 * PR_GRID_W + PR_GRID_X;
            y = 8.0 * PR_GRID_H + PR_GRID_Y;
            w = 8.55 * PR_GRID_W;
            h = 7.35 * PR_GRID_H;
            sizeEx = 0.035;
        };

        class PR_ChooseRoundCountTXT: RscText {
            idc = 1001;
            text = "CHOOSE ROUND COUNT";
            x = -5.5 * PR_GRID_W + PR_GRID_X;
            y = 15.5 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_ChooseRoundsCountBOX: RscCombo {
            idc = 2100;
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 16.5 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 1.0 * PR_GRID_H;
        };
        class PR_ChooseRoundsDelayTXT: RscText {
            idc = 1002;
            text = "CHOOSE ROUND DELAY";
            x = -5.5 * PR_GRID_W + PR_GRID_X;
            y = 17.5 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_ChooseRoundsDelayBOX: RscCombo {
            idc = 2101;
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 18.5 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 1.0 * PR_GRID_H;
        };

        class PR_InformationFRAME: RscFrame {
            idc = 1803;
            text = "ARTILLERY DATA";
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 20.0 * PR_GRID_H + PR_GRID_Y;
            w = 8.9 * PR_GRID_W;
            h = 9.5 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_SelectedArtilleryTXT: RscText {
            idc = 1005;
            text = "SELECTED ARTILLERY";
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 21.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_SelectedArtilleryRES: RscText {
            idc = 1006;
            text = "None";
            x = -4.9 * PR_GRID_W + PR_GRID_X;
            y = 21.8 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.035;
        };
        class PR_SelectedRoundTypeTXT: RscText {
            idc = 1011;
            text = "SELECTED ROUND TYPE";
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 23.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_SelectedRoundTypeRES: RscText {
            idc = 1012;
            text = "None";
            x = -4.9 * PR_GRID_W + PR_GRID_X;
            y = 23.8 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.035;
        };
        class PR_SelectedRoundCountTXT: RscText {
            idc = 1007;
            text = "SELECTED ROUND COUNT";
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 25.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_SelectedRoundCountRES: RscText {
            idc = 1008;
            text = "None";
            x = -4.9 * PR_GRID_W + PR_GRID_X;
            y = 25.8 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.035;
        };
        class PR_SelectedRoundDelayTXT: RscText {
            idc = 1009;
            text = "SELECTED ROUND DELAY";
            x = -5.4 * PR_GRID_W + PR_GRID_X;
            y = 27.0 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.030;
        };
        class PR_SelectedRoundDelayRES: RscText {
            idc = 1010;
            text = "None";
            x = -4.9 * PR_GRID_W + PR_GRID_X;
            y = 27.8 * PR_GRID_H + PR_GRID_Y;
            w = 10.0 * PR_GRID_W;
            h = 1.1 * PR_GRID_H;
            sizeEx = 0.035;
        };

        class PR_MapBUTTON: RscButton {
            idc = 1600;
            text = "Go To Map"; //--- ToDo: Localize;
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 16.0 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 1.3 * PR_GRID_H;
            action = "[] execVM 'scripts\PR\commander\artillery\TRT_AASS\lb\map.sqf'";
        };
        class PR_ClearFireMarkersBUTTON: RscButton {
            idc = 1603;
            text = "Clear Fire Markers"; //--- ToDo: Localize;
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 18.0 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 1.3 * PR_GRID_H;
            action = "[] spawn TRT_fnc_clearMarker;";
        }; 
        class PR_ClearArtilleryJamBUTTON: RscButton {
            idc = 1604;
            text = "Clear Artillery Jam"; //--- ToDo: Localize;
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 20.0 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 1.3 * PR_GRID_H;
            action = "artyFired = 1; publicVariable 'artyFired'";
        };

        class PR_CommanderDialogueButton: RscButton {
            idc = 1602;
            text = "Commander Dialogue";
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 22.0 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 1.3 * PR_GRID_H;
            action = "createDialog 'COMMAND_DIALOG'; ";
        };
        class PR_CancelButton: RscButton {
            idc = 1602;
            text = "CANCEL"; //--- ToDo: Localize;
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 24.0 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 1.3 * PR_GRID_H;
            action = "closeDialog 0;";
        };
        class PR_FireBUTTON: RscButton {
            idc = 1601;
            text = "ORDER FIRE!";
            x = -19.5 * PR_GRID_W + PR_GRID_X;
            y = 26.8 * PR_GRID_H + PR_GRID_Y;
            w = 9.8 * PR_GRID_W;
            h = 2.5 * PR_GRID_H;
            action = "nul = [] execVM 'scripts\PR\commander\artillery\TRT_AASS\lb\selection\markerSelect.sqf'";
        };
    };
};
