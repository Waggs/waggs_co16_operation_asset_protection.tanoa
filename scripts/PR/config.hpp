/*  config.hpp
*  PR mission functions
*  Author: PapaReap
*  ver 1.0 2016-01-24

*  Usage: PR_fnc_fncName
*  Example: [thisTrigger, [r_fireteam, "area1"]] call PR_fnc_unitSpawn;

*  preInit = 1;      (formerly known as "forced") 1 to call the function upon mission start, before objects are initialized. Passed arguments are ["preInit"]
*  postInit = 1;     1 to call the function upon mission start, after objects are initialized. Passed arguments are ["postInit"]
*  preStart = 1;     1 to call the function upon game start, before title screen, but after all addons are loaded.
*  recompile = 1;    1 to recompile the function upon mission start
*  ext = ".fsm";     Set file type, can be ".sqf" or ".fsm" (meaning scripted FSM). Default is ".sqf".
*  headerType = -1;  Set function header type: -1 - no header; 0 - default header; 1 - system header.
*/

class PR {

    class special {
        file = "special";
        class specialsInit { preInit = 1; description = "Setting special scripts to run"; };
    };

    class functions {
        file = "scripts\PR\scripts\functions";
        class startupVars { preInit = 1; description = "Setting mission variables"; };
        class attachPart { description = ""; };
        class delObject { description = "Delete any object after a given amount of time"; };
        class moveObject { description = "Move any object to another object or position"; };
        class namesFunctions { description = ""; };
        class upsSpawner { description = "Applies upsmon orders to spawned units "; };
    };

    class ai {
        file = "scripts\PR\ai";
        class unitArray { description = ""; };
        class bluUnits { description = "Defines unit function names for blufor units, adjusts for units used i.e. massi, RHS, etc..."; };
        class redUnits { description = "Defines unit function names for opfor units, adjusts for units used i.e. massi, RHS, etc..."; };
        class artyAmbience { description = "Fires a given number and type of shell at or near enemy for given amount of time"; };
        class artySpotter { description = "Fires a given number and type of shell at or near detected enemy"; };
        class preciseSpawn { description = ""; };
        class staticSpawn { description = "Spawns a static placement - 'AA', 'AT', 'GMG', 'HMG', 'LIGHT', 'MORTAR', 'MORTARA', 'MORTARS'"; };
        class unitSpawn { description = ""; };
        class addWaypoint { description = ""; };
        class deleteWaypoint { description = ""; };
        class enterBuilding { description = ""; };
        class setInsidePos { description = ""; };
        class taskCrew { description = ""; };
        class taskDefend { description = ""; };
    };

    class clothing {
        file = "scripts\PR\ai\clothing";
        class r_sniper { description = ""; };
        class r_spotter { description = ""; };
        class r_GuerClothing { description = ""; };
        class r_TalibanClothing { description = ""; };
        class r_urbanClothing { description = ""; };
        class r_changeUniform { description = ""; };
    };

    class debug {
        file = "scripts\PR\scripts\debug";
        class debugText { description = ""; };
        class typeCheck { description = ""; };
    };

    class loadouts {
        file = "scripts\PR\loadouts";
        class loadoutInit { description = ""; };
    };

    class getPos {
        file = "scripts\PR\scripts\getPos";
        class getBpos { description = ""; };
        class getEPos { description = ""; };
        class getPos { description = ""; };
        class getPosInArea { description = ""; };
        class nearestLandPos { description = ""; };
        class nearestPlayer { description = ""; };
        class nearestRoadPos { description = ""; };
    };

    class medFunctions {
        file = "scripts\PR\healthCare\medFunctions";
        class localHealthCare { description = "Client healthcare"; };
        class healthCareInit { description = "Server healthcare"; };
        class playerState { description = "Player health & crawling"; };
        class mediCare { description = "Medic duties"; };
        class randomBodyDamage { description = "Sets random body damage and pain"; };
    };

    class mash {
        file = "scripts\PR\healthCare\mash";
        class fmInit { description = "Field mash init"; };
        class fmBuild { description = "Builds a field mash station"; };
        class fmDelete { description = "Removes field mash station"; };
    };

    class mashMan {
        file = "scripts\PR\healthCare\mash\mashMan"; 
        class mashTentInit { description = "Medic tent init"; };
        class mashTentCreate { description = "Creates a mash tent"; };
        class mashTentDelete { description = "Removes mash tent"; };
    };

    class mashCommon {
        file = "scripts\PR\healthCare\mash\mashCommon";
        class mashTreatment { description = "Restores blood to player at given distance"; };
        class mashTreatmentClient { description = "Gives player hint while blood is given"; };
        class bloodCounter { description = "Counts and tracks units of blood used"; };
        class crateBloodInv { description = "Removes blood bag in container"; };
        class bloodCargo { description = "Sets variable if container has blood in it"; };
        class medHolder { description = "Places a weapon holder for Mashman Tent"; };
        class restoreSpeech { description = "Restores player speech on Ace Unconsciousness"; };
    };

    class misc {
        file = "scripts\PR\scripts\misc";
        class artyBarrage { description = "Scripted artillery, smokes and flares"; };
        class chaseFirst { description = "Script to find first unit in trigger area to chase"; };
        class doorLocks { description = "Lock and unlock building doors"; };
        class makeLight { description = "Creates lights on positon"; };
        class randomMove { description = ""; };
        class tfrTweeks { description = "Task Force Radio tweaks - jamming, boosting etc..."; };
    };

    class drag {
        file = "scripts\PR\scripts\misc\drag_push\drag";
        class dragActionAddDrag { description = "Drag, knockout or kill actions"; };
        class dragActionKnockOut { description = "Knockout or kill unit action"; };
        class dragActionText { description = "Text to apply to add actions"; };
        class dragActionUnload { description = "Add action to unload unit"; };
        class dragActionWhileDrag { description = "While dragging, add action to release or load"; };

        class dragAddDrag { description = "Add drag to unit, trigger or script"; };
        class dragAttachWhileDragging { description = "Attach unit to dragger while being dragged"; };
        class dragDetachDragged { description = "Detach dragged unit"; };
        class dragDisappear { description = "Temporary fix to hide dead after loading in vehicle"; };
        class dragKilled { description = "Add killed eventhandler to drag unit"; };
        class dragKnockOut { description = "Knockout unit"; };
        class dragLoad { description = "Load dragged unit in vehicle"; };
        class dragMoveDragged { description = "Keep dragged unit moving with player"; };
        class dragMoveInCargo { description = "Load dragged unit in vehicle cargo"; };
        class dragReappear { description = "Temporary fix to show dead after loading in vehicle"; };
        class dragRelease { description = "Release dragged unit"; };
        class dragRemoveBody { description = "Remove body from vehicle"; };
        class dragRemoveUnloadAction { description = "Remove unload action"; };
    };

    class defuse {
        file = "scripts\PR\intel_destruction\defuse\functions";
        class bombTimer {};
        class wireCompare {};
        class codeCompare {};
    };

    class nuke {
        file = "scripts\PR\intel_destruction\nuke\functions";
        class nuke { description = ""; };
        class nuke_afterShock { description = ""; };
        class nuke_athmo { description = ""; };
        class nuke_fallout { description = ""; };
        class nuke_damage { description = ""; };
        class nuke_dust { description = ""; };
        class nuke_dust2 { description = ""; };
        class nuke_electroPulse { description = ""; };
        class nuke_electroPulseInit { description = ""; };
        class nuke_geiger { description = ""; };
        class nuke_postprocessing { description = ""; };
        class nuke_preGeiger { description = ""; };
        class nuke_radiation { description = ""; };
        class nuke_ring1 { description = ""; };
        class nuke_rotate { description = ""; };
        class nuke_shockwave { description = ""; };
        class nuke_shockwave2 { description = ""; };
        class nuke_shockwave3 { description = ""; };
        class nuke_smoke { description = ""; };
    };

    class fire {
        file = "scripts\PR\intel_destruction\fire";
        class fire { description = ""; };
        class fireDam  { description = ""; };
        class fireRandomizer  { description = ""; };
    };

    class tasks {
        file = "scripts\PR\scripts\tasks";
        class diaryHud { description = ""; };
    };

    class commander {
        file = "scripts\PR\commander\functions";
        class leadersChoice { description = "Loads scripts for PR_Commander"; };
    };

    class aiSupplyDrop {
        file = "scripts\PR\commander\aiSupplyDrop";
        class callSupplyDrop { description = ""; };
        class findBoxAir { description = ""; };
        class findBoxLand { description = ""; };
        class supplyDrop { description = ""; };
    };

    class heli_Transport {
        file = "scripts\PR\commander\heli_Transport";
        class atlasLanding { description = "Allows landing helicopter on the Atlas"; };
        class heliTransport { description = "Creates the transport helicopter"; };
        class heliTransportCall { description = ""; };
        class newWaypoint { description = "Creates a new waypoint for the transport helicopter"; };
        class randomCirclePositions { description = "Generate random positions around a center position within the given limitations"; };
        class rtb { description = "Returns transport helicopter to base"; };
        class transportMonitor { description = "Monitors the state of transport helicopter and driver"; };
    };

    class tools {
        file = "scripts\PR\scripts\tools";
        class collectBuildings { description = ""; };
        class collectMarkers { description = ""; };
        class collectObjectsNum { description = ""; };
        class hideMapObjects { description = "Function to hide map objects at mission start"; };
        class isWallInDir { description = ""; };
        class selectRandom { description = ""; };
    };

    class vehicle {
        file = "scripts\PR\scripts\vehicle";
        class airSpawn { description = "Spawns air vehicles, various ablilities..."; };
        //class carSpawn { description = "Spawns land vehicles, sets waypoints, various ablilities..."; };
        class vehicleSpawn { description = "Spawns land vehicles, optional waypoints, various ablilities..."; };
        class carWaypoint { description = "Spawns land vehicles, sets waypoints, various ablilities..."; }; //--- check if needed
        class emptyVeh { description = "Spawns a empty vehicle"; };
        class fuelControl { description = "Control vehicle fuel system"; };
        class fuelDrain { description = "Vehicle fuel drain"; };
        class moveInVehicle { description = "Moves a player into a vehicle upon call"; };
        class preciseHeli { description = "Spawns heli, follows exact waypoints"; };
        class searchLight { description = "Searchlight script for ai to follow units"; };
        class setVelocity { description = "Sets a vehicle's speed and maintains it"; };
        class vehDamage { description = "Gives random damage to vehicle's tires and fuel"; };
        class unlimitedAmmo { description = "Adds unlimited ammo to player or vehicle"; };
        class airCargoTransport { description = "note: should probably convert to several functions?"; };
        class airParaDrop { description = "Script for adding paradrop to vehicles not covered by igiload"; };
    };

    class init {
        file = "scripts\PR\scripts\init";
        class preInit { preInit = 1; description = ""; };
        class hcCheck { postInit = 1; description = "Sets Headless Clients variables for the mission"; };
        class postInit { postInit = 1; description = ""; };
        class aiSpawner { postInit = 1; description = ""; };
        class pgSwitcher { postInit = 1; description = "Script to keep a player group alive by putting a VR Entity into the group if no players are in the group"; };
        class hcTracker { description = "Monitors the state of Headless Clients"; };
    };
};
