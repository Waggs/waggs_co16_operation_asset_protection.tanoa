/*
*  Author: PapaReap
*  Add to your init.sqf: [] execVM "scripts\PR\scripts\weather\weatherACE.sqf";
*  ver 1.5 2016-03-01 rework world scripts and optimized code
*  ver 1.4 2015-06-11 re-added wind to work with ace
*  ver 1.3 2015-05-20 added world check
*  ver 1.2 2014-04-21 added option to disable fog, tweaked fog more
*  ver 1.1 2014-03-09 fog adjustments
*  ver 1.0 2014-02-13 initial release
*/

private ["_fogEnabled", "_lastrain", "_missionweather", "_overcast", "_rain", "_timeOfDay", "_timeForecast", "_ro", "_rn", "_rainOld", "_rainCurrent"];

waitUntil { (!isnil "taskHold") };

if !(isTask1complete == "true") then {
    //if (isServer && aliveOn) then { ["serverTimerComplete", "true"] call ALiVE_fnc_setData; };
    _sunrise = [0,0];
    _sunset = [0,0];

    if ((worldName == "Altis") || (worldName == "Australia") || (worldName == "Stratis")) then { _sunrise = [6, 30]; _sunset = [16,55]; }; // Altis, Australia & Stratis
    if (worldName == "mbg_celle2") then { _sunrise = [7, 00]; _sunset = [16, 15]; }; // Celle 2
    if (worldName == "Bornholm") then { _sunrise = [7, 15]; _sunset = [16, 10]; }; // Bornholm
    if (worldName == "bozcaada") then { _sunrise = [5, 45]; _sunset = [17, 40]; }; // Bozcaada
    if ((worldName == "DaKrong") || (worldName == "Rungsat")) then { _sunrise = [5, 50]; _sunset = [17, 35]; }; // Sahrani
    if (worldName == "Kunduz") then { _sunrise = [6, 40]; _sunset = [16, 50]; }; // Kunduz, Afghanistan
    if (worldName == "Lingor3") then { _sunrise = [5, 40]; _sunset = [17, 45]; }; // Lingor v3.Obeta
    if (worldName == "pja305") then { _sunrise = [5, 45]; _sunset = [17, 40]; }; // F.S.F N' Ziwasogo
    if ((worldName == "Porto") || (worldName == "Sara")) then { _sunrise = [6, 35]; _sunset = [16, 50]; }; // Sahrani

    if ((worldName == "Takistan") || (worldName == "torabora")) then { _sunrise = [6, 25]; _sunset = [17, 00]; }; // Takistan & torabora
    if (worldName == "Tanoa") then { _sunrise = [5, 45]; _sunset = [18, 30]; }; // Tanoa
    if (worldName == "Vostok_w") then { _sunrise = [6, 55]; _sunset = [16, 30]; }; // Vostok Winter
    if (worldName == "Zargabad") then { _sunrise = [6, 25]; _sunset = [16, 55]; }; // Zargabad worldName == "bozcaada"

    _timeOfDay = (["TimeofDay",0] call BIS_fnc_getParamValue);  // Sets the time of day
    _time = 0;
    _sunriseMin = ((_sunrise select 0) * 60 + (_sunrise select 1));
    _sunsetMin = ((_sunset select 0) * 60 + (_sunset select 1));

    switch (_timeOfDay) do {
        case 0:  { _time = _sunriseMin - 180; };  //"3 hrs to Sunrise"
        case 1:  { _time = _sunriseMin - 90; };   //"1 1/2 hr till Sunrise"
        case 2:  { _time = _sunriseMin - 60; };   //"1 hr till Sunrise"
        case 3:  { _time = _sunriseMin - 45; };   //"45 min till Sunrise"
        case 4:  { _time = _sunriseMin - 30; };   //"30 min to Sunrise"
        case 5:  { _time = _sunriseMin - 15; };   //"15 min till Sunrise"
        case 6:  { _time = _sunriseMin - 15; };   //"Blank"
        case 7:  { _time = _sunriseMin; };        //"Sunrise"
        case 8:  { _time = _sunriseMin + 30; };   //"30 min after Sunrise"
        case 9:  { _time = 540; };                //"Late Morning"
        case 10: { _time = 720; };                //"Noon"
        case 11: { _time = 720; };                //"Blank"
        case 12: { _time = _sunsetMin - 180; };   //"3 hrs before Sunset"
        case 13: { _time = _sunsetMin - 90; };    //"1 1/2 hrs before Sunset"
        case 14: { _time = _sunsetMin; };         //"Sunset"
        case 15: { _time = _sunsetMin; };         //"Blank"
        case 16: { _time = _sunsetMin + 90; };    //"1 1/2 hrs after Sunset"
        case 17: { _time = _sunsetMin + 180; };   //"3 hrs after Sunset - Moonrise"
        case 18: { _time = 0; };                  //"Midnight - Full Moon"
    };

    _newHr = 0;
    _min = 0;
    _hr = _time / 60;
    _hrRound = round _hr;
    if (_hrRound > _hr) then {
        _newHr = _hrRound - 1;
        _min = 1 - (_hrRound - _hr)
    } else {
        if (_hrRound < _hr) then {
            _newHr = _hrRound;
            _min = _hr - _hrRound;
        } else {
            _newHr = _hr;
            _min = 0;
        };
    };

    _yr = 2013;
    _mo = 11;
    _day = 3;
    _newMin = _min * 60;
    setDate [_yr, _mo, _day, _newHr, _newMin];
};

//--- PR Weather and time
_timeForecast = 300;
if (isNil "weatherMissionInit") then {
    weatherMissionInit = false;
};

// Sets the weather at mission start
if (prWeather == 0) then {
    pr_weather = [0, [ 0, 0, 0], .20, [random 1, random 1,  true], date];  //"Clear"
    publicVariable "pr_weather";

    //skipTime -24;
    86400 setRain (pr_weather select 0);
    86400 setFog (pr_weather select 1);
    86400 setOvercast (pr_weather select 2);
    skipTime 24;

    0 = [] spawn {
        sleep 0.1;
        simulWeatherSync;
    };
    setwind (pr_weather select 3);
    setDate (pr_weather select 4);
} else {
    if (isServer) then {
        _windDir = random 360;
        _dirChange = 0;
        _spdSml = ((random 1.25) Max 0) Min 1.25;
        _spdMed = (((random 2.5) +1) Max 1) Min 3.5;
        _spdLrg = (((random 4.5) +2.5) Max 2.5) Min 7;
        _spdChange = 0;
        _period = 60;

        _missionweather = prWeather;  // Sets the weather at mission start
        switch (_missionweather) do {
            //                     [    0, [         1         ],     2,         3, [                          4                       ],    5,     6]
            //                     [ Rain, [  Fog, Decay,  Base], Cloud, Lightning, [     Dir,        Chg,   Speed,        Chg,  Period], date, waves]
            case 0:  {pr_weather = [    0, [    0,     0,     0],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0];};    //"Clear"
            case 1:  {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 2:  {pr_weather = [    0, [    0,     0,     0],  0.40,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.1];};  //"Light Overcast"
            case 3:  {pr_weather = [    0, [    0,     0,     0],  0.55,       0.4, [_windDir, _dirChange, _spdMed, _spdChange, _period], date, 0.3];};  //"Medium Overcast"
            case 4:  {pr_weather = [    0, [    0,     0,     0],  0.85,       0.9, [_windDir, _dirChange, _spdLrg, _spdChange, _period], date, 0.5];};  //"Heavy Overcast"
            case 5:  {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 6:  {pr_weather = [ 0.15, [    0,     0,     0],  0.55,       0.3, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.5];};  //"Light Rain" 
            case 7:  {pr_weather = [ 0.575,[    0,     0,     0], 0.775,      0.65, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.7];};  //"Medium Rain" 
            case 8:  {pr_weather = [    1, [    0,     0,     0],     1,         1, [_windDir, _dirChange, _spdLrg, _spdChange, _period], date,   1];};  //"Heavy Rain"
            case 9:  {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 10: {pr_weather = [    0, [    1,   0.5,   1.5],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"Low Coastal Fog"
            case 11: {pr_weather = [    0, [    1,   0.5,  3.97],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"Med Coastal Fog"
            case 12: {pr_weather = [    0, [    1,  0.17, 30.78],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"High Coastal Fog"
            case 13: {pr_weather = [    0, [ 0.28,  0.03,  91.1],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"High Fog"
            case 14: {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 15: {pr_weather = [    0, [  0.3,     0,     0],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"Light Fog"
            case 16: {pr_weather = [    0, [  0.7,     0,     0],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"Med Fog"
            case 17: {pr_weather = [    0, [    1,     0,     0],     0,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date,   0];};  //"Heavy Fog"
            case 18: {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 19: {pr_weather = [    0, [    0,     0,     0], random 0.4,  0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.2];};  //"Cold and Possibly Clear"
            case 20: {pr_weather = [    0, [    0,     0,     0],    .5,       0.0, [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.5];};  //"Light Snow"
            case 21: {pr_weather = [    0, [    0,     0,     0],    .7,       0.0, [_windDir, _dirChange, _spdMed, _spdChange, _period], date, 0.7];};  //"Heavy Snow"
            case 22: {pr_weather = [    0, [    0,     0,     0],    .8,       0.0, [_windDir, _dirChange, _spdLrg, _spdChange, _period], date,   1];};  //"Blizzard"
            case 23: {pr_weather = [0,[0,0,0],0,0,date, 0];};                                                                                            //"Blank - Default"
            case 24: {pr_weather = [  0.3, [  0.12,  0.5,   7.5],  0.7,        0,   [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.3];};  //"Steamy Water"
            case 25: {pr_weather = [    0, [  0.15,  0.02,   250],  0.72,       0,   [_windDir, _dirChange, _spdSml, _spdChange, _period], date, 0.7];};  //"Foggy Rain"
        };
        publicVariable "pr_weather";
        weatherMissionInit = true; publicVariable "weatherMissionInit";
    };

    waitUntil { (weatherMissionInit) };

    _fogEnabled = (["FogEnabled", 1] call BIS_fnc_getParamValue);
    switch (_fogEnabled) do {
        case 0: {pr_fog = 0};  // "Disable Fog"
        case 1: {pr_fog = 1};  // "Enable Fog"
    };
    _fogTrue = pr_fog;

    _waves = (["WavesEnabled", -1] call BIS_fnc_getParamValue);

    if ((local player) && !(isServer)) then {
        if (isNil "client_weatherstart") then { client_weatherstart = true; publicVariable "client_weatherstart"; };
        if (client_weatherstart) then {
            86400 setRain ((pr_weather select 0) Min 1);
            if (_fogTrue != 0) then { 86400 setFog (pr_weather select 1); } else { 86400 setFog [0,0,0]; };
            86400 setOvercast ((pr_weather select 2) Min 1);
            if (_waves == -1) then { 86400 setWaves ((pr_weather select 6) Min 1); } else { 86400 setWaves _waves; };
            86400 setLightnings ((pr_weather select 3) Min 1);
            skipTime 24;
            0 = [] spawn { sleep 0.1; simulWeatherSync; };
        } else {
            60 setRain (pr_weather select 0);
            if (_fogTrue != 0) then { 60 setFog (pr_weather select 1); } else { 60 setFog [0,0,0]; };
            60 setOvercast ((pr_weather select 2) Min 1);
			//60 setWaves ((pr_weather select 6) Min 1);
            if (_waves == -1) then { 86400 setWaves ((pr_weather select 6) Min 1); } else { 86400 setWaves _waves; };
            60 setLightnings ((pr_weather select 3) Min 1);
        };
    };

    if !(isServer) exitWith {};

    86400 setRain ((pr_weather select 0) Min 1);
    if (_fogTrue != 0) then { 86400 setFog (pr_weather select 1); } else { 86400 setFog [0,0,0]; };
    86400 setOvercast ((pr_weather select 2) Min 1);
	//86400 setWaves ((pr_weather select 6) Min 1);
    if (_waves == -1) then { 86400 setWaves ((pr_weather select 6) Min 1); } else { 86400 setWaves _waves; };
    86400 setLightnings ((pr_weather select 3) Min 1);
    skipTime 24;
    0 = [] spawn { sleep 0.1; simulWeatherSync; };
    if (aceOn) then {
        ACE_WIND_PARAMS = pr_weather select 4; publicVariable "ACE_WIND_PARAMS"; //ACE_WIND_PARAMS params ["_dir", "_dirChange", "_spd", "_spdChange", "_period"];

        _rainOld = pr_weather select 0;
        _rainCurrent = pr_weather select 0;
        ACE_RAIN_PARAMS = [_rainOld,_rainCurrent, 60]; publicVariable "ACE_RAIN_PARAMS"; //ACE_RAIN_PARAMS = [lastRain, currentRain, transitionTime];

        //ACE_MISC_PARAMS = [       lightnings, rainbow,           fogParams,       GVAR(temperatureShift),       GVAR(badWeatherShift),       GVAR(humidityShift)]; publicVariable "ACE_MISC_PARAMS";
        //                  [         0.261459,    0.35,    [0.00197362,0,0],                     0.217289,                     3.41477,               -0.00222516]
        ACE_MISC_PARAMS = [pr_weather select 3, rainbow, pr_weather select 1, ace_weather_temperatureShift, ace_weather_badWeatherShift, ace_weather_humidityShift]; publicVariable "ACE_MISC_PARAMS";

        sleep 20;
        client_weatherstart = false; publicVariable "client_weatherstart";
        sleep _timeForecast;

        _ForecastChange = (["ForecastChange", 0] call BIS_fnc_getParamValue);
        switch (_ForecastChange) do {
            case 0:  { pr_weatherChg = 0 };  // "No Change"
            case 1:  { pr_weatherChg = 1 };  // "Change to Good Weather"
            case 2:  { pr_weatherChg = 2 };  // "Change to Bad Weather"
            case 3:  { pr_weatherChg = 3 };  // "Random"
        };
        if ((prWeather == 20) || (prWeather == 21) || (prWeather == 22)) then { pr_weatherChg = 0; };
        _randomChange = "";
        _lightnings = 0;
        //--- Server Loop ---//
        while { true } do {
            private ["_period", "_aceRainOld", "_rainOld", "_rainChange", "_rainCurrent", "_aceWindOld", "_oldWindDir", "_dirChange", "_newWindDir", "_oldSpeed", "_spdChange", "_newSpeed"];
            _period = 60;

            _aceWindOld = ACE_WIND_PARAMS;
            _aceRainOld = ACE_RAIN_PARAMS;
            _aceMiscOld = ACE_MISC_PARAMS;

            _newWindDir = (_aceWindOld select 0) + (round (random 40) -20);
            _newSpeed = (((_aceWindOld select 2) + ((((random 2) - 1) Max -1) Min 1)) Max 0) Min 5;
            _dirChange = 0;
            _spdChange = 0;
            ACE_WIND_PARAMS = [_newWindDir, _dirChange, _newSpeed, _spdChange, _period]; publicVariable "ACE_WIND_PARAMS";

            _lightingOld = _aceMiscOld select 0;

            _rainChange = 0;
            _rainOld = _aceRainOld select 1;
            if (pr_weatherChg == 0) then {  // "No Change"
                _lightnings = _lightingOld;
            } else {
                if (pr_weatherChg == 1) then {  // "Change to Good Weather"
                    _rainChange = ((random -0.05) Max -0.05) Min 0;
                    _timeForecast setOvercast ((((overcast) - 0.2) Max 0) Min 1);
                    _lightnings = ((((_lightingOld) - 0.05) Max 0) Min 1);
                } else {
                    if (pr_weatherChg == 2) then {  // "Change to Bad Weather"
                        _rainChange = ((random 0.05) Max 0) Min 0.05;
                        _timeForecast setOvercast ((((overcast) + 0.2) Max 0) Min 1);
                        _lightnings = ((((_lightingOld) + 0.05) Max 0) Min 1);
                    } else {
                        if (pr_weatherChg == 3) then {  // "Random"
                            if (isNil "randomChange") then {
                                randomChange = true;
                                _randomChange = ["GOOD", "BAD"]  call BIS_fnc_selectRandom;
                            };
                            if (_randomChange == "GOOD") then {
                                _rainChange = ((random -0.05) Max -0.05) Min 0;
                                _timeForecast setOvercast ((((overcast) - 0.2) Max 0) Min 1);
                                _lightnings = ((((_lightingOld) - 0.05) Max 0) Min 1);
                            };
                            if (_randomChange == "BAD") then {
                                _rainChange = ((random 0.05) Max 0) Min 0.05;
                                _timeForecast setOvercast ((((overcast) + 0.2) Max 0) Min 1);
                                _lightnings = ((((_lightingOld) + 0.05) Max 0) Min 1);
                            };
                        };
                    };
                };
            };

            _rainCurrent = ((_rainOld + _rainChange) Max 0) Min overcast;
            ACE_RAIN_PARAMS = [_rainOld, _rainCurrent, _period]; publicVariable "ACE_RAIN_PARAMS";
        
            _timeForecast setLightnings _lightnings;
            _rainbows = ((_aceMiscOld select 1) + (((((random 0.2) - 0.1) Max -0.1) Min 0.1)) Max 0) Min 1;

            if((_rainOld > 0.6) and (_rainCurrent < 0.4)) then {
                _fogDecay = true;
            };

            _fogParms = [0,0,0];
            if (_fogTrue != 0) then {
                _fog0 = (((_aceMiscOld select 2) select 0) + ((((random 0.05) - 0.025) Max -0.025) Min 0.025) Max 0) Min 1;
                _fog1 = (((_aceMiscOld select 2) select 1) + ((((random 0.01) - 0.005) Max -0.005) Min 0.005) Max 0) Min 0.5;
                _fog2 = (((_aceMiscOld select 2) select 2) + ((((random 2) - 1) Max -1) Min 1) Max 0) Min 100;
                _fogParms = [_fog0, _fog1, _fog2];
            } else {
                _fogParms = _aceMiscOld select 2;
            };

            _temperatureShift = (((random 6) - 3) Max -3) Min 3;
            _badWeatherShift = ((random 10) Max 0) Min 10;
            _humidityShift = (((random 0.1) - 0.05) Max -0.05) Min 0.05;
            //ACE_MISC_PARAMS = [pr_weather select 3, rainbow, pr_weather select 1, _temperatureShift, _badWeatherShift, _humidityShift]; publicVariable "ACE_MISC_PARAMS";
            ACE_MISC_PARAMS = [_lightnings, _rainbows, _fogParms, _temperatureShift, _badWeatherShift, _humidityShift]; publicVariable "ACE_MISC_PARAMS";

            sleep _timeForecast;
        };
    };
};
