//--- stormyWeather.sqf
//--- Adds foggy breath, wind sounds and snow effects

if ((prWeather == 19) || (prWeather == 20) || (prWeather == 21) || (prWeather == 22)) then { 
    fn_foggyBreath = compile preprocessFileLineNumbers "scripts\PR\scripts\weather\fn_foggyBreath.sqf"; 
    fn_windSound = compile preprocessFileLineNumbers "scripts\PR\scripts\weather\fn_windSound.sqf"; 
    fn_snowfall = compile preprocessFileLineNumbers "scripts\PR\scripts\weather\fn_snowfall.sqf"; 
    fn_snowFall2 = compile preprocessFileLineNumbers "scripts\PR\scripts\weather\fn_snowFall2.sqf"; 

    if (!isDedicated) then { 
        if (prWeather == 19) then { 
            { nul = [_x, 0.01] spawn fn_foggyBreath } forEach units (group player); 
            nul = [] spawn fn_windSound; 
        } else {  
            if (prWeather == 20) then { 
                { nul = [_x, 0.01] spawn fn_foggyBreath } forEach units (group player); 
                nul = [] spawn fn_windSound; 
                nul = [] spawn fn_snowfall; 
            } else {  
                if (prWeather == 21) then { 
                    { nul = [_x, 0.01] spawn fn_foggyBreath } forEach units (group player); 
                    nul = [] spawn fn_windSound; 
                    nul = [] spawn fn_snowfall; 
                } else { 
                    if (prWeather == 22) then { 
                        { nul = [_x, 0.01] spawn fn_foggyBreath } forEach units (group player); 
                        nul = [] spawn fn_windSound; 
                        nul = [] spawn fn_snowfall; 
                        nul = [] spawn fn_snowFall2; 
                    }; 
                }; 
            }; 
        }; 
    }; 
}; 
