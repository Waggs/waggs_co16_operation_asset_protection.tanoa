// Credit to Skull for his contribution on this script 
// SKL_ArtySpotter - V2.0
//
// will fire a given number of shells at any detect BLUFOR 
// accuracy is based upon how well they know where the target is
//
// in spotters init.sqf: _nil = [this,ArtyArray,TypeShells,numShells,hitTarget] execVM "custom_scripts\SKL_ArtySpotter.sqf"
//
//                       _nil = [this,[art1,art2,art3],0,2,true,(random 15) max 10] execVM "custom_scripts\SKL_ArtySpotter.sqf" 
//                       _nil = [_grp, [_veh], _typeShell, _numShell, _hitTarget, _sleep] execVM "scripts\SKULL\SKL_ArtySpotter.sqf"; 
// Optional arguments:
// typeShells: 0 = 8Rnd_82mm_Mo_shells (default)
//             1 = 8Rnd_82mm_Mo_Smoke_white
//             2 = 8Rnd_82mm_Mo_Flare_white
//
// numShells: default 1
//
// hitTarget: if true, then aim centered on unit +/- and error, if false, then don't actually ever hit the target (default true)
//
// Version 1.0  2-13-14
// 1.0: initial release
// _nil = [[_grp, true], [_veh], _typeShell, _numShells, _hitTarget, _sleep] execVM "scripts\SKULL\SKL_ArtySpotter.sqf"; 

private ["_targets","_artys","_type","_numShells","_arty","_ka","_pos","_target","_error","_px","_y", "_hitTarget","_hitpos","_tooClose"]; 
//_unit = _this select 0; 
_grp      = _this select 0; 
_isUpsmon = false; 
if (typeName _grp == "ARRAY") then { 
    _grp      = (_this select 0) select 0; 
    _isUpsmon = (_this select 0) select 1; 
}; 

_unit = leader _grp; 
_artys = _this select 1; 
_type  = "8Rnd_82mm_Mo_shells"; 
_numShells = 1; 
_hitTarget = true; 
_sleep = 30 + random 15; 
if (count _this > 2) then { _type = ["8Rnd_82mm_Mo_shells","8Rnd_82mm_Mo_Smoke_white","8Rnd_82mm_Mo_Flare_white"] select (_this select 2) }; 
if (count _this > 3) then { _numShells = _this select 3 }; 
if (count _this > 4) then { _hitTarget = _this select 4 }; 
if (count _this > 5) then { _sleep = _this select 5 }; 
_ka = 4; 
_enemySide = WEST; if (side _unit == WEST) then { _enemySide = EAST }; 
if (!local _unit) exitWith {}; // BELEIVE THIS IS NECCESSARY

{ 
    gunner _x setUnitAbility 0.5; 
    gunner _x setSkill 0.5; 
    gunner _x setSkill ["spotDistance", 2]; 
    gunner _x setSkill ["spotTime", 1]; 
    gunner _x setCombatMode "RED"; 
    gunner _x setBehaviour "COMBAT"; 
} forEach _artys; 

_unit setSkill ["spotDistance", 0.85]; 
_unit setSkill ["spotTime", 0.85]; 
_c = 0; 

while { ((count units _grp > 0) && ({ alive gunner _x } count _artys > 0)) } do { 
    if !(alive _unit) then { _c = 1 }; 
    if (_c == 1) then { 
        _unit = leader _grp; 
        _unit setSkill ["spotDistance", 0.85]; 
        _unit setSkill ["spotTime", 0.85]; 
        if (_isUpsmon) then { 
            _r_final_array = _grp getVariable "upsArray"; 
            [_r_final_array] spawn { 
                private ["_r_final_array"]; 
                _r_final_array = _this select 0; 
                sleep 1; 
                _nil = _r_final_array execVM "scripts\UPSMON\upsmon.sqf"; 
            }; 
        }; 
        _c = 0; 
    }; 

    sleep (8 + random 4); 
    { 
        _target = _x; 
        if (side _target == _enemySide) then { 
            _ka = _unit knowsAbout _target; 
            if (_ka > 10) then { _ka = 0 }; 
            if (_ka >= 1) then { 
                { 
                    _arty = _x; 
                    if (_arty distance _target < 2000) then { 
                        _pos = getPos (_target); 
                        _error = 240/_ka; 
                        if ((_target distance _unit > _error + 20) && (_target distance _arty > _error + 20)) then { 
                            _px = random (2*_error) - _error; 
                            _py = random (2*_error) - _error; 
                            _hitpos = [(_pos select 0) + _px, (_pos select 1) + _py, _pos select 2]; 
                            
                            if (!_hitTarget) then { 
                                while { ({ _x distance _hitpos < 100 } count playableUnits) != 0 } do { 
                                    _px = random (3*_error) - 1.5 * _error; 
                                    _py = random (3*_error) - 1.5 * _error; 
                                    _hitpos = [(_pos select 0) + _px, (_pos select 1) + _py, _pos select 2]; 
                                }; 
                            }; 

                            _arty setVehicleAmmo 1; 
                            _arty commandArtilleryFire [_hitpos, _type, _numShells]; 
                            //sleep random 3; 
                            sleep _sleep; 
                        }; 
                    }; 
                } forEach _artys; 
                sleep 5; 
            }; 
        }; 
    } forEach playableUnits - noArty;  //allUnits;
}; 

if (isNil "artySpotterInit") then { artySpotterInit = true }; 
