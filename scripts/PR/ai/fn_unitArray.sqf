

pr_medicArray = [ 
    // Default Arma 
    "B_medic_F","B_recon_medic_F","B_G_medic",
    "I_medic_F",
    "O_medic_F","O_soldierU_medic_F"
]; 

pr_sniperArray = [ 
    // Default Arma 
    "B_soldier_M_F","B_Sharpshooter_F","B_recon_M_F","B_Recon_Sharpshooter_F","B_sniper_F",
    "B_ghillie_ard_F","B_ghillie_lsh_F","B_ghillie_sard_F"
]; 

pr_opticsArray = [ 
    // Default Arma 
    "optic_Hamr","optic_Arco","optic_AMS","optic_SOS","optic_LRPS",
    // Massi 
    "optic_mas_Arco_blk","optic_mas_DMS",

    //RHS 
    "rhs_acc_pkas","rhsusf_acc_LEUPOLDMK4_2"
]; 

unitArrayCompiled = true; 
