//--- r_GuerClothing.sqf
//--- ver 1.1 - 2015-06-17 fixed items not placed back to unit 
//--- ver 1.0 - 2015-05-28
//--- Usage in the units init:  _nil = [this] execVM "scripts\PR\spawnUnits\r_urbanClothing.sqf";

waitUntil {!isNil "bis_fnc_init"}; 

_r_urbanClothes = ["U_O_CombatUniform_oucamo"]; 
_r_urbanVests = ["V_HarnessO_gry","V_TacVest_blk","V_HarnessOGL_gry"]; 
_r_urbanHats = ["H_HelmetO_oucamo","H_HelmetLeaderO_oucamo"]; 
 
_unit = _this select 0; 

_unitormContainer = uniformContainer _unit; 
_items = getItemCargo _unitormContainer; 
_magazines = getMagazineCargo _unitormContainer; 

_vestContainer = vestContainer _unit; 
_itemsVest = getItemCargo _vestContainer; 
_magazinesVest = getMagazineCargo _vestContainer; 
 
removeUniform _unit; 
removeVest _unit; 
removeHeadgear _unit; 

_cloth_item = _r_urbanClothes call BIS_fnc_selectRandom; 
_unit forceAddUniform  _cloth_item; 

_vest_item = _r_urbanVests call BIS_fnc_selectRandom; 
_unit addVest _vest_item; 

//--- add items and magazines back to uniform 
_unitormContainer = uniformContainer _unit; 
_count = count (_items select 0) - 1; 
for "_i" from 0 to _count do { 
    _unitormContainer addItemCargoGlobal [(_items select 0) select _i, (_items select 1) select _i]; 
}; 
_count = count (_magazines select 0) - 1; 
for "_i" from 0 to _count do { 
    _unitormContainer addMagazineCargoGlobal [(_magazines select 0) select _i, (_magazines select 1) select _i]; 
}; 

//--- add items and magazines back to vest 
_vestContainer = vestContainer _unit; 
_count = count (_itemsVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i]; 
}; 

_count = count (_magazinesVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i]; 
}; 

//--- add headgear back 
_hat_item = _r_urbanHats call BIS_fnc_selectRandom; 
_unit addHeadgear _hat_item; 
 