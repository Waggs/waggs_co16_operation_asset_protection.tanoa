/* 
*  Author: PapaReap 
*  function name: pr_fnc_unitSpawn 
*  ver 1.3 2016-01-25 code clean-up 
*  ver 1.2 2015-12-14 added move in vehicle, code clean-up 
*  ver 1.1 2015-06-22 convert for all side's usage and clean-up 
*  ver 1.0 2015-05-28 

* UPSMON parameters - https://dev.withsix.com/projects/upsmon/wiki/UPSMONsqf 

*  Arguments: 
*  0: <SPAWN OBJECT/MARKER>       e.g. - (REQUIRED) "marker1", gameLogic, thisTrigger or [x, y, z] position
*  0: <OPT ARRAY> 
*     0: <SPAWN OBJECT/MARKER>    e.g. - (REQUIRED) see: above 
*     1: <RANDOM DISTANCE/SIDE>   e.g. - 200 or "west", use one or the other. If random distance set to 0, then spawn will be exact position.
*     2: <SIDE/RANDOM DISTANCE>   e.g. - "west" or 200, use one or the other 

*  1: <TEAM/UNIT>                 e.g. - (REQUIRED) r_fireteam or r_sniper + r_fireteam or ["O_soldier_LAT_F"] 
*  1: <OPT TEAM ARRAY>            e.g. - [r_fireteam, "area1", ["careless", "onroad"]] or [["O_soldier_LAT_F"], "area1", ["careless", "onroad"]] 
*     0: <TEAM/UNIT>              e.g. - (REQUIRED) see: above 
*     1: <OPT AREA MARKER>        e.g. - "area1" (existing area marker name) or "" (will use a default 50x50 marker) or [30, 45] (axis A, axis B) or [30, 45, 60] (axis A, axis B, direction) 
*     2: <OPT UPSMON PARAMETERS>  e.g. - ["combat", "onroad"] or ["fortify"]  etc... --- no limit on upsmon parameters (see: UPSMON parameters) 

*  2: <OPT RESTOCK>               e.g. - (OPTIONAL) leave blank if not desired, (NOTE) use "", if not used and using <PLACE IN VEHICLE ARRAY> below 
*  2: <OPT RESTOCK ARRAY>         e.g. - (OPTIONAL) ["ammo", 360] 
*     0: <FUEL/AMMO/BOTH>         e.g. - "AMMO", "FUEL", "BOTH" 
*     1: <SECONDS>                e.g. - 360, seconds between restock (seconds) 

*  3: <PLACE IN VEHICLE ARRAY>    e.g. - ["cargo", car_1]  --- optional
*     0: <CARGO/DRIVER>           e.g. - "cargo" 
*     1: <VEHICLE NAME>           e.g. - car_1 
*   EXAMPLES
    [spawnTrg, [r_fireteam, "area1"]] call pr_fnc_unitSpawn; 
    [spawnTrg, [r_fireteam, "area1", ["COMBAT"]]] call pr_fnc_unitSpawn; 
    [spawnTrg, [r_fireteam, "area1", ["COMBAT"]],[],["cargo", car_1"]] call pr_fnc_unitSpawn; 
    [spawnTrg, [r_fireteam, "area1", ["COMBAT","FORTIFY"]]] call pr_fnc_unitSpawn; 
    [spawnTrg,[["O_soldier_LAT_F"], "area1", ["CARELESS"]] call pr_fnc_unitSpawn; 
    [spawnTrg, [r_tank, "area1", ["onroad"]], ["fuel", 360]] call pr_fnc_unitSpawn; 
    [[spawnTrg,200], [r_fireteam, "area1", ["STEALTH"]]] call pr_fnc_unitSpawn; 
    [[spawnTrg,200], [r_fireteam + r_fireteam, "area1", ["COMBAT"]]] call pr_fnc_unitSpawn; 
    [[spawnTrg,"west"], [r_fireteam, "area1"]] call pr_fnc_unitSpawn; 
    [[spawnTrg,50,"west"], [r_fireteam, "area1"]] call pr_fnc_unitSpawn; 
    [[spawnTrg,"west",50], [r_fireteam, "area1"]] call pr_fnc_unitSpawn; 
    [thisTrigger, [r_fireteam, [50,80,45], ["careless"]]] call pr_fnc_unitSpawn; 

    [r_armor] call r_randomSpawn; [thisTrigger, [randomUnit, [400,400]]] call pr_fnc_unitSpawn; 
	[[thisTrigger,"west"], [b_fireteam, "area2"],["nofollow"],["cargo", Boat_4]] call pr_fnc_unitSpawn; 0 = [thisTrigger, 30] spawn pr_fnc_delObject;
*/

if (!isServer && hasInterface) exitWith {};
if ((isNull aiSpawnOwner) && !(isServer)) exitWith {};
if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {};

_this spawn {
    //[[], "pr_fnc_aiSpawner"] call BIS_fnc_MP;
    _spawn = _this select 0;
    _spawnPos = [0,0,0];
    _side = EAST;
    if (prEnemySide == 1) then { _side = WEST; };
    if (prEnemySide == 2) then { _side = EAST; };
    if (prEnemySide == 3) then { _side = RESISTANCE; };

    _name1 = 1;
    _name2 = 1;
	_randomPos = false;

    if (typeName _spawn == "ARRAY") then {
        if (typeName (_spawn select 0) == "STRING") then {
            _spawnPos = getMarkerPos (_spawn select 0);
        } else {
            if (typeName (_spawn select 0) == "ARRAY") then {
                _spawnPos = _spawn select 0;
            } else {
                _spawnPos = getPos (_spawn select 0);
            };
        };
        if (count _spawn > 1) then {
            _name1 = _spawn select 1;
            if (typeName _name1 == "STRING") then {
                _side = _spawn select 1;
                _side = toUpper _side;
                switch (_side) do {
                    case "EAST": { _side = EAST; };
                    case "WEST": { _side = WEST; };
                    case "RESISTANCE": { _side = RESISTANCE; };
                    case "CIVILIAN": { _side = CIVILIAN; };
                    default { _side = EAST; };
                };
            } else {
                //if !(_name1 == 0) then {
					_randomPos = true;
                    _pos = _spawnPos;
                    _randomDistance = _spawn select 1;
                    _rx = random (2 * _randomDistance) - _randomDistance;
                    _ry = random (2 * _randomDistance) - _randomDistance;
                    _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
                    _spawnPos = _randomSpawn;
                //};
            };
        };

        if (count _spawn > 2) then {
            _name2 = _spawn select 2;
            if (typeName _name2 == "STRING") then {
                _side = _spawn select 2;
                _side = toUpper _side;
                switch (_side) do {
                    case "EAST": { _side = EAST };
                    case "WEST": { _side = WEST };
                    case "RESISTANCE": { _side = RESISTANCE };
                    case "CIVILIAN": { _side = CIVILIAN };
                    default { _side = EAST };
                };
            } else {
                //if !(_name2 == 0) then {
					_randomPos = true;
                    _pos = _spawnPos;
                    _randomDistance = _spawn select 2;
                    _rx = random (2 * _randomDistance) - _randomDistance;
                    _ry = random (2 * _randomDistance) - _randomDistance;
                    _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
                    _spawnPos = _randomSpawn;
                //};
            };
        };
    } else {
        if (typeName _spawn == "STRING") then {
            _spawnPos = getMarkerPos _spawn;
        } else {
            _spawnPos = getPos _spawn;
        };
    };
    _newPos = _spawnPos; z_name1=_name1; z_name2=_name2;

    //if (!(_name1 == 0) && !(_name2 == 0)) then {
    if !(_randomPos) then {
        // position isFlatEmpty [float minDistance, float precizePos, float maxGradient, float gradientRadius, float onWater, bool onShore, object skipobj]
        _newPos = _spawnPos isFlatEmpty [
            5,       //--- Minimal distance from another object
            10,      //--- If 0, just check position. If >0, select new one
            0.7,     //--- Max gradient
            2,       //--- Gradient area
            0,       //--- 0 for restricted water, 2 for required water,
            false,   //--- True if some water can be in 25m radius
            ObjNull  //--- Ignored object
        ];
    };

    while { (count _newPos < 1) } do {  //Loop the following code so long as isFlatEmpty cannot find a valid position near the current _pos.
        _pos = _spawnPos;
        _randomDistance = 30;
        _rx = random (2 * _randomDistance) - _randomDistance;
        _ry = random (2 * _randomDistance) - _randomDistance;
        _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
        _spawnPos = _randomSpawn;
        _newPos = _spawnPos isFlatEmpty [10, 1, 0.7, 40, 0, false, ObjNull];
    };

    _teamA = [_this, 1, [], [[]]] call BIS_fnc_param;
    _team = [_teamA, 0, []] call BIS_fnc_paramIn;
    _grp = [_newPos, _side, _team] call BIS_fnc_spawnGroup;
    _cnt = count units _grp;
    if !(pr_deBug == 0) then { diag_log format ["*PR* aiSpawner: %1 has spawned %2, pr_fnc_unitSpawn %3 units", aiSpawnOwner, _grp, _cnt]; };

    if (name aiSpawnOwner == "hc") then {
        { hcUnits = hcUnits + [_x] } forEach units _grp; publicVariable "hcUnits";
    } else {
        if (name aiSpawnOwner == "hc2") then {
            { hc2Units = hc2Units + [_x] } forEach units _grp; publicVariable "hc2Units";
        } else {
            { serverUnits = serverUnits + [_x] } forEach units _grp; publicVariable "serverUnits";
        };
    };

    //if (pr_addDragToAll) then { { _nil = [_x] spawn pr_fnc_dragAddDrag; } forEach units _grp; };
    if (pr_addDragToAll) then { { [_x] remoteExec ["pr_fnc_dragAddDrag", 0, true]; } forEach units _grp; };

    if ((prEnemyUnits == 2) || (prEnemyUnits == 3) || (prEnemyUnits == 4)) then {
        if (_side == EAST) then {
            if (prEnemyUnits == 2) then {  //---  Non-existent soldier, use default soldier and add Guerilla clothing
                if ((_team select 0) == "o_g_soldier_universal_f") then { deleteVehicle leader _grp; { [_x] spawn pr_fnc_r_GuerClothing } forEach units _grp };
            } else {
                if (prEnemyUnits == 3) then {  //---  Non-existent soldier, use default soldier and add Urban clothing
                    if ((_team select 0) == "o_g_soldier_universal_f") then { deleteVehicle leader _grp; { [_x] spawn pr_fnc_r_urbanClothing } forEach units _grp };
                } else {
                    if (prEnemyUnits == 4) then {  //---  Non-existent soldier, use default soldier and add Taliban clothing
                        { [_x] spawn pr_fnc_r_TalibanClothing } forEach units _grp;
                    };
                };
            };
        };
    };

    if ((_team select 0) in r_sniper) then {
        if ((_team select 0) in r_sniper) then { [((units _grp) select 0)] spawn pr_fnc_r_Sniper }; //pr_team1 = (units _grp) select 0;
        if ((_team select 1) in r_sniper) then { [((units _grp) select 1)] spawn pr_fnc_r_spotter }; //pr_team2 = (units _grp) select 1;
    };

    //upsmon stuff ****
    _isUpsmon = false;
    _area = "";
    if (count _teamA > 1) then {
        _isUpsmon = true;
        _area = _teamA select 1;
        if (count _teamA > 2) then {
            _upsmonA = _teamA select 2; [_grp, _area, _upsmonA] call PR_fnc_upsSpawner;
        } else {
            [_grp, _area] call PR_fnc_upsSpawner;
        };
    };

    //--- Adds restocking vehicles: [("fuel", "ammo"), seconds]
    _restock = [_this, 2, "", [[]]] call BIS_fnc_param; //["fuel", 360]
    _item = [_restock, 0, ""] call BIS_fnc_paramIn;
    _ReloadTime = [_restock, 1, 500] call BIS_fnc_paramIn;
    _leader = leader _grp;
    if (_item != "") then { [[[_leader, _item, _ReloadTime], "scripts\PR\scripts\vehicle\restock.sqf"], "BIS_fnc_execVM", leader _grp, false] call BIS_fnc_MP };

    //--- Special Init for spawned group to move into vehicle ie: [("cargo", "driver"), car_1]
    _specialInitArray = [_this, 3, "",[[]]] call BIS_fnc_param;
    _specialInit = [_specialInitArray, 0, ""] call BIS_fnc_param;
    _specialInit2 = [_specialInitArray, 1, []] call BIS_fnc_param;
    if (_specialInit != "") then {
        for [{ _loop = 0 }, { _loop < count units _grp }, { _loop = _loop + 1 }] do {
            {
                if (_specialInit == "cargo") then { _x moveInCargo _specialInit2 };
                if (_specialInit == "driver") then { _x moveInCargo _specialInit2; /*(leader _grp) moveInDriver _specialInit2*/ };
            } forEach units _grp;
        };
        if (_specialInit == "driver") then { (leader _grp) moveInDriver _specialInit2 };
    };
    [[], "pr_fnc_aiSpawner"] call BIS_fnc_MP;
};
