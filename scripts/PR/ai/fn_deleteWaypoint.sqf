// [z_heliGroup, true] call pr_fnc_deleteWaypoint
/* fn_deleteWaypoint.sqf
*  Author: PapaReap
*  function name: pr_fnc_deleteWaypoint
*  Arguments:
*  0: GROUP    - Required, group the waypoint is attached to
*  1: WAYPOINT - (Optional), if true, then delete all waypoints (bool)

*  usage:    - [heliGroup] call pr_fnc_deleteWaypoint, [heliGroup, true] call pr_fnc_deleteWaypoint
*/

_group = _this select 0;
_wpAll = false;
if (count _this > 1) then {
    _wpAll = _this select 1;
};

if (_wpAll) then {
    // delete all waypoints
    while { (count (waypoints _group)) > 0 } do {
        deleteWaypoint ((waypoints _group) select 0);
    };
} else {
    // delete current waypoint
    deleteWaypoint [_group, currentWaypoint (_group)];
    //deleteWaypoint [group this, currentWaypoint (group this)];
};