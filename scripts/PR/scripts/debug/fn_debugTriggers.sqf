//WS_fnc_debugTriggers
//Creates various triggers to be used ingame. Does not need to run twice

_debug = false; if !(isNil "pr_debug") then {_debug = pr_debug}; 
if !(_debug) exitWith {}; 
if !(isNil "pr_dbg_trg") exitWith {}; 


player allowDammage false; 
if (ws_game_a3) then { player addAction ["Start Camera","[] call bis_fnc_camera;", [], 2, false, true, "", "driver _target == _this"]; } else { onMapSingleClick "player setPos _pos"; }; 

//Radio triggers to assist with debugging 

//Count units 
pr_dbg_trg = createTrigger ["EmptyDetector", [0,0,0]]; 
pr_dbg_trg setTriggerArea [0,0,0,false]; 
pr_dbg_trg setTriggerActivation ["GOLF", "PRESENT", true]; 
pr_dbg_trg setTriggerStatements ["this", "call pr_fnc_countUnits", ""]; 
pr_dbg_trg setTriggerText "Count units"; 

//Copy player position 
pr_dbg_trg = createTrigger ["EmptyDetector", [0,0,0]]; 
pr_dbg_trg setTriggerArea [0,0,0,false]; 
pr_dbg_trg setTriggerActivation ["HOTEL", "PRESENT", true]; 
pr_dbg_trg setTriggerStatements ["this", "call pr_fnc_copyPos", ""]; 
pr_dbg_trg setTriggerText "Copy player position"; 

//Clipboard code 
pr_dbg_trg = createTrigger ["EmptyDetector", [0,0,0]]; 
pr_dbg_trg setTriggerArea [0,0,0, false]; 
pr_dbg_trg setTriggerActivation ["INDIA", "PRESENT", true]; 
pr_dbg_trg setTriggerStatements ["this", "call pr_fnc_clipboardcode", ""]; 
pr_dbg_trg setTriggerText "execute code from clipboard"; 

//Recompile 
pr_dbg_trg = createTrigger ["EmptyDetector", [0,0,0]]; 
pr_dbg_trg setTriggerArea [0,0,0,false]; 
pr_dbg_trg setTriggerActivation ["JULIET", "PRESENT", true]; 
if !(ws_game_a3) then { 
    pr_dbg_trg setTriggerStatements ["this", "pr_fnc_compiled = false; pr_fnc_compiled = false; publicVariable ""pr_fnc_compiled""; nul = [] execVM ""scripts\PR\scripts\init\fn_preInit.sqf"";", ""]; 
} else { 
    pr_dbg_trg setTriggerStatements ["this", "pr_fnc_compiled = false; publicVariable ""pr_fnc_compiled""; [] call BIS_fnc_Recompile;", ""]; 
}; 
pr_dbg_trg setTriggerText "recompile all pr_fnc"; 
