//pr_fnc_clipboardcode
//run code from clipboard
//call pr_fnc_clipboardcode

private ["_code","_error"]; 

_code = copyFromClipboard; 
_error = [_code, ["STRING", "CODE"], "pr_fnc_clipboardcode"] call pr_fnc_typecheck; 
if (_error) exitWith { ["pr_fnc_clipboardcode DBG: ", _code, " invalid. exiting."] call pr_fnc_debugtext }; 
["pr_fnc_clipboardcode DBG: ", _code, " executing."] call pr_fnc_debugtext; 
[] spawn (compile _code); 
