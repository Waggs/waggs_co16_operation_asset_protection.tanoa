//--- chem attachTo [c130_1, [0.0, 0, 3.85]]; chem setPos getPos chem; 
//--- [chem, c130_1, [0.0, 0, 3.85]] 
//--- if (isServer) then { [[chem, c130_1, [0.0, 0, 3.85]], "pr_fnc_attachPart"] spawn BIS_fnc_MP; }; 
//--- if (isServer) then { [[chem, c130_1, [0.0, 0, 3.85], true], "pr_fnc_attachPart"] spawn BIS_fnc_MP; }; 


_part = _this select 0; 
_host = _this select 1; 
_posArray = _this select 2; 
_loop = false; 
if (count _this > 3) then { _loop = _this select 3; }; 

_posX = 0; 
_posY = 0; 
_posZ = 0; 
_posX = _posArray select 0; 
_posY = _posArray select 1; 
_posZ = _posArray select 2; 

if (_loop) then { 
    _part attachTo [_host, [_posX, _posY, _posZ]]; 
    _part setPos getPos _part; 

    while { (alive _part) } do { 
        sleep 0.5; 
        if (isNull attachedTo _part) then { 
            _part attachTo [_host, [_posX, _posY, _posZ]]; 
            _part setPos getPos _part; 
        }; 
    }; 
} else { 
    _part attachTo [_host, [_posX, _posY, _posZ]]; 
    _part setPos getPos _part; 
}; 

/*
if (isServer) then { 
[[chem, c130_1, [0.0, 0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem_1, c130_1, [0.0, 3.5, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem_2, c130_1, [0.0, 7.0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
}; 

chem attachTo [c130_1, [0.0, 0, 3.85]]; chem setPos getPos chem;   chem_1 attachTo [c130_1, [0.0, 3.5, 3.85]]; chem_1 setPos getPos chem_1;   chem_2 attachTo [c130_1, [0.0, 7.0, 3.85]]; chem_2 setPos getPos chem_2; 

if (isServer) then { 
[[chem1, c130_1, [0.0, 0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem1_1, c130_1, [0.0, 3.5, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem1_2, c130_1, [0.0, 7.0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
}; 

{ deleteVehicle _x } forEach chemA; chem1 attachTo [c130_1, [0.0, 0, 3.85]]; chem1 setPos getPos chem1; chem1_1 attachTo [c130_1, [0.0, 3.5, 3.85]]; chem1_1 setPos getPos chem1_1; chem1_2 attachTo [c130_1, [0.0, 7.0, 3.85]]; chem1_2 setPos getPos chem1_2; 

{ deleteVehicle _x } forEach chem1A; 
if (isServer) then { 
[[chem2, c130_1, [0.0, 0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem2_1, c130_1, [0.0, 3.5, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
[[chem2_2, c130_1, [0.0, 7.0, 3.85], true], "pr_fnc_attachPart"] call BIS_fnc_MP;
}; 
{ deleteVehicle _x } forEach chem1A; chem2 attachTo [c130_1, [0.0, 0, 3.85]]; chem2 setPos getPos chem2; chem2_1 attachTo [c130_1, [0.0, 3.5, 3.85]]; chem2_1 setPos getPos chem2_1; chem2_2 attachTo [c130_1, [0.0, 7.0, 3.85]]; chem2_2 setPos getPos chem2_2; 
*/