/* fn_randomMove.sqf
*  Author: PapaReap
*  Function name: pr_fnc_randomMove
*  Called by randomMoveInit.sqf
*  Trigger Usage: [crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]] remoteExec ["pr_fnc_randomMove", 0, false];
*
*  Ver 1.0 2016-05-25
*/

/* Note: below is random moving attachments, need to convert usage
//pt1 camp table (small)
laptop02 attachTo [pt1, [0.22, 0.2, 1.38]]; laptop02 setPos getPos laptop02; //laptop (open)
pt2 attachTo [pt1, [-0.25, 0.11, 1.3]]; pt2 setPos getPos pt2;		//fm radio
pt3 attachTo [pt1, [-0.29, 0.25, 1.235]]; pt3 setPos getPos pt3;		//file (photos)
//pt1_1 camp table (small)
pt4 attachTo [pt1_1, [0.1, 0.1, 1.41]]; pt4 setPos getPos pt4;		//satellite phone
pt5 attachTo [pt1_1, [-0.2, 0.1, -0.17]]; pt5 setPos getPos pt5;	//suitcase
pt6 attachTo [pt1_1, [-0.25, 0.0, 1.235]]; pt6 setPos getPos pt6;		//file (top secret)
pt7 attachTo [pt1_1, [0.15, -0.16, 1.235]]; pt7 setPos getPos pt7;		//file (documents)


//rad_1 camp table (small)
radio01 attachTo [rad_1, [-0.5, -0.18, 1.20]]; radio01 setPos getPos radio01; //satellite phone
laptop02_1 attachTo [rad_1, [-0.4, 0.40, 0.68]]; laptop02_1 setPos getPos laptop02_1; //laptop (open)

sleep 20;
//add to init.sqf:	[basephoto, [photo,photo_1,photo_2,photo_3,photo_4], [pt1,pt2,pt3,pt4,pt5,pt6,pt7,got_docs]] execVM "scripts\REAP\Random_Start.sqf";
[crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage]] execVM "scripts\REAP\Random_Start.sqf";
[crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage]] execVM "scripts\REAP\Random_Start.sqf";
Trigger Usage: [crash, [crash_1, crash_2, crash_3, crash_4], [hWreck, pilot1, pilot2, task1succeeded, task1Damage, "area2"]] remoteExec ["pr_fnc_randomMove", 0, false];
*/
//[[crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]], "pr_fnc_randomMove", true, false] call BIS_fnc_MP;
if ( !isServer ) exitWith {};

_reference = _this select 0;
_rPos = [0,0,0];
if (typeName _reference == "STRING") then {
    _rPos = getMarkerPos _reference;
} else { 
    if (typeName _reference == "ARRAY") then {
        _rPos = _reference;
    } else {
        _rPos = getPosATL _reference;
    };
};

_rX = _rPos select 0;
_rY = _rPos select 1;
_rZ = _rPos select 2;
sleep 1;

_movePos = _this select 1;
_mPos = [0,0,0];
_newPos = _movePos call BIS_fnc_selectRandom;

if (typeName _newPos == "STRING") then {
    _mPos = getMarkerPos _newPos;
} else { 
    if (typeName _newPos == "ARRAY") then {
        _mPos = _newPos;
    } else {
        _mPos = getPosATL _newPos;
    };
};

_mX = _mPos select 0;
_mY = _mPos select 1;
_mZ = (_mPos select 2) + 0.33; //player setpos [getPos target_6 select 0,getPos target_6 select 1,getPos (target_6 select 2) + 1]
//_mZ = _mPos select 2;

{
    if (typeName _x == "STRING") then {
        _objPos = getMarkerPos _x;
        _deltaX = (_objPos select 0) - _rX;
        _deltaY = (_objPos select 1) - _rY;
        _deltaZ = (_objPos select 2) - _rZ;
        _x setMarkerPos [_deltaX + _mX, _deltaY + _mY, _deltaZ + _mZ];
    } else {
        _objPos = getPosATL _x;
        _deltaX = (_objPos select 0) - _rX;
        _deltaY = (_objPos select 1) - _rY;
        _deltaZ = (_objPos select 2) - _rZ;
        _x setPosATL [_deltaX + _mX, _deltaY + _mY, _deltaZ + _mZ];
    };
} foreach (_this select 2);

//--- used for randomMoveInit.sqf to loop through array
randomMove = true;
