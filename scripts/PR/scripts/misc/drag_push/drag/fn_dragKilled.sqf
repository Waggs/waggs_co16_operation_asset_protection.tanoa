/* fn_dragKilled.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragKilled

*  Add killed eventhandler to drag unit
*/

_unit = _this select 0;
_unit addEventHandler ["killed", {
    _unit = _this select 0;
    //removeAllActions _unit;
    //_dragCond = "vehicle _this != vehicle _target && isNull attachedTo _target && count attachedObjects _this == 0 && _target distance _this < 2 && !(alive _target)";
    //_unit addaction [[_unit, "Drag"] call pr_fnc_dragActionText, { call pr_fnc_dragActionWhileDrag }, nil, 6, false, false, "", _dragCond];
    //sleep 0.5;
    [_unit] remoteExec ["pr_fnc_dragAddDrag", 2, false]; // possibly add above to remoteexec instead? will test this first
}];
