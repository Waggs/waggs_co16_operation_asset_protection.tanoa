/* fn_dragBody.sqf
*  Author: PapaReap

*  ver 1.0 - 2016-05-31 
*  ver 1.1 - 2016-08-19 separated into separate functions
 
*  Description: Drag uncnconscious or dead bodies out of sight to avoid detection. JIP/MP/SP/Dedicated compatible
*  Instructions: Add this line into the init.sqf. This will add a drag action to all editor placed units.
*  null = allUnits execVM "scripts\PR\scripts\misc\push\fn_dragBody.sqf";

*  Functions
*  0 = [this] spawn pr_fnc_dragAddDrag;
*  Add living units into the script. (Useful for units spawned mid-mission).

*  Credits to BangaBob (H8erMaker), Tajin, Norrin & Bohemia wiki for inspiration and guidance on developing this script
*/

waitUntil { !isNil "dragInitized" }; if !(dragInitized) exitWith {};

//--- server variables
if (isServer) then {
    if (isNil ("pr_dragArray")) then { pr_dragArray = []; };
    if (isNil ("pr_dragTotal")) then { pr_dragTotal = 0; };

    {
        pr_dragTotal = pr_dragTotal + 1;
        _x setVariable ["pr_dragID", pr_dragTotal, true];
        //pr_dragArray set [count pr_dragArray, _x];
    } forEach pr_dragArray;

    publicVariable "pr_dragTotal";
    publicVariable "pr_dragArray";
    serverDragInit = true; publicVariable "serverDragInit";
};

if (!isServer && (player != player)) then {
    waitUntil { player == player }; 
};

if ((!isDedicated) && (hasInterface)) then {
    _this spawn {
        waitUntil { !isNil "serverDragInit" };
        0 = pr_dragArray spawn pr_fnc_dragActionAddDrag;
    };
};
