/* fn_dragDisappear.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragDisappear

*  Temporary fix to hide dead after loading in vehicle
*/

_unit = _this select 0;
if (isServer) then { _unit hideObjectGlobal true; };
