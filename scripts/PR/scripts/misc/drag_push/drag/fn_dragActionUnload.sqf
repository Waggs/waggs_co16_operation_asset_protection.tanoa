/* fn_dragActionUnload.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragActionUnload

*  Add action to unload unit
*/

_vehicle = _this select 0;
_unit = _this select 1;
_vehicle setVariable ["prLoadedBody", _unit, false];
_unloadCond = "((_this distance _target < 6) && (vehicle _this == _this))";
_vehUnloadId = _vehicle addAction [
    [_unit, "Unload"] call pr_fnc_dragActionText,
    { call pr_fnc_dragRemoveBody },
    nil, 0, false, false, "", _unloadCond
];
_unit setVariable ["pr_vehUnloadId", _vehUnloadId];
