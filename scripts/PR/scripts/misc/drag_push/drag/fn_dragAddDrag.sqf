/* fn_dragAddDrag.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragAddDrag

*  Add drag to unit, trigger or script
*  Example: 0 = [joe,joe_1,joe_2,mark] spawn pr_fnc_dragAddDrag;
*/

{
    pr_dragTotal = pr_dragTotal + 1;
    _x setVariable ["pr_dragID", pr_dragTotal, true];
    pr_dragArray set [count pr_dragArray, _x];
} forEach _this;

_this remoteExec ["pr_fnc_dragActionAddDrag", 0, true];

publicVariable "pr_dragTotal";
publicVariable "pr_dragArray";
