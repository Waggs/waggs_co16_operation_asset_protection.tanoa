/*  pr_push.sqf
*  Author: PapaReap
*  Makes objects pushable
*  ver 1.0 2016-04-23
*
*  To remove object from pushable, add to object init: [this] call pr_fnc_noPush;
*/

if (isNil "pr_pushVars") then {
    pr_pushVars = true;
    pr_actionPushPriority = 30; //--- AddAction menu position
    _unit = player;
    _unit setVariable ["myFatigue", 0, false];
    if (isNil "pr_pushArray") then { pr_pushArray = []; };
};

waitUntil { time > 0 };

//--- pushable object arrays
//--- distance 3.6
if (aceOn) then {
    pr_pushObj1 = [];
} else {
    pr_pushObj1 = ["B_Boat_Transport_01_F","B_G_Boat_Transport_01_F","B_Lifeboat","O_Boat_Transport_01_F","O_G_Boat_Transport_01_F","O_Lifeboat","I_Boat_Transport_01_F","I_G_Boat_Transport_01_F","C_Rubberboat","B_SDV_01_F","C_Scooter_Transport_01_F"];
};
//--- distance 8
pr_pushObj2 = ["v","O_Boat_Armed_01_hmg_F","I_Boat_Armed_01_minigun_F","C_Boat_Civil_01_F","C_Boat_Civil_01_police_F","C_Boat_Civil_01_rescue_F"];
pr_pushable = pr_pushObj1 + pr_pushObj2;

//--- push function
pr_fnc_push = {
    private ["_object","_man"];
    _object = _this select 0;
    _man = _this select 1;
    if ((getStamina _man < 1.5) && (getFatigue _man > 0.9)) exitwith {
        _man spawn {
            _man = _this;
            titleText ["You're too tired, rest a moment","PLAIN", 3];
            _man setVariable ["myFatigue", 1, false];
            waitUntil { ((getStamina _man > 1.0) && (getFatigue _man < 0.9)) };
            _man setVariable ["myFatigue", 0, false];
            titleText ["","PLAIN DOWN", 0.1];
        };
    };

    _baggage = 0;
    _fAdd = 0;
    _sSub = 0;
    if ({_x in _object} count playableunits > 0) then {
        _baggage = {_x in _object} count playableunits;
        _fAdd = _fAdd + (_baggage * 0.02);
        _sSub = _sSub + (_baggage * 0.03);
    };

    if ((typeof _object) in pr_pushObj1) then {
        _object setVelocity [(sin(direction _man))*3.1, (cos(direction _man))*3.1, 0];
        _man setFatigue ((getFatigue _man) + 0.1 + _fAdd);
        _man setStamina ((getStamina _man) - 0.15 - _sSub);
    } else {
        if ((typeof _object) in pr_pushObj2) then {
            _object setVelocity [(sin(direction _man))*1.2, (cos(direction _man))*1.2, 0];
            _man setFatigue ((getFatigue _man) + 0.2 + _fAdd);
            _man setStamina ((getStamina _man) - 0.3 - _sSub);
        };
    };
};

//--- push addAction
pr_fnc_pushAction = {
    _veh = _this select 0;
    _veh_type = (typeOf _veh);
    //--- set conditions
    _pushCondition = "(cursorObject == _target) && !(vehicle _this == _target) && !((surfaceIsWater position _this) && (position _this select 2 < -1)) && (_this getVariable 'myFatigue' == 0)";
    if ((typeof _veh) in pr_pushObj1) then {
        _pushCondition = _pushCondition + " && (_this distance _target < 3.6)";
    } else {
        if ((typeof _veh) in pr_pushObj2) then {
            _pushCondition = _pushCondition + " && (_this distance _target < 8) && (position _target select 2 < -0.45)";
        };
    };
    //--- addAction
    if (_veh_type in pr_pushable) then {
        _veh addAction [
            "<img image='scripts\PR\scripts\misc\drag_push\dd.jpg' /><t color=""#fff200"">  Push</t>",
            { [[_this select 0, _this select 1], "pr_fnc_push", true, false] call BIS_fnc_MP; },
            [],pr_actionPushPriority,
            false,
            true,
            "",
            _pushCondition
        ];
    };
};

//--- a little sleep for good measure
sleep 1;
pr_pushArray = pr_pushArray - pr_noPushArray;

//if (isNil "pr_pushArray") then { pr_pushArray = []; };
//--- add existing objects to pushable array
{
    if (((typeOf _x) in (pr_pushable)) && !(_x in pr_noPushArray)) then {
        pr_pushArray = pr_pushArray + [_x];

        [_x] call pr_fnc_pushAction;
    };
} forEach (vehicles);

if !(isServer) exitWith {};

//--- server removes unused objects, then adds new objects to pushable array
while { true } do {
    pr_pushArray = pr_pushArray - pr_noPushArray;
    sleep 60;
    {
        if !(_x in vehicles) then {
            pr_pushArray = pr_pushArray - [_x];
        };
    } forEach (pr_pushArray);

    {
        if (((typeOf _x) in (pr_pushable)) && !(_x in pr_pushArray) && !(_x in pr_noPushArray)) then {
            pr_pushArray = pr_pushArray + [_x];

            [[_x],"pr_fnc_pushAction",true,false] call BIS_fnc_MP;
        };
    } forEach (vehicles);
    //--- keep clients updated, JIP and such
    publicVariable "pr_pushArray";
};
