//0 = [thisTrigger, west, 5000, true] execVM "scripts\PR\scripts\misc\unitDamage.sqf"; 

// this sets damage to false for side west within 500m from trigger: 
// 0 = [thisTrigger, west, 500, false] execVM "scripts\PR\scripts\misc\unitDamage.sqf"; 

_obj = _this select 0;
_side = _this select 1;
_distance = _this select 2;
_damage = _this select 3;

_debug = false;
if (count _this > 4) then { _debug = _this select 4; };

{
    if (((side _x) == _side) && (_x distance _obj < _distance)) then {
        _x allowDamage _damage;
    };
//} count allUnits;
    if (_debug) then {
        if (_damage) then { hint "allow damage is on"; } else { hint "allow damage is off"; };
    };
} forEach (allUnits);


//hint "damage is on";
