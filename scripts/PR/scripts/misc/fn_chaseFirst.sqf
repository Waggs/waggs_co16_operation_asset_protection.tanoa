/*
*  Author: PapaReap
*  function name: pr_fnc_chaseFirst
*  ver 1.0 2016-08-17

*  examples:
*  if (isServer) then { whoToChase = [thislist, true] call pr_fnc_chaseFirst; publicVariable "whoToChase"; };
*  if (isServer) then { whoToChase = [playableUnits, true] call pr_fnc_chaseFirst; publicVariable "whoToChase"; };
   
   nul = ["opforReinfor_heli_1",true,2,3,false,true,whoToChase,180,5000,true,true,8,"default",[false,false,true,true],nil,nil,false] execVM "scripts\LV\reinforcementChopper.sqf";
*/

_inThisList = _this select 0;
_random = false;
if (count _this > 1) then { _random = _this select 1; };

_unitToChase = [];
_yourIt = objNull;

{ if ((isPlayer _x) && (_x in _inThisList)) then { _unitToChase = _unitToChase + [_x]; }; } forEach playableUnits;

if (_random) then { _yourIt = _unitToChase call BIS_fnc_selectRandom; } else { _yourIt = _unitToChase select 0; };

_yourIt
