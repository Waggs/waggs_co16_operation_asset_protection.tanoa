/* fn_preInit.sqf
*  Author: PapaReap
*  Call the function upon mission start, before objects are initialized. Passed arguments are ["preInit"]
*  ver 1.0 2016-01-31
*/

[] execVM "scripts\PR\scripts\init\areaMarkers.sqf";  //--- PR Markers, turns markers starting with area#'s off

if (isNil "game_a3") then { fnc_gameCheck = call compile preprocessFileLineNumbers "scripts\PR\scripts\tools\fn_gamecheck.sqf"; };  //---

//--- "scripts\PR\
prFolder = "scripts\PR\";

//--- "scripts\PR\scripts\
prSCRf = (prFolder + "scripts\");



//--- "scripts\PR\scripts\ai\clothing\
prCLOf = (prAIf + "clothing\");
pr_fnc_r_Sniper = compile preprocessFileLineNumbers (prCLOf + "fn_r_Sniper.sqf");  //---
pr_fnc_r_Spotter = compile preprocessFileLineNumbers (prCLOf + "fn_r_Spotter.sqf");  //---
pr_fnc_r_GuerClothing = compile preprocessFileLineNumbers (prCLOf + "fn_r_guerClothing.sqf");  //--- Opfor Guerilla Clothing
pr_fnc_r_TalibanClothing = compile preprocessFileLineNumbers (prCLOf + "fn_r_TalibanClothing.sqf");  //--- Opfor Taliban Clothing
pr_fnc_r_urbanClothing = compile preprocessFileLineNumbers (prCLOf + "fn_r_urbanClothing.sqf");  //--- Opfor Urban Clothing
pr_fnc_r_changeUniform = compile preprocessFileLineNumbers (prCLOf + "fn_r_changeUniform.sqf");  //--- Script to auto change Guerilla, Taliban or Urban Clothing

//--- "scripts\PR\scripts\debug\
prDGf = (prSCRf + "debug\");
pr_fnc_debugText = compile preprocessFileLineNumbers (prDGf + "fn_debugText.sqf");  //---
pr_fnc_typecheck = compile preprocessFileLineNumbers (prDGf + "fn_typecheck.sqf");  //---

//--- "scripts\PR\scripts\functions\
prFUNf = (prSCRf + "functions\");
pr_fnc_delObject = compile preprocessFileLineNumbers (prFUNf + "fn_delObject.sqf");  //---
pr_fnc_moveObject = compile preprocessFileLineNumbers (prFUNf + "fn_moveObject.sqf");  //---
pr_fnc_attachPart = compile preprocessFileLineNumbers (prFUNf + "fn_attachPart.sqf");  //---

//--- "scripts\PR\scripts\getPos\
prGPf = (prSCRf + "getPos\");
pr_fnc_getBPos = compile preprocessFileLineNumbers (prGPf + "fn_getBPos.sqf");  //---
pr_fnc_getBuildingtop = compile preprocessFileLineNumbers (prGPf + "fn_getBuildingtop.sqf");  //---
pr_fnc_getEPos = compile preprocessFileLineNumbers (prGPf + "fn_getEPos.sqf");  //---
pr_fnc_getPos = compile preprocessFileLineNumbers (prGPf + "fn_getPos.sqf");  //---
pr_fnc_getPosInArea = compile preprocessFileLineNumbers (prGPf + "fn_getPosInArea.sqf");  //---
pr_fnc_findRoofTop = compile preprocessFileLineNumbers (prGPf + "fn_getRooftop.sqf");  //--- Find building rooftops
pr_fnc_nearestLandPos = compile preprocessFileLineNumbers (prGPf + "fn_nearestLandPos.sqf");  //---
pr_fnc_nearestRoadPos = compile preprocessFileLineNumbers (prGPf + "fn_nearestRoadPos.sqf");  //---
pr_fnc_probeSurface = compile preprocessFileLineNumbers (prGPf + "fn_probeSurface.sqf");  //---

//--- "scripts\PR\scripts\init\
prINITf = (prSCRf + "init\");
pr_fnc_aiSpawner = compile preprocessFileLineNumbers (prINITf + "fn_aiSpawner.sqf");  //---
pr_fnc_hcCheck = compile preprocessFileLineNumbers (prINITf + "fn_hcCheck.sqf");  //--- Headless Client check
pr_fnc_hcTracker = compile preprocessFileLineNumbers (prINITf + "fn_hcTracker.sqf");  //---

//--- "scripts\PR\scripts\misc\
prMISCf = (prSCRf + "misc\");
pr_fnc_randomMove = compile preprocessFileLineNumbers (prMISCf + "fn_randomMove.sqf");  //---
pr_fnc_haloToPos = compile preprocessFileLineNumbers (prMISCf + "fn_haloToPos.sqf");  //---
prDRAGf = (prMISCf + "drag_push\");
pr_fnc_noPush = compile preprocessFileLineNumbers (prDRAGf + "fn_noPush.sqf");  //--- Removes objects from pushable array

//--- "scripts\PR\scripts\tasks\
prTASKf = (prSCRf + "tasks\");
fnc_tasker = compile preprocessFileLineNumbers (prTASKf + "fn_tasker.sqf");

//--- "scripts\PR\scripts\tools\
prTLf = (prSCRf + "tools\");
pr_fnc_collectBuildings = compile preprocessFileLineNumbers (prTLf + "fn_collectBuildings.sqf");  //---
pr_fnc_collectMarkers = compile preprocessFileLineNumbers (prTLf + "fn_collectMarkers.sqf");  //---
pr_fnc_collectObjectsNum = compile preprocessFileLineNumbers (prTLf + "fn_collectObjectsNum.sqf");  //---
pr_fnc_isWallInDir = compile preprocessFileLineNumbers (prTLf + "fn_isWallInDir.sqf");  //---
pr_fnc_selectRandom = compile preprocessFileLineNumbers (prTLf + "fn_selectRandom.sqf");  //---
pr_fnc_getTurret = compile preprocessFileLineNumbers (prTLf + "fn_getTurret.sqf");  //--- Replaces CBA dependancy, used to find out which config turret is turretpath.
pr_fnc_isTurnedOut = compile preprocessFileLineNumbers (prTLf + "fn_isTurnedOut.sqf");  //--- Replaces CBA dependancy, checks whether a unit is turned out in a vehicle or not.

//--- "scripts\PR\scripts\vehicle\
prVEHf = (prSCRf + "vehicle\");
pr_fnc_vehicleSpawn = compile preprocessFileLineNumbers (prVEHf + "fn_vehicleSpawn.sqf");  //--- Spawns land vehicles, optional waypoints, various ablilities...
pr_fnc_carWaypoint = compile preprocessFileLineNumbers (prVEHf + "fn_carWaypoint.sqf");  //--- Spawns land vehicles, sets waypoints, various ablilities...


/*  MAIN PR FOLDER */

//--- "scripts\PR\scripts\ai\
prAIf = (prFolder + "ai\");
pr_fnc_artyAmbience = compile preprocessFileLineNumbers (prAIf  + "fn_artyAmbience.sqf");  //---
pr_fnc_artySpotter = compile preprocessFileLineNumbers (prAIf + "fn_artySpotter.sqf");  //---
pr_fnc_staticSpawn = compile preprocessFileLineNumbers (prAIf + "fn_staticSpawn.sqf");  //---
pr_fnc_addWaypoint = compile preprocessFileLineNumbers (prAIf + "fn_addWaypoint.sqf");  //---
pr_fnc_deleteWaypoint = compile preprocessFileLineNumbers (prAIf + "fn_deleteWaypoint.sqf");  //---
pr_fnc_enterBuilding = compile preprocessFileLineNumbers (prAIf + "fn_enterBuilding.sqf");  //---
pr_fnc_setInsidePos = compile preprocessFileLineNumbers (prAIf + "fn_setInsidePos.sqf");  //---
pr_fnc_taskCrew = compile preprocessFileLineNumbers (prAIf + "fn_taskCrew.sqf");  //---
pr_fnc_taskDefend = compile preprocessFileLineNumbers (prAIf + "fn_taskDefend.sqf");  //---
pr_fnc_unitSpawn = compile preprocessFileLineNumbers (prAIf + "fn_unitSpawn.sqf");  //---

//--- "scripts\PR\intel_destruction\
prINDEf = (prFolder + "intel_destruction\");
prNUKEf = (prINDEf + "nuke\");
pr_fnc_nukeInit = compile preProcessFileLineNumbers (prNUKEf + "fn_nuke_init.sqf");  //--- Init settings for nuclear explosion

//--- "scripts\PR\healthCare\
prHCf = (prFolder + "healthCare\");
prHCMFf = (prHCf + "medFunctions\"); // "scripts\PR\healthCare\medfunctions\
pr_fnc_localHealthCare = compile preprocessFileLineNumbers (prHCMFf + "fn_localHealthCare.sqf");  //--- Client healthcare
pr_fnc_randomBodyDamage = compile preprocessFileLineNumbers (prHCMFf + "fn_randomBodyDamage.sqf");  //--- Sets random body damage and pain
prHCMf = (prHCf + "mash\"); // "scripts\PR\HealthCare\mash\
pr_fnc_fmInit = compile preprocessFileLineNumbers (prHCMf + "fn_fmInit.sqf");  //--- Field mash init
pr_fnc_fmBuild = compile preprocessFileLineNumbers (prHCMf + "fn_fmBuild.sqf");  //--- Builds a field mash station
pr_fnc_fmDelete = compile preprocessFileLineNumbers (prHCMf + "fn_fmDelete.sqf");  //--- Removes field mash station
prHCMMf = (prHCMf + "mashMan\"); // "scripts\PR\HealthCare\mash\mashMan\
pr_fnc_mashTentInit = compile preprocessFileLineNumbers (prHCMMf + "fn_mashTentInit.sqf");  //--- Medic tent init
pr_fnc_mashTentCreate = compile preprocessFileLineNumbers (prHCMMf + "fn_mashTentCreate.sqf");  //--- Creates a mash tent
pr_fnc_mashTentDelete = compile preprocessFileLineNumbers (prHCMMf + "fn_mashTentDelete.sqf");  //--- Removes mash tent
prMCf = (prHCMf + "mashCommon\"); // "scripts\PR\healthCare\mash\mashCommon\
pr_fnc_mashTreatment = compile preprocessFileLineNumbers (prMCf + "fn_mashTreatment.sqf");  //--- Restores blood to player at given distance
pr_fnc_mashTreatmentClient = compile preprocessFileLineNumbers (prMCf + "fn_mashTreatmentClient.sqf");  //--- Gives player hint while blood is given
pr_fnc_bloodCounter = compile preprocessFileLineNumbers (prMCf + "fn_bloodCounter.sqf");  //--- Counts and tracks units of blood used
pr_fnc_crateBloodInv = compile preprocessFileLineNumbers (prMCf + "fn_crateBloodInv.sqf");  //--- Removes blood bag in container
pr_fnc_bloodCargo = compile preprocessFileLineNumbers (prMCf + "fn_bloodCargo.sqf");  //--- Sets variable if container has blood in it
pr_fnc_medHolder = compile preprocessFileLineNumbers (prMCf + "fn_medHolder.sqf");  //--- Places a weapon holder for Mashman Tent
pr_fnc_restoreSpeech = compile preprocessFileLineNumbers (prMCf + "fn_restoreSpeech.sqf");  //--- Restores player speech on Ace Unconsciousness

//--- "scripts\PR\commander\
prCOMf = (prFolder + "commander\");
prASDf = (prCOMf + "aiSupplyDrop\"); // "scripts\PR\commander\aiSupplyDrop\
pr_fnc_supplyDropCall = compile preprocessFileLineNumbers (prASDf + "fn_callSupplyDrop.sqf");  //---
pr_fnc_findBoxAir = compile preprocessFileLineNumbers (prASDf + "fn_findBoxAir.sqf");  //---
pr_fnc_findBoxLand = compile preprocessFileLineNumbers (prASDf + "fn_findBoxLand.sqf");  //---
pr_fnc_supplyDrop = compile preprocessFileLineNumbers (prASDf + "fn_supplyDrop.sqf");  //---
prHTf = (prCOMf + "heli_Transport\"); // "scripts\PR\commander\heli_Transport\
pr_fnc_heliTransportCall = compile preprocessFileLineNumbers (prHTf + "fn_heliTransportCall.sqf");  //---
pr_fnc_heliTransportNewWPCall = compile preprocessFileLineNumbers (prHTf + "fn_heliTransportNewWPCall.sqf");  //---
prCOMFf = (prCOMf + "functions\"); // "scripts\PR\commander\functions\

//--- "scripts\PR\scripts\weather\
prWEAf = (prFolder + "weather\");

diag_log format ["*PR* fn_preInit complete"];
