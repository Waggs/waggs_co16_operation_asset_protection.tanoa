

[
    ["Supplies_spawn", c_wp, "c130_land", "c130_end", 80, 300],  // vehicle spawn, 1st wp (can have copies for more waypoints, will create wp's from original), (land, drop or patrol), end wp, height, speed
    ["RHS_C130J", "west"], // vehicle type, side (default east)
    [false, true], // set captive (default: false), allow damage (default: true)
    [true, "Box_NATO_WpsLaunch_F",[{ [newCrate] call fnc_natoBoxAmmo; },'BIS_fnc_spawn', true, false] call BIS_fnc_MP], // allow crate (default: true), crate type (default: "Box_NATO_Ammo_F"), crate init (optional)
    "drop", // Drop cargo, Land or "" to pass through (default: "")
    "", // Land, Delete or "" to pass through (default: "")
    ["patrol", 900]  // last waypoint statement mode & radius (default: "")
] call fnc_airSpawn; 

    /*
    _vehName = "john";
	_initinit = format["[
		{
		    (objectFromNetId '%1') setVehicleVarName '%2'; 
		}
	]", (netId _veh), _vehName]; 

	[call compile format ["%1", _initinit], "BIS_fnc_spawn", true, false] spawn BIS_fnc_MP; 
	_veh call compile format [" %1 = _this; publicvariable '%1' ", _vehName];
	*/