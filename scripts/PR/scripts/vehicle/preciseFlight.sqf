// usage _nil = [chinook4,"c4_drop",["c4wp","c4wp_1","c4wp_2","c4wp_3"],"c4_end"] execVM "scripts\PR\scripts\vehicle\preciseHeli.sqf"; 
// future work/// _nil = [chinook3,"c3_drop",["c3wp",["c3wp_1",["true", "this flyInHeight 25;"]],"c3wp_2","c3wp_3"],"c3_end"] execVM "scripts\PR\scripts\vehicle\preciseHeli.sqf"; 

if (!isServer) exitWith {}; 

waitUntil { !(isNil "objectVarsCompiled") };

_heli  = _this select 0; 
_heli allowDamage false; 
_driver = driver _heli; 

_grp = createGroup WEST; 
[_driver] joinSilent _grp; 
_driver setBehaviour "careless"; 

_mrkDrop = _this select 1; 

_mrkWP0 = (_this select 2) select 0; 
_mrkWP1 = (_this select 2) select 1; 

_mrkWPEnd = _this select 3; 

_pad = "Land_HelipadEmpty_F" createVehicle getMarkerPos _mrkDrop; 
sleep 0.5; 

if (surfaceIsWater (getPos _pad)) then { 
    _pad setPosASL [position _pad select 0, position _pad select 1, 0]; 
} else { 
    _pad setPosATL [position _pad select 0, position _pad select 1, -2]; 
}; 

_pos0 = getMarkerPos _mrkWP0; 
_wp0 = _grp addWaypoint [_pos0, 0]; 
//_wp0 setWaypointStatements ["true", "0 = [(vehicle this), [50, 30]] call fnc_setVelocity;"]; 

_pos1 = getMarkerPos _mrkWP1; 
_wp1 = _grp addWaypoint [_pos1, 1]; 

if ((count (_this select 2)) > 3) then { 
    _mrkWP2 = (_this select 2) select 2; 
    _pos2 = getMarkerPos _mrkWP2; 
    _wp2 = _grp addWaypoint [_pos2, 2]; 
}; 

if ((count (_this select 2)) > 3) then { 
    _mrkWP3 = (_this select 2) select 3; 
    _pos3 = getMarkerPos _mrkWP3; 
    _wp3 = _grp addWaypoint [_pos3, 3]; 
}; 

if ((count (_this select 2)) > 4) then { 
    _mrkWP4 = (_this select 2) select 4; 
    _pos4 = getMarkerPos _mrkWP4; 
    _wp4 = _grp addWaypoint [_pos4, 4]; 
}; 

if ((count (_this select 2)) > 5) then { 
    _mrkWP5 = (_this select 2) select 5; 
    _pos5 = getMarkerPos _mrkWP5; 
    _wp5 = _grp addWaypoint [_pos5, 5]; 
}; 

if ((count (_this select 2)) > 6) then { 
    _mrkWP6 = (_this select 2) select 6; 
    _pos6 = getMarkerPos _mrkWP6; 
    _wp6 = _grp addWaypoint [_pos6, 6]; 
}; 

if ((count (_this select 2)) > 7) then { 
    _mrkWP7 = (_this select 2) select 7; 
    _pos7 = getMarkerPos _mrkWP7; 
    _wp7 = _grp addWaypoint [_pos7, 7]; 
}; 

if ((count (_this select 2)) > 8) then { 
    _mrkWP8 = (_this select 2) select 8; 
    _pos8 = getMarkerPos _mrkWP8; 
    _wp8 = _grp addWaypoint [_pos8, 8]; 
}; 

if ((count (_this select 2)) > 9) then { 
    _mrkWP9 = (_this select 2) select 9; 
    _pos9 = getMarkerPos _mrkWP9; 
    _wp9 = _grp addWaypoint [_pos9, 9]; 
}; 

if ((count (_this select 2)) > 10) then { 
    _mrkWP10 = (_this select 2) select 10; 
    _pos10 = getMarkerPos _mrkWP10; 
    _wp10 = _grp addWaypoint [_pos10, 10]; 
}; 

waitUntil { _heli distance _pad < 110 }; 
private ["_asloc","_fastropePos","_lockobj","_lock","_xfall","_xvelx","_xvely","_xvelz","_delay","_height","_lz0","_lz2","_px","_py","_spdmode"]; 
_heli SetVariable ["fastRopeHeight", 5]; 
_heli SetVariable ["fastRopePos", getPos _pad]; 

_fastropePos = getPos _pad; 
_height = _heli getVariable ["fastRopeHeight", 5]; 
if (_height < 5) then {_height = 5}; 
if (_height > 20) then {_height = 20}; 
_heli setVariable ["fastRopePos", _fastropePos, true]; 
_heli setVariable ["fastRopeHeight", (_fastropePos select 2) + _height, true]; 
_heli flyInHeight 20; 
_heli doMove _fastropePos; 
sleep 0.5; 
_px = (_fastropePos select 0) + (100 * sin(direction _heli)); 
_py = (_fastropePos select 1) + (100 * cos(direction _heli)); 
_lz2 = [_px, _py, (_fastropePos select 2) + _height]; 
_heli doMove _lz2; 
_asloc = [_fastropePos select 0, _fastropePos select 1, (_fastropePos select 2) + _height]; 
_lockobj = "Land_HelipadEmpty_F" createVehiclelocal _asloc; 

if (surfaceIsWater (getPos _lockobj)) then { 
    _lock = getPosASL _lockobj select 2; deleteVehicle _lockobj; 
} else { 
    _lock = getPosATL _lockobj select 2; deleteVehicle _lockobj; 
}; 

_lz0 = [_fastropePos select 0, _fastropePos select 1, 0]; 
while {(alive _heli) and ([getPos _heli select 0, getPos _heli select 1, 0] distance _lz0) > 100} do {sleep 0.5}; 
_delay = 0.75; 
_fastropePos = _asloc; 
_heli disableAi "move"; 
_heli setSpeedMode "limited"; 
_c = 0; 

while {(!isNull _heli) && ((getPos _heli) distance _asloc > 5) && (alive _heli) && (_c == 0)} do { 
    if (surfaceIsWater (getPos _pad)) then { 
        _xfall = sqrt (((getPosASL _heli select 2) - _lock) / 0.95); 
    } else { 
        _xfall = sqrt (((getPosATL _heli select 2) - _lock) / 0.95); 
    }; 
    _xvelx = (((_fastropePos select 0) - (getPos _heli select 0)) / _xfall) / _delay; 
    _xvely = (((_fastropePos select 1) - (getPos _heli select 1)) / _xfall) / _delay; 
    _xvelz = (((_fastropePos select 2) - (getPos _heli select 2)) / _xfall) / (_delay * 1.5); 
    _heli setVelocity [_xvelx, _xvely, _xvelz]; 
    if (_heli distance _pad < 15) then { 
        _c = 1; 
    }; 
    sleep 0.05; 
}; 

waitUntil { _heli distance _pad < 100 }; 
_smoke = "SmokeShell" createVehicle [getPos _pad select 0, getPos _pad select 1, 10]; 
_smoke1 = chemY createVehicle [(getPos _pad select 0) -3, getPos _pad select 1, 5]; 

waitUntil { _heli distance _pad < 15 }; 

if (count ropes _heli == 2) then { 
    ropeCut [ropes _heli select 0, 5]; 
    ropeCut [ropes _heli select 1, 5]; 
}; 
if (count ropes _heli == 4) then { 
    ropeCut [ropes _heli select 0, 5]; 
    ropeCut [ropes _heli select 1, 5]; 
    ropeCut [ropes _heli select 2, 5]; 
    ropeCut [ropes _heli select 3, 5]; 
}; 
if (count ropes _heli == 6) then { 
    ropeCut [ropes _heli select 0, 5]; 
    ropeCut [ropes _heli select 1, 5]; 
    ropeCut [ropes _heli select 2, 5]; 
    ropeCut [ropes _heli select 3, 5]; 
    ropeCut [ropes _heli select 4, 5]; 
    ropeCut [ropes _heli select 5, 5]; 
}; 
sleep 2; 

//deleteVehicle _pad; 
_heli enableAi "move"; 
_pos4 = getMarkerPos _mrkWPEnd; 
_wp4 = _grp addWaypoint [_pos4, 4]; 
_heli setSpeedMode "normal"; 
waitUntil {_heli distance _pos4 < 100}; 
{deleteVehicle _x;} forEach crew _heli; deleteVehicle _heli; 
