//nul = [this,Reload Time in seconds] execVM "scripts\PR\scripts\vehicle\unlimitedAmmo.sqf";

_unit = _this select 0; 
_ReloadTime = _this select 1; 

while {alive _unit} do { 
    _unit setVehicleAmmo 1; 
    sleep _ReloadTime; 
}; 
