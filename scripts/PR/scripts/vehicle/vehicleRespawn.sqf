// [EVO] Dan's vehicle respawn script
// Its very basic, but that should hopefully improve performance
// Takes into account changes that have been made to the scripting library
// Will run only on the server, as it only needs to run in one place, but obviously can be used in both singleplayer and multiplayer
/*
Usage:
To use this put the following code into the init of your vehicle:
0 = [this,30,"optional init"] execVM "scripts\PR\scripts\vehicle\vehicleRespawn.sqf";
0 = [this, 30, "_nil = _this execVM 'scripts\PR\loadouts\box\natoBox_ammo.sqf'; "] execVM "scripts\PR\scripts\vehicle\vehicleRespawn.sqf";
You can change the value 5 to whatever you desire as your respawn time.
Inside of the code you can change the various sleep values so as to change the looping time (for performance) and the vehicle deletion delay. Both of these commands have notes next to them telling you what they do.
*/
private ["_nil","_vehicle","_respawntime","_init","_facingofvehicle","_positionofvehicle","_vehicletype","_n"];

Init = {(_this select 0) call compile (_this select 1)};

if (!isServer) exitWith {};

_nil = _this spawn {
    _vehicle = _this select 0;
    _respawntime = _this select 1;
    _init = "";
    if ((count _this) > 2) then {
        _init = _this select 2;
        if (_init != "") then {
            [[_vehicle, _init], "Init", true, false, false] spawn BIS_fnc_MP;
        };
    };
    _facingofvehicle = getDir _vehicle;
    _positionofvehicle = getPosATL _vehicle;
    _vehicletype = typeOf _vehicle;
    _n = 1;

    while {_n == 1} do {
        //if((!alive _vehicle) || (!canMove _vehicle)) then { //true if vehicle is not alive or it cannot move // alternate: if(!alive _vehicle) then {
        if (!alive _vehicle) then { //true if vehicle is not alive or it cannot move // alternate: if(!alive _vehicle) then {
            sleep 10; //change this to what you like, longer will give you a bigger response delay between unit being killed and the vehicle being deleted. //default sleep 240;
            deleteVehicle _vehicle;
            sleep _respawntime;
            _vehicle = _vehicletype createVehicle _positionofvehicle;
            _vehicle setPosATL _positionofvehicle;
            _vehicle setDir _facingofvehicle;
            [[[_vehicle, _respawntime, _init], "scripts\PR\scripts\vehicle\vehicleRespawn.sqf"], "BIS_fnc_execVM", false, false, false] spawn BIS_fnc_MP;
            _n = 0;
        };
        sleep 10;
    };
};
