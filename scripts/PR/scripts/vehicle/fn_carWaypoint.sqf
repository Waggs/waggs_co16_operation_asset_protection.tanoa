/*
*  Author: PapaReap
*  Function name: fnc_carSpawn
*  vehicls waypoints
*  ver 1.0 2015-12-14
*
*  Arguments: 
*  0: <SPAWN/WAYPOINT - ARRAY>                                --- required
*     0: <SPAWN POSITION>                                     --- Marker or object, sets spawn position.
*     1: <FIRST WAYPOINT>                                     --- Marker or object, sets first waypoint and vehicle direction, copied of original will create additional waypoints inside.
*     2: <END WAYPOINT>                                       --- Marker or object, sets end waypoint.
*     3; <SPEED OVERRIDE> default - 120                       --- optional, limits running speed of vehicle, e.g. - 25 or 55 etc...

*  1: <VEHICLE TYPE/SIDE - ARRAY>
*     0: <VEHICLE TYPE>   default - "O_Truck_02_transport_F"  --- if using vehicle array, precede this script with example: [r_carTransport] call r_randomSpawn; use as unit; (randomUnit select 0)
*     1: <SIDE>           default - "EAST"                    --- options, use string "east", "west", "resistance", "civilian"

*  2: <VEHICLE STATUS>
*     0: <SET CAPTIVE>    default - false                     --- options, use boolean - true or false
*     1: <ALLOW DAMAGE>   default - true                      --- options, use boolean - true or false

*  3: <START WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY>
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH"
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED"
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL"

*  4: <START WAYPOINT STATEMENT> default - ""                 --- action to execute at waypoint completion, e.g. - "car_1 = this" or "hint 'waypoint completed' "

*  5: <END WAYPOINT TYPE/RADIUS - ARRAY>
*     0: <WAYPOINT TYPE>  default - ""                        --- options, use string - "MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", "UNLOAD", "TR UNLOAD",
*                                                                                      "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", "GETIN NEAREST", "DISMISS" and "LOITER"
*     1: <RADIUS>         default - 0                         --- optional, set radius, e.g. - 230, 300 etc...

*  6: <END WAYPOINT BEHAVIOR/COMBAT MODE/SPEED - ARRAY>
*     0: <BEHAVIOR>       default - "AWARE"                   --- options, use string - "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH"
*     1: <COMBAT MODE>    default - "YELLOW"                  --- options, use string - "BLUE", "GREEN", "WHITE", "YELLOW" and "RED"
*     2: <WP SPEED>       default - "NORMAL"                  --- options, use string - "UNCHANGED", "LIMITED", "NORMAL" and "FULL"

*  7: <END WAYPOINT STATEMENT> default - ""                   --- action to execute at waypoint completion, e.g. - "car_1 = vehicle this" or "hint 'waypoint completed' "

* Examples:
[r_carTransport] call r_randomSpawn;
[["car_spawn", "carWp", "carWpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","YELLOW","LIMITED"], "car_1 = vehicle this",["tr unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call fnc_carSpawn;

[["car1_spawn", "car1wp", "car1wpEnd", 40], [(randomUnit select 0), "EAST"], [false, true], ["CARELESS","white","full"], "car_1 = vehicle this",["unload"], ["AWARE","RED","LIMITED"], "hint 'waypoint completed' "] call fnc_carSpawn;

[["c130Spawn", "c130WP", "c130End", 400, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; ",[""], ["CARELESS","white","full"], ""] call fnc_carSpawn;

cond:  !(isNil "c130_1")
onAct: [c130_1] call fnc_vehEjectAction; p1 moveInCargo c130_1;

[["c130Spawn", "c130WP", "c130End", 350, 5000], ["RHS_C130J", "West"], [false, true], ["CARELESS","white","full"], "c130_1 = vehicle this; if !(isNil 'p1') then {p1 moveInCargo c130_1}; if !(isNil 'p3') then {p3 moveInCargo c130_1}; if !(isNil 'leadmedic') then {leadmedic moveInCargo c130_1}; 0 = [c130_1] spawn fnc_vehAction; ",[""], ["CARELESS","white","full"], "hint 'waypoint completed' "] call fnc_carSpawn;
*/

/* fn_airSpawn.sqf
usage:
[[spawnObject], vehType, height, special init,[optional upsmon parameters; ""]] call fnc_carSpawn;
[[                   0,                                ], [    1   ], [    2      ], [   3  ], [    4    ]] call fnc_carSpawn;

[Vehicle spawn position, 1st waypoint (sets direction), end waypoint, speed] // _this select 0
[Vehicle, side] // _this select 1
[setcaptive, allowdamage] // _this select 2 bool

// starting waypoints
["AWARE","YELLOW","NORMAL] // _this select 3 start waypoint; array defining waypoint behaviour, combatmode and speed
[""] // _this select 4 waypoint statement

// end waypoint
[["patrol", 900]] // _this select 5 mode, modifier
["AWARE","YELLOW","NORMAL] // _this select 6 end waypoint; array defining waypoint behaviour, combatmode and speed
[""] // _this select 7 waypoint statement
*/

//if (!isServer) exitWith {};
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_carWaypoint start"]; };

private ["_wayArray","_spawn","_wp","_end","_height","_speed","_spawnPos","_wpPos","_limitSpeed"];
_height = 0;
_spawnPos = [0,0,0];
_wpPos = [0,0,0];
_limitSpeed = false;

//[[c130_1, "c130WP", "c130End", 400, 5000], [false, true], ["CARELESS","white","full"], "", [""], ["CARELESS","white","full"], ""] call pr_fnc_carWaypoint;
//[[c130_1, "c130WP", "c130End", 400, 2200], [false, true], ["MOVE",0,0], ["CARELESS","white","normal"], "", [], ["CARELESS","white","normal"], "{ deleteVehicle _x } forEach (crew c130_1) + [c130_1]; { deleteVehicle _x } forEach chemA2;"] call pr_fnc_carWaypoint;

_veh = _this select 0; //pr_veh = _veh;

_wayArray = [_this, 1, [], [[]]] call BIS_fnc_param;  // _this select 0
_wp = [_wayArray, 0, [0, 0, 0]] call BIS_fnc_paramIn;
_end = [_wayArray, 1, [0, 0, 0]] call BIS_fnc_paramIn;
if (count _wayArray > 2) then { _speed = _wayArray select 2; _limitSpeed = true; };
if (count _wayArray > 3) then { _height = _wayArray select 3; };

//--- SetCaptive, Allowdamage
private ["_vehStatus","_captive","_damage"];
_vehStatus = [_this, 2, [], [[]]] call BIS_fnc_param;  // _this select 2
_captive = [_vehStatus, 0, false, [false]] call BIS_fnc_paramIn;
_damage = [_vehStatus, 1, true, [true]] call BIS_fnc_paramIn;

_groupVeh = group _veh; //z_groupVeh = _groupVeh;
{ noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh;

//speed
if (_limitSpeed) then { _veh limitSpeed _speed; };
_groupVeh allowFleeing 0;
_veh allowDamage _damage;
_veh setCaptive _captive;

//--- Waypoint: 1 / array
private ["_waypoints","_loop","_wpL","_wpLPos"];
_waypoints = [];

if (typeName _wp == "STRING") then { _waypoints = _wp call pr_fnc_collectMarkers; } else { _waypoints = _wp call pr_fnc_collectObjectsNum; };
_waypoints spawn { sleep 0.01; };

if (typeName _end == "ARRAY") then {
    _waypoints = _waypoints + [_end]; //z_waypoints = _waypoints;
};

// start waypoint behaviour
//[[car1, "carWP", "carEnd", 40, 0], [false, true], ["getout",0,0], ["SAFE","white","normal","column"], "hint 'we are here'", [], ["CARELESS","white","normal"], "[group this, 'area10',['ambush']] call pr_fnc_upsSpawner; "] call pr_fnc_carWaypoint;
_statementStart = [_this, 3, [], [[]]] call BIS_fnc_param;  // _this select 2
//_sMode = ["move","destroy","getin","sad","join","leader","getout","cycle","load","unload","tr unload","hold","sentry","guard","talk","scripted","support","getin nearest","dismiss","defend","garrison","patrol","ambush"];
_sMode = "MOVE";           // see _sMode above
_sMofidier = 0;            // the waypoint radius or radius to patrol/defend/find ambush position/wait after landing
_sComplete = 0;            // completition radius - the radius for the radius to be completed in
_sRoad = false;            // force road - whether the radius has to be on a road

if (count _statementStart > 0) then { _sMode = _statementStart select 0; };
if (count _statementStart > 1) then { _sMofidier = _statementStart select 1; };
if (count _statementStart > 2) then { _sComplete = _statementStart select 2; };
if (count _statementStart > 3) then { _sRoad = _statementStart select 3; };

_wpModes = [_this, 4, []] call BIS_fnc_param;  // _this select 3
_wpBehaviour = "AWARE";
_wpCombatMode = "YELLOW";
_wpSpeed = "NORMAL";
_wpFormation = "NO CHANGE"; // formation - Switches the group formation when the waypoint becomes active.

if (count _wpModes > 0) then { _wpBehaviour = _wpModes select 0; };
if (count _wpModes > 1) then { _wpCombatMode = _wpModes select 1; };
if (count _wpModes > 2) then { _wpSpeed = _wpModes select 2; };
if (count _wpModes > 3) then { _wpFormation = _wpModes select 3; };

_statement = [_this, 5, ""] call BIS_fnc_param; ; // OPTIONAL - must be string! (e.g. "hint 'waypoint completed' ")  // _this select 4 "car_1 = vehicle this"

// [[c130_1, "c130WP", "c130End", 400, 2000], [false, true], ["MOVE",0,0], ["CARELESS","white","normal"], "", [], ["CARELESS","white","normal"], ""] call pr_fnc_carWaypoint;

// [[jeep1, "jeepmove", ["jeepCycle", "CYCLE"], 30, 0], [false, true], ["MOVE",0,0], ["SAFE","white","LIMITED"], ""] call pr_fnc_carWaypoint;
_wpLPos = [0,0,0];
for [{ _loop = 0 }, { _loop < count _waypoints }, { _loop = _loop + 1 }] do {
    {
        _wpL = _waypoints select _loop;
        if (typeName _wpL == "ARRAY") then {
            _new_wpL = _wpL select 0; pr_new_wpL = _new_wpL;
            _sMode = _wpL select 1;
            if (typeName _new_wpL == "STRING") then { _wpLPos = getMarkerPos _new_wpL; } else { _wpLPos = getPos _new_wpL; };
        } else {
            if (typeName _wpL == "STRING") then { _wpLPos = getMarkerPos _wpL; } else { _wpLPos = getPos _wpL; };
        };

        [_groupVeh, _wpLPos, [_sMode, _sMofidier, _sComplete, _sRoad], [_wpBehaviour, _wpCombatMode, _wpSpeed, _wpFormation], _statement] call pr_fnc_addWaypoint;
    } forEach _waypoints;
    _waypoints spawn { sleep 0.01; };
};

//pr_waypoints = _waypoints; publicVariable "pr_waypoints"; // delete after testing
if (_limitSpeed) then { _veh limitSpeed _speed; };
_veh flyInHeight _height;


// exit script if wanting to cycle waypoints
if (typeName _end == "ARRAY") exitWith { if !(pr_deBug == 0) then { diag_log format ["*PR* fn_carWaypoint start"]; }; };

//--- Waypoint: End
//[group, position, ["mode", modifier, completition radius, force road], ["BEHAVIOUR", "COMBATMODE", "SPEED"], "code"] call ws_fnc_addWaypoint;
private ["_endPos","_end","_statementEnd","_sMode2","_sMofidier","_endDest","_statementLast"];
_endPos = [0,0,0];

//pr_end = _end; // delete after testing
if (typeName _end == "STRING") then { _endPos = getMarkerPos _end; } else { _endPos = getPos _end; };
_statementEnd = [_this, 6, [], [[]]] call BIS_fnc_param;  // _this select 5
//_sMode2 = ["move","destroy","getin","sad","join","leader","getout","cycle","load","unload","tr unload","hold","sentry","guard","talk","scripted","support","getin nearest","dismiss","defend","garrison","patrol","ambush"];
_sMode2 = "MOVE";           // see _sMode2 above
_sMofidier2 = 0;            // the waypoint radius or radius to patrol/defend/find ambush position/wait after landing
_sComplete2 = 0;            // completition radius - the radius for the radius to be completed in
_sRoad2 = false;            // force road - whether the radius has to be on a road

if (count _statementEnd > 0) then { _sMode2 = _statementEnd select 0; };
if (count _statementEnd > 1) then { _sMofidier2 = _statementEnd select 1; };
if (count _statementEnd > 2) then { _sComplete2 = _statementEnd select 2; };
if (count _statementEnd > 3) then { _sRoad2 = _statementEnd select 3; };

// end waypoint behaviour
_wp2Modes = [_this, 7, []] call BIS_fnc_param;  // _this select 6
_wp2Behaviour = "AWARE";
_wp2CombatMode = "YELLOW";
_wp2Speed = "NORMAL";
_wp2Formation = "NO CHANGE"; // formation - Switches the group formation when the waypoint becomes active.

if (count _wp2Modes > 0) then { _wp2Behaviour = _wp2Modes select 0; };
if (count _wp2Modes > 1) then { _wp2CombatMode = _wp2Modes select 1; };
if (count _wp2Modes > 2) then { _wp2Speed = _wp2Modes select 2; };
if (count _wp2Modes > 3) then { _wp2Formation = _wp2Modes select 3; };

_endDest = "Land_HelipadEmpty_F" createVehicle _endPos;
_statementLast = [_this, 8, ""] call BIS_fnc_param; ; // OPTIONAL - must be string! (e.g. "hint 'waypoint completed' ")  // _this select 7

//[[c130_1, "c130WP", "c130End", 600, 5000], [false, true], ["CARELESS","white","full"], "", [""], ["CARELESS","white","full"], ""] call pr_fnc_carWaypoint;
//pr_endDest = _endDest; // delete after testing
[_groupVeh, _endDest, [_sMode2, _sMofidier2, _sComplete2, _sRoad2], [_wp2Behaviour, _wp2CombatMode, _wp2Speed, _wp2Formation], _statementLast] call pr_fnc_addWaypoint;
//pr_statementLast = _statementLast; // delete after testing

/*
_groupVeh spawn {
    _groupVeh = _this select 0;
    sleep 300;

    if (_groupVeh != grpNull) then {
        { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh;
    };
};
*/

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_carWaypoint start"]; };
