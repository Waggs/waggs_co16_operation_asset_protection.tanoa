//--- natoBox_support.sqf
//--- Add the following to the init field of an ammo box in the editor
//--- _nil = this execVM "scripts\PR\loadouts\box\natoBox_support.sqf";
//--- When adding items, make sure they are in the correct sections (weapons/ammo/items..) 
//--- and make sure the last item in each group doesn't have a comma at the end of the line

waitUntil { !(isNil "objectVarsCompiled") };

[_this,
//--- <1> Add Nato Common weapons
[
	["Binocular",							 5 ],
	["RangeFinder",							 5 ],
	["LaserDesignator",						 5 ],
	["MineDetector",						 5 ]
],
//--- <2> Add Nato Arma 3 Weapons
[], 
//<3> Add Nato Massi Weapons
[],
//--- <4> Add Nato Common Ammo
[
	["LaserBatteries",                		10 ],
[chemY, 10 ],
	["Chemlight_blue",                    	20 ],
	["B_IR_Grenade",                    	 5 ]
],
//--- <5> Add Nato Arma 3 Ammo
[],
//--- <6> Add Nato Massi Ammo
[],
//--- <7> Add Nato Common Items
[
	["FirstAidKit",       					10 ],
	["B_UavTerminal",						 5 ]
],
//--- <8> Add Nato Arma 3 Items
[],
//--- <9> Add Nato Massi Items
[],
//--- <10> Add Nato Common Backpacks
[
	["B_Carryall_mcamo",					 4 ]
],
//--- <11> Add Nato Arma 3 Backpacks
[],
//--- <12> Add Nato Massi Backpacks
[],
//--- <13> How many backup short range radios?
4
]
execVM "scripts\PR\loadouts\box\box_fill.sqf";
