/* csatBox_ammo.sqf
*  Add the following to the init field of an ammo box in the editor:  _nil = this execVM "scripts\PR\loadouts\box\csatBox_ammo.sqf";
*  When adding items, make sure they are in the correct sections (weapons/ammo/items..), and make sure the last item in each group doesn't have a comma at the end of the line
*  if spawned box use: [[namebox], "fnc_csatBoxAmmo"] call BIS_fnc_MP;
*/

waitUntil { !(isNil "objectVarsCompiled") };

//--- <0> Box or item to add items
[_this,

//*** WEAPONS ***//
//--- <1> Add csat Common weapons
[],

//--- <2> Add csat ACE Weapons
[],

//--- <3> Add csat Arma 3 Weapons
[],

//--- <4> Add csat Massi Weapons
[],

//--- <5> Add csat RHS Weapons
[
["rhs_weap_rpg7_pgo", 4 ]
],


//*** AMMO ***//
//--- <6> Add csat Common Ammo *** need russian
[
["1Rnd_HE_Grenade_shell", 20 ],
["1Rnd_Smoke_Grenade_shell", 5 ],
["UGL_FlareWhite_F", 5 ],
[grenade, 5 ]
],

//--- <7> Add csat ACE Ammo
[
[aceFlashBang,  5 ]
],

//--- <8> Add csat Arma 3 Ammo *** need russian
[
[TiAT, 2 ],
[TiAA, 2 ],
["RPG32_F", 2 ],
["NLAW_F", 2 ],
["150Rnd_762x54_Box_Tracer", 6 ],
["5Rnd_127x108_Mag", 5 ],//need
["7Rnd_408_Mag", 5 ],//need
["30Rnd_65x39_caseless_green_mag_Tracer", 20 ]
],

//--- <9> Add csat Massi Ammo *** need russian
[
["mas_M136", 3 ],
["mas_SMAW", 3 ],
["mas_Stinger", 3 ],
["100Rnd_mas_762x51_T_Stanag", 4 ],
["200Rnd_mas_556x45_T_Stanag", 4 ],
["30Rnd_mas_556x45_T_Stanag", 20 ],
["30Rnd_556x45_Stanag_Tracer_Yellow", 8 ],
["5Rnd_127x108_Mag", 5 ],
["5Rnd_mas_127x99_Stanag", 5 ],
["5Rnd_mas_127x99_dem_Stanag", 3 ],
["5Rnd_mas_127x99_T_Stanag", 3 ]
],

//--- <10> Add csat RHS Ammo
[
["rhs_45Rnd_545X39_AK_Green", 12 ],
["rhs_30Rnd_545x39_7N10_AK", 12 ],
["rhs_100Rnd_762x54mmR_green", 8 ],
["rhs_100Rnd_762x54mmR", 8 ],
["rhs_VOG25", 8 ],
["rhs_VG40OP_white", 8 ],
["rhs_GRD40_White", 8 ],
["rhs_rpg7_PG7VL_mag", 3 ],
["rhs_rpg7_PG7VR_mag", 4 ]
],

//*** ITEMS ***//
//--- <11> Add csat Common Items
[],

//--- <12> Add csat ACE Items
[],

//--- <13> Add csat Arma 3 Items
[],

//--- <14> Add csat Massi Items
[],

//--- <15> Add csat RHS Items
[],

//***BACKPACKS***//
//--- <16> Add csat Common Backpacks
[],

//--- <17> Add csat ACE Backpacks
[],

//--- <18> Add csat Arma 3 Backpacks
[],

//--- <19> Add csat Massi Backpacks
[],

//--- <20> Add csat RHS Backpacks
[],

//*** RADIOS ***//
//--- <21> How many backup short range radios?
0
] execVM "scripts\PR\loadouts\box\box_fill.sqf";
