/* fn_customBox.sqf
*  Author: PapaReap
*  [[_dropbox], "fnc_natoBoxAmmo"] call BIS_fnc_MP;
*/

waitUntil { !(isNil "objectVarsCompiled") };

fnc_natoBoxAmmo = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\natoBox_ammo.sqf";
};

fnc_natoBoxAmmoCargo = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\natoBox_ammoCargo.sqf";
};

fnc_csatBoxAmmo = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\csatBox_ammo.sqf";
};

fnc_aceMedBox = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\aceBox_med.sqf";
};

fnc_aceDefend = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\aceBox_defend.sqf";
};

fnc_uniformBox = {
    _box = _this select 0;
    _nil = _box execVM "scripts\PR\loadouts\box\uniform.sqf";
};
