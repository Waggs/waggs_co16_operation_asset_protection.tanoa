/* Copy this file and create a box type name, i.e. - natoBox_ammo.sqf 
*  Add the following to the init field of an ammo box in the editor:  _nil = this execVM "scripts\PR\loadouts\box\csatBox_ammo.sqf"; 
*  When adding items, make sure they are in the correct sections (weapons/ammo/items..), and make sure the last item in each group doesn't have a comma at the end of the line
*  If spawned box use: i.e.  [[namebox], "functionName"] call BIS_fnc_MP; 
*/

//--- <0> Box or item to add items 
[_this, 

//*** WEAPONS ***// 
//--- <1> Add csat Common weapons 
[], 

//--- <2> Add csat ACE Weapons 
[], 

//--- <3> Add csat Arma 3 Weapons 
[], 

//--- <4> Add csat Massi Weapons 
[], 

//--- <5> Add csat RHS Weapons 
[], 


//*** AMMO ***// 
//--- <6> Add csat Common Ammo *** need russian 
[], 

//--- <7> Add csat ACE Ammo 
[], 

//--- <8> Add csat Arma 3 Ammo *** need russian 
[], 

//--- <9> Add csat Massi Ammo *** need russian 
[], 

//--- <10> Add csat RHS Ammo 
[], 

//*** ITEMS ***// 
//--- <11> Add csat Common Items 
[], 

//--- <12> Add csat ACE Items 
[], 

//--- <13> Add csat Arma 3 Items 
[], 

//--- <14> Add csat Massi Items 
[], 

//--- <15> Add csat RHS Items 
[], 

//***BACKPACKS***// 
//--- <16> Add csat Common Backpacks 
[], 

//--- <17> Add csat ACE Backpacks 
[], 

//--- <18> Add csat Arma 3 Backpacks 
[], 

//--- <19> Add csat Massi Backpacks 
[], 

//--- <20> Add csat RHS Backpacks 
[], 

//*** RADIOS ***// 
//--- <21> How many backup short range radios? 
0 
] execVM "scripts\PR\loadouts\box\box_fill.sqf"; 