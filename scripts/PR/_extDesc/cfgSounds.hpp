
//--- Adds wind sounds for snow missions
#include "..\weather\sounds\weather_cfgSounds.hpp"

//--- Defuse
#include "..\intel_destruction\defuse\sounds\cfgSounds.hpp"

//--- Nuke
#include "..\intel_destruction\nuke\sounds\cfgSounds.hpp"

//--- Burn
#include "..\intel_destruction\fire\sounds\cfgSounds.hpp"
