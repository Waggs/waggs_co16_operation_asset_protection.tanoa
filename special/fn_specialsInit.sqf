/*
*  Author: PapaReap
*  Setting up scripts to be ran mission specific - this is performed pre-init
*/


//--- briefing.sqf               - Sets mission breifing
useBriefing = true;

//--- tasks.sqf                  - Sets mission tasks
useTasks = true;

//--- fn_Client.sqf              - Initilize Custom Loadout scripts
useClient = true;

//--- fn_missionTimer.sqf        - Allows putting an ending on mission by timer
useMissionTimer = false;

//--- fn_moveInVehTimer.sqf      - Moves players into vehicle at mission start, set on a timer
useMoveInVehicleTimer = false;

//--- randomMoveInit.sqf         - Random object start positions
useRandomMove = false;

//--- intel_destructionInit.sqf  - Data Down/Up load, Bomb defuse, Nuke & Fire scripts
useIntel_Destruction = false;

//--- drag_koInit.sqf            - Allows drag and knock out on units
useDrag_koInit = false;

//--- hideMapObjects.sqf         - Function to hide map objects at mission start
useHideMapObjects = false;

//--- IgiPreLoad.sqf             - IgiLoad Pre-Load items in vehicles before mission start
useIgiPreLoad = false;

//--- pr_Init.sqf                - Reaps custom init for all things not init.sqf
usePR_Init = false;