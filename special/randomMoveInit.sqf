/* randomMoveInit.sqf
*  Author: PapaReap
*
*  RANDOM MOVER
*  Random Movers: Can be any object, marker, logic, trigger. Can use as many as you like.
*  Trigger Usage: [crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]] remoteExec ["pr_fnc_randomMove", 0, false];
*  Usage: Separate by commas i.e. - randomMover = [<REFERENCE POINT>, [<RANDOM POSITIONS TO MOVE TO>], [<OBJECTS TO BE MOVED>]];
*  Arguments:
*  0: <REFERENCE POINT>              - Name of object or position to get reference to movable objects, use only one. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  1: <RANDOM POSITIONS TO MOVE TO>  - Object names to use for random placement, any amount can be used. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  2: <OBJECTS TO BE MOVED>          - Objects to be moved, any amount can be used.  - (variable name)  note: not position
*
*  Ver 1.0 2016-05-25
*/

//--- un-comment the line below to stop script
if (true) exitWith {};

randomMover = [
    [
/*  <0> */  crash,
/*  <1> */  [crash_1, crash_2, crash_3, crash_4, crash_5, crash_6, crash_7, crash_8],
/*  <2> */  [hWreck, pilot1, pilot2, task1succeeded, "area2", Trg01_trip, Trg02_trip, Trg03_trip, mzi_1, mzi_2, mzi_3]
    ]
/* un-comment lines below to add more random movers, can copy paste format above, replace names */
//   ,[
//          <enter more random movers here>
//    ]
];




/*************************************** DO NOT EDIT BELOW ***************************************/

if !(isServer) exitWith {};

waitUntil { time > 0 };

if (isNil "randomMove") then { randomMove = true; };
for [{ _loop = 0 }, { _loop < count randomMover }, { _loop = _loop + 1 }] do {
    waitUntil { randomMove };
    randomMove = false;
    _reference = (randomMover select _loop) select 0;
    _movePos = (randomMover select _loop) select 1;
    _moveable = (randomMover select _loop) select 2;
    [_reference, _movePos, _moveable] remoteExec ["pr_fnc_randomMove", 0, false]; // check local server moving instead
};
