
//--- un-comment the line below to stop script
//if (true) exitWith {};

[
    west,
        ["Situation",
            "We have taken Saint-George Airstrip but failed to take the rest of the island on our first insertion"],
        ["Mission",
            "Clear the island surrounding our new airstrip use any and all equipment you need to do so. Do Not lose our foothold here it is our only chance."],
        ["Execution",
            ""],
        ["Exfiltration",
            "RTB once task is complete"]
] call FHQ_fnc_ttAddBriefing;
